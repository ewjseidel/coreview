'use strict';

const fs = require('fs');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');
const webpack = require('webpack');

module.exports = function (mode, isProduction, externals, loaders) {
    const extensionPrefix = isProduction ? '.min' : '';

    // Name of PHP constant to store cache busting version number in.
    const versionConstant = 'COREVIEW_THEME_VERSION';

    // Relative path to the theme.
    const basePath = './wp-content/themes/coreview';

    const paths = {
        cacheBusting: `${basePath}/version.php`,
        css: `${basePath}/assets/css/`,
        img: `${basePath}/assets/img/`,
        font: `${basePath}/assets/font/`,
        js: `${basePath}/assets/js/`,
        lang: `${basePath}/languages/`,
    };

    // Entry points for theme. Key will be used as output file name.
    const entry = {
        theme: [
            `${basePath}/build/js/theme.js`,
            `${basePath}/build/scss/theme.scss`,
        ],
        editor: [
            `${basePath}/build/js/editor.js`,
            `${basePath}/build/scss/editor.scss`,
        ],
        fontawesome: [
            `${basePath}/build/js/font-awesome.js`,
        ],
        login: [
            `${basePath}/build/scss/wpLogin.scss`,
        ],
    };

    const config = {
        mode,
        entry,
        output: {
            path: path.join(__dirname, '/'),
            filename: `${paths.js}[name]${extensionPrefix}.js`,
        },
        externals,
        resolve: {
            alias: {
                'vue$': 'vue/dist/vue.esm.js' // 'vue/dist/vue.common.js' for webpack 1
            },
            modules: [
                `${basePath}/build/js/modules`,
                'node_modules',
            ],
        },
        module: {
            rules: [
                {
                    enforce: 'pre',
                    test: /\.js|.jsx/,
                    loader: 'import-glob',
                    exclude: /node_modules/,
                },
                {
                    test: /\.js|.jsx/,
                    loader: 'babel-loader',
                    query: {
                        presets: [
                            '@wordpress/default',
                        ],
                        plugins: [
                            'transform-class-properties',
                            [
                                'transform-react-jsx',
                                {
                                    pragma: 'wp.element.createElement'
                                }
                            ],
                            'lodash',
                        ]
                    },
                    exclude: /node_modules/
                },
                {
                    test: /\.html$/,
                    loader: 'raw-loader',
                    exclude: /node_modules/
                },
                {
                    test: /\.css$/,
                    use: [MiniCssExtractPlugin.loader, loaders.css, loaders.postCss],
                    exclude: /node_modules/
                },
                {
                    test: /\.scss$/,
                    use: [MiniCssExtractPlugin.loader, loaders.css, loaders.postCss, loaders.sass],
                    exclude: /node_modules/
                },
                {
                    test: /\.(ttf|eot|svg|woff2?)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: '[name].[ext]',
                                outputPath: paths.font,
                            }
                        }
                    ],
                    exclude: /(assets|build\/img)/
                },
                {
                    test: /\.(gif|jpg|png)?$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: '[name].[ext]',
                                outputPath: paths.img,
                                publicPath: '../img',
                            }
                        }
                    ],
                    exclude: /(assets|build\/font)/
                },
                {
                    test: /\.(svg)?$/,
                    use: [
                        {
                            loader: 'svg-url-loader',
                            options: {
                                stripdeclarations: true,
                                iesafe: true,
                                encoding: 'none',
                                limit: 2048,
                                name: '[name].[ext]',
                                outputPath: paths.img,
                                publicPath: '../img',
                            }
                        }
                    ],
                    exclude: /(assets|build\/font)/
                }
            ],
        },
        plugins: [
            new MiniCssExtractPlugin({
                filename: `${paths.css}[name]${extensionPrefix}.css`
            }),
            new webpack.DefinePlugin({
                'process.env.NODE_ENV': JSON.stringify(mode),
            }),
            // Custom webpack plugin - remove generated JS files that aren't needed
            function () {
                this.hooks.done.tap('webpack', function (stats) {
                    stats.compilation.chunks.forEach(chunk => {
                        if (!chunk.entryModule._identifier.includes(".js")) {
                            chunk.files.forEach(file => {
                                if (fs.existsSync(file) && file.includes(".js")) {
                                    fs.unlinkSync(path.join(__dirname, `/${file}`));
                                }
                            });
                        }
                    });
                });
            },
            // Custom webpack plugin - generate file with constant for cache busting
            function () {
                if (isProduction) {
                    this.hooks.done.tap('webpack', function () {
                        fs.writeFile(paths.cacheBusting, `<?php\n\ndefine( '${versionConstant}', ${Math.round((new Date()).getTime() / 1000)} );\n`);
                    });
                }
            },
        ],
    };

    if (!isProduction) {
        config.devtool = 'source-map';
    }

    return config;
};
