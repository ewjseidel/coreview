const gulp = require('gulp');
const clean = require('del');
const rename = require('gulp-rename');

const config = {
    src: [
        './wp-content/**',
        '!./wp-content/cache',
        '!./wp-content/cache/**',
        '!./wp-content/ewww',
        '!./wp-content/ewww/**',
        '!./wp-content/upgrade',
        '!./wp-content/upgrade/**',
        '!./wp-content/uploads',
        '!./wp-content/uploads/**',
        '!**/.git',
        '!**/.git*',
        '!**/browsers.json',
        '!**/composer.json',
        '!**/gulpfile.js',
        '!**/node_modules',
        '!**/node_modules/**',
        '!**/webpack*config.js',
        '!**/*.lock',
        '!**/*.local.php',
        '!**/coreview/build',
        '!**/coreview/build/**',
        '!**/coreview-blocks/build',
        '!**/coreview-blocks/build/**',
    ],
    dest: './build/wp-content',
    clean: [
        './build/wp-content/**/*',
        './build/wp-config.php',
    ],
};

gulp.task('project:clean', gulp.series(() => {
    return clean(config.clean);
}));

gulp.task('project:stage', gulp.series('project:clean', (done) => {
    gulp.src(config.src).pipe(gulp.dest(config.dest));
    gulp.src('./coreview-wp-config.php').pipe(rename('wp-config.php')).pipe(gulp.dest('./build'));
    done();
}));

gulp.task('default', gulp.series('project:stage'));
