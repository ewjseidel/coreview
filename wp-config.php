<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'GuTs3MLLcUyppiU62Ooagbf09YL5KdyC83h4x4b/pjGs3UKnectofrVH68RN9IAzKLK0wrxAKHfgHNCzIclHCg==');
define('SECURE_AUTH_KEY',  'G+Ydm1xykz1X5xUpkjNMMqXt6fzFgUU66qs6LI7MMVo0AS6oQHyJ8cygJeAZWFDjSu/fzRwuhzvAx16fQNfuPA==');
define('LOGGED_IN_KEY',    'uEhG5NQUYh7nEwAGWpxWiHnK/TSpPNJyVaD8noYNBmyKqe+keX3ZIM573EkgVOSVyKg55mbhm/BIAv1uslDe4w==');
define('NONCE_KEY',        'wEETAy9RvLNSWcM/wJjIzs+vPEp0wVkkZP+HdoZLi+g1cOQ4UBr+FfiqditRk04NaXmLypG7ujUuvyXGh3VlQA==');
define('AUTH_SALT',        'EO3vdqBF4VFcngFgS08C1kGrESwaXAWLZoV1XCM1T4NPwLUiLzeJbbIVvCa613tXmDMyJMLl5icVlrpvHaWGyg==');
define('SECURE_AUTH_SALT', 'DeUrf2iNeeYq2dekRVSQ8+7bZXhPacJYsZX2xmIv5u9b+uyAOMz7ytK6t4ABcYFwWlJwyR0N9rcju+caXJaQlQ==');
define('LOGGED_IN_SALT',   'XN7tL7RSUF8BR85QGBhIVnPab4Vqe9WSshiKhd6M5DmzDw+JvvENF78QSviHBHXMsWbqARe4CyRucfTSwYWm5g==');
define('NONCE_SALT',       'sgw07p66QeryLQP0GcUc6tIRub3TVlgRzJo0OQsMtav3DkXtqnBX3icoL11CxWXpD+Htn1aavi8TST/X/v/z+A==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


define( 'WP_HOME', 'https://coreview.local' );
define( 'WP_SITEURL', 'https://coreview.local' );

define( 'WP_DEBUG', true );
define( 'SCRIPT_DEBUG', true );

define( 'WP_POST_REVISIONS', 25 );
define( 'DISALLOW_UNFILTERED_HTML', true );


/* Inserted by Local by Flywheel. See: http://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy */
if ( isset( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ) {
	$_SERVER['HTTPS'] = 'on';
}
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
