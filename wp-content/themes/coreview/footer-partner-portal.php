

            </div> <!-- .container -->
        </main><!-- .site-content -->

        <?php get_template_part( 'partials/pre-footer' ); ?>

        <footer itemscope itemtype="http://schema.org/WPFooter" class="site-footer">
            <?php get_template_part( 'partials/partner-portal-footer' ); ?>
        </footer>

    </div><!-- .site -->

    <?php get_template_part( 'partials/nav-mobile' ); ?>

    <?php wp_footer(); ?>

    </body>

</html>
