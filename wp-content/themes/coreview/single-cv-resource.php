<?php

the_post();
get_header();

$postData = \Coreview\Models\Resource::create(get_post());

$postTags = $postData->getTags();

$postTerm = $postData->getMainResource();

$postType = 'cv-resource';


?>

<?php //echo "resource is gated".$postData->isGated();?>
	<div class="grid__row">
		<div class="grid__col col-10/12@lg offset-1/12@lg">

			<article class='single-cv-resource post-type-template'>
				<header class='single-cv-resource__post-header'>
					<div class="single-cv-resource__tax-title alignfull">
						<div class="container">
                            <p>
								<?php if ($postData->getNextPost()):?>
                                    <a class="single-cv-news-item__tax-title-next" href="<?php echo get_the_permalink($postData->getNextPost())?>">
                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" height="14px" width="8px" viewBox="0 0 8 14" style="enable-background:new 0 0 8 14;" xml:space="preserve">
                                            <polygon points="8,0.3 8,13.7 0,7 "></polygon>
                                        </svg>
                                    </a>
	                            <?php endif;?>
								<?php echo $postData->getMainResourceTypeName();?>
								<?php if ($postData->getPreviousPost()):?>
                                    <a class="single-cv-news-item__tax-title-prev" href="<?php echo get_the_permalink($postData->getPreviousPost())?>">
                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" height="14px" width="8px" viewBox="0 0 8 14" style="enable-background:new 0 0 8 14;" xml:space="preserve">
                                            <polygon points="0,13.7 0,0.3 8,7 "></polygon>
                                        </svg>
                                    </a>
	                            <?php endif;?>
                            </p>
						</div>
					</div>

					<div class='single-cv-resource__breadcrumbs'>
						<ul class=''>
							<?php //TODO: Add breadcrumb package ?>
							<li><a href="<?php echo get_site_url();?>">Home</a></li>
							<li><a href="<?php echo get_site_url("", "/resources");?>">Resources</a></li>
							<li><a href="#"><?php echo the_title();?></a></li>
						</ul>
					</div>
				</header>

				<div class="single-cv-resource__main">
					<div class="single-cv-resource__inner">
						<div class="single-cv-resource__post-date">
							<p><?php echo get_the_date('M j Y');?></p>

                        </div>

                        <?php  global $wp_query; ?>

                            <?php if ( empty( $postData->isGated() ) ):?>

                                <div class="single-cv-resource__content">

                                    <div class="single-cv-resource__content">
                                        <div class="single-cv-resource__post-title">
                                            <h1><?php echo $postData->PostTitle();?></h1>
                                        </div>
                                        <?php the_content(); ?>

                                        <?php if ( ! empty ( $postData->isDownload() ) ):?>
                                            <div class="wp-block-button cv-button">
                                                <a class="wp-block-button__link has-white-color has-background has-blue-background-color" href="<?php echo $postData->downloadFile();?>" download>
                                                    Download This Resource</a>
                                            </div>
                                        <?php endif; ?>


                                    </div>

                                </div>

                            <?php elseif( ! empty( $postData->isGated() ) ): ?>
                                <div class="single-cv-resource__content">
		                            <?php if ( isset ($wp_query->query_vars['resource-ungated'])):?>
                                        <div class="single-cv-resource__content">
                                            <div class="single-cv-resource__post-title">
                                                <h1><?php echo $postData->PostTitle();?></h1>
                                            </div>
				                            <?php the_content(); ?>

				                            <?php if ( ! empty ( $postData->isDownload() ) ):?>
                                                <div class="wp-block-button cv-button">
                                                    <a class="wp-block-button__link has-white-color has-background has-blue-background-color" href="<?php echo $postData->downloadFile();?>" download>
                                                        Download This Resource</a>
                                                </div>
				                            <?php endif; ?>


                                        </div>
		                            <?php else: ?>
                                        <div class="single-cv-resource__content-gated">
                                            <div class="single-cv-resource__excerpt">
					                            <?php if ( !empty( $postData->postExcerpt() ) ): ?>
                                                    <div class="single-cv-resource__post-title">
                                                        <h1><?php echo $postData->PostTitle();?></h1>
                                                    </div>
                                                    <p><?php echo $postData->postExcerpt(); ?></p>
					                            <?php else: ?>
                                                    <p>Checkout our our resource.</p>
					                            <?php endif;?>
                                                <div class="single-cv-resource__gated-message">
                                                    <p>Fill out the attached form for access to this resource</p>
                                                </div>
                                            </div>
                                            <div class="single-cv-resource__form">
					                            <?php if ( ! empty ( $postData->formTitle() ) ):?>
                                                    <h2 class="single-cv-resource__form-title"><?php echo esc_html($postData->formTitle()); ?></h2>
					                            <?php endif; ?>
					                            <?php if ( ! empty ( $postData->formDescription() ) ):?>
                                                    <p class="single-cv-resource__form-description"><?php echo esc_html($postData->formDescription()); ?></p>
					                            <?php endif; ?>
					                            <?php echo $postData->getForm()?>
                                            </div>

                                        </div>
		                            <?php endif;?>

                                </div>

                            <?php endif;?>

					</div>



				</div>
			</article>

		</div>

	</div>

	<div class="grid__row">
		<div class="grid__col">
			<?php \Coreview\loadTemplateWithContext("partials/related-resources", [
				'postType' => $postType,
			]) ?>
		</div>

	</div>


<?php get_footer();

