<div class="portal-territories">
    <div class="portal-territories__inner">
        <div class="portal-territories__heading">
            <p>CoreView Sales Territories</p>
        </div>
        
        <div class="portal-territories__body">
            <div class="portal-territories__nav-links">
                <a class="portal-territories__nav-item active" href="#" data-tab="0"><h4>North America</h4></a>
                <a class="portal-territories__nav-item " href="#" data-tab="1"><h4>EMEA</h4></a>
            </div>
            <div class="portal-territories__tabs">
                <div class="portal-territories-tab tab-0 active">
                    <div class="portal-territories-item">
                        <div class="portal-territories-item__title">
                            <h3>Great Lakes</h3>
                            <p class="name"><b>Tracy Riddle -- Account Exec.</b></p>
                            <p>Chicago, IL</p>
                            <p>(630) 217 - 5939</p>
                            <a href="mailto:tracy.riddle@coreview.com">Email</a>
                            <hr>
                            <ul>
                                <li>Michigan</li>
                                <li>Indiana</li>
                                <li>Kentucky</li>
                            </ul>
                        </div>
                    </div>
                    <div class="portal-territories-item">
                        <div class="portal-territories-item__title">
                            <h3>North Central</h3>
                            <p class="name"><b>Tracy Riddle -- Account Exec.</b></p>
                            <p>Chicago, IL</p>
                            <p>(630) 217 - 5939</p>
                            <a href="mailto:tracy.riddle@coreview.com">Email</a>
                            <hr>
                            <ul>
                                <li>Illinois</li>
                                <li>Iowa</li>
                                <li>Minnesota</li>
                                <li>Nebraska</li>
                                <li>North Dakota</li>
                                <li>South Dakota</li>
                                <li>Wisconsin</li>
                            </ul>
                        </div>
                    </div>
                    <div class="portal-territories-item">
                        <div class="portal-territories-item__title">
                            <h3>Northwest</h3>
                            <p class="name"><b>Lambryne DeKazos -- Account Exec.</b></p>
                            <p>San Fransisco, CA</p>
                            <p>(312) 560 - 0552</p>
                            <a href="mailto:lambryne.dekazos@coreview.com">Email</a>
                            <hr>
                            <ul>
                                <li>Alaska</li>
                                <li>California (north)</li>
                                <li>Montana</li>
                                <li>Oregon</li>
                                <li>Washington</li>
                                <li>Wyoming</li>
                            </ul>
                        </div>
                    </div>
                    <div class="portal-territories-item">
                        <div class="portal-territories-item__title">
                            <h3>Northeast</h3>
                            <p class="name"><b>Herb Labrada — Account Exec.</b></p>
                            <p>New York, NY</p>
                            <p>(914) 772 - 4658</p>
                            <a href="mailto:herb.labrada@coreview.com">Email</a>
                            <hr>
                            <ul>
                                <li>Connecticut</li>
                                <li>Maine</li>
                                <li>Massachusetts</li>
                                <li>New Hampshire</li>
                                <li>New Jersey</li>
                                <li>New York</li>
                                <li>Pennsylvania (west)</li>
                                <li>Rhode Island</li>
                                <li>Vermont</li>
                            </ul>
                        </div>
                    </div>
                    <div class="portal-territories-item">
                        <div class="portal-territories-item__title">
                            <h3>Southwest</h3>
                            <p class="name"><b>Paul Galichia — Account Exec.</b></p>
                            <p>Los Angeles, CA</p>
                            <p>(917) 691 - 1644</p>
                            <a href="mailto:paul.galichia@coreview.com">Email</a>
                            <hr>
                            <ul>
                                <li>Arizona</li>
                                <li>California (south)</li>
                                <li>Colorado</li>
                                <li>Hawaii</li>
                                <li>New Mexico</li>
                                <li>Nevada</li>
                                <li>Utah</li>
                            </ul>
                        </div>
                    </div><div class="portal-territories-item">
                        <div class="portal-territories-item__title">
                            <h3>South Central</h3>
                            <p class="name"><b>Marcus Conroy — Vice President, North America Sales</b></p>
                            <p>Dallas, TX</p>
                            <p>(972) 310 - 4898</p>
                            <a href="mailto:marcus.conroy@coreview.com">Email</a>
                            <hr>
                            <ul>
                                <li>Arkansas</li>
                                <li>Kansas</li>
                                <li>Louisiana</li>
                                <li>Missouri</li>
                                <li>Oklahoma</li>
                                <li>Texas</li>
                            </ul>
                        </div>
                    </div>
                    <div class="portal-territories-item">
                        <div class="portal-territories-item__title">
                            <h3>Southeast</h3>
                            <p class="name"><b>Valerie Forrester — Account Exec.</b></p>
                            <p>Atlanta, GA</p>
                            <p>(678) 488 - 2611</p>
                            <a href="mailto:valerie.forrester@coreview.com">Email</a>
                            <hr>
                            <ul>
                                <li>Alabama</li>
                                <li>Florida</li>
                                <li>Georgia</li>
                                <li>Mississippi</li>
                                <li>North Carolina</li>
                                <li>South Carolina</li>
                                <li>Tennessee</li>
                            </ul>
                        </div>
                    </div>
                    <div class="portal-territories-item">
                        <div class="portal-territories-item__title">
                            <h3>Washington D.C.</h3>
                            <p class="name"><b>Marcus Conroy — Vice President, North America Sales</b></p>
                            <p>Dallas, TX</p>
                            <p>(972) 310 - 4898</p>
                            <a href="mailto:marcus.conroy@coreview.com">Email</a>
                            <hr>
                            <ul>
                                <li>D.C.</li>
                                <li>Delaware</li>
                                <li>Maryland</li>
                                <li>Ohio</li>
                                <li>Pennsylvania (east)</li>
                                <li>Virginia</li>
                                <li>West Virginia</li>
                            </ul>
                        </div>
                    </div>
                    <div class="portal-territories-item">
                        <div class="portal-territories-item__title">
                            <h3>Canada</h3>
                            <p class="name"><b>John David — Account Exec.</b></p>
                            <p>Vancouver, AB Canada</p>
                            <p>(250) 889 - 9294</p>
                            <a href="mailto:john.david@coreview.com">Email</a>
                            <hr>
                            <ul>
                                <li>Canada (all territories)</li>
                            </ul>
                        </div>
                    </div>

                </div>

                <div class="portal-territories-tab tab-1">

                    <div class="portal-territories-item">
                        <div class="portal-territories-item__title">
                            <h3>Southern Europe & ME</h3>
                            <p class="name"><b>Andrea Calcagni</b></p>
                            <a href="mailto:andrea.calcagni@coreview.com">Email</a>
                        </div>
                    </div>
                    <div class="portal-territories-item">
                        <div class="portal-territories-item__title">
                            <h3>Germany, Austria, Switzerland & CE Europe</h3>
                            <p class="name"><b>Kristie Franco</b></p>
                            <a href="mailto:kristie.franco@coreview.com">Email</a>
                        </div>
                    </div><div class="portal-territories-item">
                        <div class="portal-territories-item__title">
                            <h3>UK, Ireland & Netherlands</h3>
                            <p class="name"><b>Stewart Connors</b></p>
                            <a href="mailto:stewart.connors@coreview.com">Email</a>
                        </div>
                    </div>
                    <div class="portal-territories-item">
                        <div class="portal-territories-item__title">
                            <h3>Nordics</h3>
                            <p class="name"><b>Kristie Franco</b></p>
                            <a href="mailto:kristie.franco@coreview.com">Email</a>

                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>