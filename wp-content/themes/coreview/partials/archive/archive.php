<?php

/* @var string $postType */

/* @var \wpscholar\WordPress\PostCollectionBase $results */

use Coreview\Archives;

use function Coreview\loadTemplateWithContext;

if ( ! isset( $results ) ) {
	return;
}

if ( ! isset( $postType ) ) {
	$postType = 'post';
	if ( $results->count() ) {
		$postType = get_post_type( $results->collection()->first() );
	}
}

?>

<div class="archive archive-<?php echo esc_attr( $postType ); ?>">
	<?php if ( $postType !== \Coreview\Models\TeamItem::POST_TYPE ) : ?>
        <div class="archive__filters archive-<?php echo esc_attr( $postType ); ?>__filters archive-<?php echo esc_attr( $postType ); ?>-filters grid__row">
            <?php loadTemplateWithContext( 'partials/archive/filters', [
                'postType' => $postType,
                'taxList'  => Archives::getTaxonomies( $postType ),
            ] ); ?>
        </div>
    <?php endif;?>
    <div class="archive__results archive-<?php echo esc_attr( $postType ); ?>__results grid__row">
	    <?php loadTemplateWithContext( 'partials/archive/results', [
		    'postType' => $postType,
		    'results'  => $results,
	    ] ); ?>
    </div>
    <div class="archive__pagination archive-<?php echo esc_attr( $postType ); ?>__pagination grid__row">
	    <?php loadTemplateWithContext( 'partials/archive/pagination', [
		    'results' => $results,
	    ] ); ?>
    </div>
</div>
