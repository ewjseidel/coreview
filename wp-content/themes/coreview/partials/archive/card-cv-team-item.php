<?php

/* @var \Coreview\Models\TeamItem $article */

$postType = get_post_type( $article->post );

?>

<article class="archive-card grid__col col-3/12@md col-4/12@sm col-6/12@xs card-<?php echo esc_attr( $postType ); ?>">
    <div class="archive-card__inner card-<?php echo esc_attr( $postType ); ?>__inner">
        <div class="card-<?php echo esc_attr( $postType ); ?>__image">
            <div class="card-<?php echo esc_attr( $postType ); ?>__image-inner">
                <?php echo wp_get_attachment_image( $article->personPhoto(), "medium");?>
            </div>
            <div class="card-<?php echo esc_attr( $postType );?>__link">
                <a href="<?php echo $article->personSocial(); ?>" target="_blank">
	                <?php echo file_get_contents( get_template_directory() . "/assets/img/LinkedIn.svg")?>
                </a>
            </div>
        </div>
        <div class="card-<?php echo esc_attr( $postType ); ?>__person-name">
            <h6><?php echo $article->personName(); ?></h6>
        </div>
        <div class="card-<?php echo esc_attr( $postType ); ?>__person-position">
            <p><?php echo $article->personPosition(); ?></p>
        </div>
    </div>
</article>
