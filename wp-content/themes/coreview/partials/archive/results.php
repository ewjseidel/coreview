<?php
/**
 * This file should not contain any markup. It is a highly reusable layer between a collection and archive cards.
 */

/* @var \wpscholar\WordPress\PostCollectionBase $results */

if ( $results->count() ) {
	foreach ( $results as $article ) {
		$post = $article->post;
		$GLOBALS['post'] = $post;
		setup_postdata( $post );
		$postType = get_post_type();
		\Coreview\loadTemplateWithContext( "partials/archive/card-{$postType}", [
			'article' => $article,
		] );
		wp_reset_postdata();
	}
} else {
	echo "<p style='width: 100%; text-align: center;'>Sorry, We didn't find any results for your search. Please try searching for something else.</p>";
}