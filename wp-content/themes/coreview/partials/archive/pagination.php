<?php

/* @var \Coreview\Collections\MixedPostCollection $results */

if ( ! isset( $results ) ) {
	return;
}

?>
<div class="tri-pagination">
	<?php
	echo paginate_links( [
		'current'   => $results->page ? : 1,
		'format'    => '?pg=%#%',
		'total'     => $results->max_num_pages,
		'prev_text' => '<i class="far fa-angle-double-left"></i> <span class="screen-reader-text">' . esc_html__( 'Previous page', 'coreview' ) . '</span>',
		'next_text' => '<span class="screen-reader-text">' . esc_html__( 'Next page', 'coreview' ) . '</span> <i class="far fa-angle-double-right"></i>',
	] );
	?>
</div>
