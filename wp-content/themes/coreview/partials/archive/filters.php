<?php
/* @var array $taxList */
/* @var string $postType */
/* @var bool $searchEnabled */

if ( ! isset( $searchEnabled ) ) {
    $searchEnabled = true;
}
?>

<div class="archive-filters__inner archive-<?php echo esc_attr( $postType ); ?>-filters__inner">
    <div class="archive-filters__intro-text archive-<?php echo esc_attr( $postType ); ?>-filters__intro-text"><?php esc_html_e( 'Filter By:', 'coreview' ); ?></div>

    <div class="archive-filters__list archive-<?php echo esc_attr( $postType ); ?>-filters__list">
        <div class="archive-filters__list-inner archive-<?php echo esc_attr( $postType ); ?>-filters__list-inner">
            <?php
            // Iterate through our taxonomy list and present a dropdown for each which will then trigger RESTful calls to repopulate an archive block.
            foreach ( $taxList as $tax ) {
                // Grab all the terms.
                $tax_obj = get_taxonomy( $tax );

                $terms = get_terms( [
                    'taxonomy'   => $tax,
                    'hide_empty' => true,
                ] );

                if ( $tax_obj && ! empty( $terms ) && is_array( $terms ) ) {
                    $currentValue = \Coreview\Archives::getCurrentFilterValue( $tax );
                    ?>
                    <label>
                        <span class="screen-reader-text"><?php printf( esc_html__( 'Filter by %s', 'coreview' ), esc_html( $tax_obj->labels->singular_name ) ); ?></span>
                        <span class="archive-select-filter-wrapper">
                            <select id="archive-filter--<?php echo esc_attr( $tax ); ?>"
                                    class="archive-filter archive-select-filter archive-filter--<?php echo esc_attr( $tax ); ?> archive-<?php echo esc_attr( $postType ); ?>--<?php echo esc_attr( $tax ); ?>"
                                    data-query-var="<?php echo esc_attr( $tax ); ?>">
                                <option value=""><?php echo esc_html( $tax_obj->labels->all_items ); ?></option>
                                <?php foreach ( $terms as $filter_term ) { ?>
                                    <option value="<?php echo esc_attr( $filter_term->term_id ); ?>" <?php selected( $filter_term->term_id, $currentValue ); ?>><?php echo esc_html( $filter_term->name ); ?></option>
                                <?php } ?>
                            </select>
                        </span>
                    </label>
                    <?php
                }
            }
            ?>
        </div>
    </div>

    <?php
    if ( $searchEnabled ) {
        ?>
        <div class="archive-filters__search-wrap archive-<?php echo esc_attr( $postType ); ?>-filters__search-wrap">
            <div class="archive-filters__search archive-<?php echo esc_attr( $postType ); ?>-filters__search">
                <div class="coreview-search-form">
                    <label class="coreview-search-form__label">
                        <span class="coreview-search-form__label-text screen-reader-text"><?php esc_html_e( 'Search Archives', 'coreview' ); ?></span>
                        <input class="coreview-search-form__input"
                            type="text" name="search" autocomplete="off"
                            placeholder="<?php esc_attr_e( 'Search', 'coreview' ); ?>"
                            value="<?php echo esc_attr( filter_input( INPUT_GET, 'search', FILTER_SANITIZE_STRING ) ); ?>" />
                    </label>
                    <div class="wp-block-button cv-button">
                        <button type="button" class="coreview-search-form__submit wp-block-button__link has-white-color has-border has-background has-blue-background-color" value="<?php esc_attr_e( 'Search', 'coreview' ); ?>">
                            Search</button>
                    </div>
                </div>

            </div>
        </div>
        <?php
    }
    ?>
</div>
