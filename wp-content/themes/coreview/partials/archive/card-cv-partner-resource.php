<?php

/* @var \Coreview\Models\PartnerResource $article */

if ( ! $article->canUserView() ) {
	return;
}

\Coreview\loadTemplateWithContext( 'partials/archive/card-cv-resource.php', [
	'article' => $article,
] );
