<?php

/* @var \Coreview\Models\Event $article */

$postType = get_post_type( $article->post );
$postTypeCard = "card-".$postType;
$postLabel = "Event";
$postSlug = "event";

?>

<article class="grid__col archive-card card-<?php echo $postTypeCard; ?> <?php echo $postTypeCard;?>__type--<?php echo $postSlug?>">
    <div class="archive-card__inner grid__row card-<?php echo $postTypeCard; ?>__inner">
        <div class="grid__col col-6/12@sm">
            <div class="card-<?php echo $postTypeCard; ?>__image">
                <div class="card-<?php echo $postTypeCard; ?>__image-inner">
                    <a href="<?php echo esc_url( $article->postPermalink() ); ?>">
                        <?php echo $article->featuredImage( "medium_large"); ?>
                    </a>
                </div>
            </div>
        </div>
        <div class="grid__col col-6/12@sm">
            <div class="card-<?php echo $postTypeCard; ?>__text">
                <div class="card-<?php echo $postTypeCard; ?>__meta">
                    <p><?php echo $postLabel?></p>
                </div>
                <h3 class="card-<?php echo $postTypeCard; ?>__title">
                    <a href="<?php echo esc_url( $article->postPermalink() ); ?>">
                        <?php echo esc_html( $article->postTitle() ); ?>
                    </a>
                </h3>
                <div class="card-<?php echo $postTypeCard; ?>__excerpt">
                    <b><?php echo $article->eventStartDate();?> - </b>
                    <?php echo esc_html( $article->trimmedExcerpt( 18 ) ); ?>
                </div>
                <div class="card-<?php echo $postTypeCard;?>__cta">
                    <a href="<?php echo $article->url();?>">
                        Read More
                        <?php echo file_get_contents( get_template_directory() . "/assets/img/background-icons/link-arrow.svg")?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</article>
