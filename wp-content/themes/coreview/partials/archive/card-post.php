<?php

/* @var \Coreview\Models\Post $article */

$postType = get_post_type( $article->post );
$postTypeCard = "card-".$postType;
$postLabel = "Blog Post";
$postSlug = "blog-post";

?>


<article class="grid__col col-6/12@sm col-4/12@md archive-card <?php echo $postTypeCard;?> <?php echo $postTypeCard;?>__type--<?php echo $postSlug?>">
    <div class="<?php echo $postTypeCard;?>__card-background">
        <div class="<?php echo $postTypeCard;?>__card-image <?php echo $postTypeCard;?>__card-image--<?php echo $postSlug?>"></div>
        <div class="<?php echo $postTypeCard;?>__big-circle <?php echo $postTypeCard;?>__big-circle--<?php echo $postSlug?>"></div>
        <div class="<?php echo $postTypeCard;?>__big-square <?php echo $postTypeCard;?>__big-square--<?php echo $postSlug?>"></div>
        <div class="<?php echo $postTypeCard;?>__small-square <?php echo $postTypeCard;?>__small-square--<?php echo $postSlug?>"></div>
        <div class="<?php echo $postTypeCard;?>__coreview-c <?php echo $postTypeCard;?>__coreview-c--<?php echo $postSlug?>"></div>
        <div class="<?php echo $postTypeCard;?>__dotted-square <?php echo $postTypeCard;?>__dotted-square--<?php echo $postSlug?>"></div>
        <div class="<?php echo $postTypeCard;?>__card-content">
            <div class="<?php echo $postTypeCard;?>__card--type <?php echo $postTypeCard;?>__tag--<?php echo $postSlug?>">
                <a><?php echo $postLabel; ?></a>

            </div>
            <div class="<?php echo $postTypeCard;?>__card-title">
                <a href="<?php echo $article->url()?>"><?php echo wp_trim_words($article->postTitle())?></a>
            </div>
            <div class="<?php echo $postTypeCard;?>__card-body">
                <p><?php echo wp_trim_words($article->postExcerpt(), 20);?></p>
            </div>
            <div class="<?php echo $postTypeCard;?>__cta">
                <a href="<?php echo $article->url();?>">
                    Read More
					<?php echo file_get_contents( get_template_directory() . "/assets/img/background-icons/link-arrow.svg")?>
                </a>
            </div>
        </div>
    </div>
</article>
