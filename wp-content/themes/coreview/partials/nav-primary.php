<?php
if ( ! has_nav_menu( 'nav-primary' ) ) {
	return;
}
?>
<nav class="nav-primary" aria-label="<?php esc_attr_e( 'Primary navigation', 'coreview' ); ?>">

	<?php
	wp_nav_menu( [
		'theme_location' => 'nav-primary',
		'menu_id'        => 'nav-primary',
		'menu_class'     => 'menu cv-main-menu',
		'depth'          => 3,
		'walker' => new WP_Walker_Nav_Menu(),
	] );
	class WP_Walker_Nav_Menu extends Walker_Nav_Menu {
		function start_lvl( &$output, $depth = 0, $args = array() ) {
			// Depth-dependent classes.
			$indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
			$display_depth = ( $depth + 1);// because it counts the first submenu as 0
			$classes = array(
				'sub-menu',
				( $display_depth % 2  ? 'sub-menu-odd' : 'sub-menu-even' ),
				( $display_depth >=2 ? 'sub-sub-menu' : '' ),
				'sub-menu-depth-' . $display_depth,
			);
			$class_names = implode( ' ', $classes );
			// Build HTML for output.
			$output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";
		}
	}
	?>

</nav>
