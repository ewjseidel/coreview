<?php
if ( ! has_nav_menu( 'nav-products' ) ) {
	return;
}
?>

<span class="menu-label-product"><?php esc_html_e( 'Explore Coreview:', 'coreview' ); ?></span>

<div class="nav-products" aria-label="<?php esc_attr_e( 'Products navigation', 'coreview' ); ?>">

	<?php
	wp_nav_menu( [
		'theme_location' => 'nav-products',
		'menu_id'        => 'nav-products',
		'menu_class'     => 'menu menu-inline',
	] );
	?>

</div>
