<?php
if ( ! has_nav_menu( 'nav-utility' ) ) {
	return;
}
?>
<div class="nav-utility" aria-label="<?php esc_attr_e( 'Utility navigation', 'coreview' ); ?>">

	<?php
	wp_nav_menu( [
		'theme_location' => 'nav-utility',
		'menu_id'        => 'nav-utility',
		'menu_class'     => 'menu menu-inline nav-utility',
	] );
	?>

</div>
