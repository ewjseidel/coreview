<div class="portal-contacts">
	<div class="portal-contacts__inner">
		<div class="portal-contacts__subhead">
			<p>CoreView Contacts</p>
		</div>
		<div class="portal-contacts__body">
			<div class="portal-contacts__heading">
				<h2>North America</h2>
			</div>
			<div class="portal-contacts__collection">
				<div class="portal-contacts-item">
					<div class="portal-contacts-item__heading">
						<p>Don Landers</p>
					</div>
					<div class="portal-contacts-item__copy">
                        <p>Channel & Partner Director</p>
                        <p>Boston, MA</p>
                        <p><a href="mailto:don.landers@coreview.com">don.landers@coreview.com</a></p>
                        <p>(978) 609 - 0199</p>
					</div>
				</div>
                <div class="portal-contacts-item">
                    <div class="portal-contacts-item__heading">
                        <p>Marcus Conroy</p>
                    </div>
                    <div class="portal-contacts-item__copy">
                        <p>Vice President, North America Sales</p>
                        <p>Dallas, TX</p>
                        <p><a href="mailto:marcus.conroy@coreview.com">marcus.conroy@coreview.com</a></p>
                        <p>(972) 310 - 4898</p>
                    </div>
                </div>
			</div>
		</div>
	</div>

</div>