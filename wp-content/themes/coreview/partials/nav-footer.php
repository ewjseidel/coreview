<?php
if ( ! has_nav_menu( 'nav-footer-quick' ) && ! has_nav_menu( 'nav-footer-detail' ) ) {
	return;
}
?>
<div class="nav-footer" aria-label="<?php esc_attr_e( 'Legal navigation', 'coreview' ); ?>">

	<?php

	wp_nav_menu( [
		'theme_location' => 'nav-footer-detail',
		'menu_id'        => 'nav-footer-detail',
		'menu_class'     => 'menu menu-detail',
		'depth'          => 2,
	] );
	?>

</div>
