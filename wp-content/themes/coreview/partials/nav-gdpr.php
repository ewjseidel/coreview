<?php
if ( ! has_nav_menu( 'nav-gdpr' ) ) {
	return;
}
?>
<div class="nav-gdpr" aria-label="<?php esc_attr_e( 'GDPR navigation', 'coreview' ); ?>">
	<div class="nav-gdpr__body">
		<p>We use cookies to ensure that we give you the best experience on our website. If you continue to use this site we will assume that you are happy with it.</p>
	</div>
	<div class="nav-gdpr__buttons">
		<?php
			wp_nav_menu( [
				'theme_location' => 'nav-gdpr',
				'menu_id'        => 'nav-gdpr',
				'menu_class'     => 'menu',
			] );
		?>

	</div>


</div>
