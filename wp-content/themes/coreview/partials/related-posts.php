<?php
use Coreview\Collections\PostCollection;
use Coreview\Models\Post;

use function Coreview\loadTemplateWithContext;
use function wpscholar\WordPress\relatedPosts;

if ( ! is_singular( "post" ) ) {
	return;
}

$relatedPostIds = relatedPosts( 3, get_post() );
if ( empty( $relatedPostIds ) ) {
	return;
}
$relatedPosts = new PostCollection();
$relatedPosts->populate( $relatedPostIds );

if ( $relatedPosts->count() ) {
	?>
    <div class="related-posts">
        <div class="related-posts__header">
            <p>Related Posts</p>
        </div>
        <div class="grid__row related-posts__card-grid">
	        <?php
	        foreach ( $relatedPosts as $relatedPost ) {
		        $post = $relatedPost->post;
		        $GLOBALS['post'] = $post;
		        setup_postdata( $post ); ?>
                <?php loadTemplateWithContext("partials/archive/card-{$postType}", [
			        'article'     => $relatedPost,
                    'postType'    => $postType,
		        ] );

		        wp_reset_postdata();
	        }
	        ?>


        </div>
    </div>


	<?php
}; ?>




