<div class="portal-deal-form">
	<div class="portal-deal-form__inner">
		<div class="portal-deal-form__subhead">
			<p>Register</p>
		</div>
		<div class="portal-deal-form__body">
			<div class="portal-deal-form__heading">
				<h2>Register a Potential Deal or Opportunity</h2>
			</div>
			<div class="portal-deal-form__collection">
				<?php
					use FlexFields\Forms\FormHandler;
					$formName = basename( __FILE__, '.php' );
					$formHandler = FormHandler::getInstance();
					$form = $formHandler->forms->getForm( $formName );
					if ( $form ) {
						$form->render();
					}
				?>
			</div>
		</div>
	</div>
</div>