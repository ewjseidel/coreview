<?php
if ( ! has_nav_menu( 'nav-gdpr' ) ) {
	return;
}
?>
<div class="nav-legal" aria-label="<?php esc_attr_e( 'Legal navigation', 'coreview' ); ?>">

	<?php
	wp_nav_menu( [
		'theme_location' => 'nav-legal',
		'menu_id'        => 'nav-legal',
		'menu_class'     => 'menu',
	] );
	?>

</div>
