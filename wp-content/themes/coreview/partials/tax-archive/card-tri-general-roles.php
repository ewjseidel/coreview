<?php

/* @var \wpscholar\WordPress\PostModelBase $article */

$taxonomy = 'tri-general-role';

?>

<article class="archive-card card-<?php echo esc_attr( $taxonomy ); ?>">
    <div class="archive-card__inner card-<?php echo esc_attr( $taxonomy ); ?>__inner">
		<?php
		if ( $article->hasPostThumbnail() ) {
			?>
            <div class="card-<?php echo esc_attr( $taxonomy ); ?>__image">
                <a href="<?php echo esc_url( $article->url() ); ?>" aria-hidden="true">
					<?php echo $article->postThumbnail( 'logo' ); ?>
                </a>
            </div>
			<?php
		}
		?>
        <div class="card-<?php echo esc_attr( $taxonomy ); ?>__text">
            <h3 class="card-<?php echo esc_attr( $taxonomy ); ?>__title">
                <a href="<?php echo esc_url( $article->url() ); ?>" aria-hidden="true">
					<?php echo esc_html( $article->title() ); ?>
                </a>
            </h3>
            <div class="card-<?php echo esc_attr( $taxonomy ); ?>__excerpt">
				<?php echo esc_html( $article->trimmedExcerpt( 20 ) ); ?>
            </div>
            <div class="card-<?php echo esc_attr( $taxonomy ); ?>__more-link">
                <a href="<?php echo esc_url( $article->url() ); ?>"
                   class="card-<?php echo esc_attr( $taxonomy ); ?>__more">
					<?php esc_html_e( 'Read More', 'coreview' ); ?>
                </a>
            </div>
        </div>
    </div>
</article>
