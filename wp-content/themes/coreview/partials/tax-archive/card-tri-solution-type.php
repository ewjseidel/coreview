<?php

/* @var \Coreview\Models\Solution $article */

?>
<article class="archive-card card-tri-solution-type">

    <a href="<?php echo esc_url( $article->url() ); ?>" class="archive-card__inner card-tri-solution-type__inner">

        <h3 class="card-tri-solution-type__title"><?php echo esc_html( $article->title() ); ?></h3>

        <p class="card-tri-solution-type__excerpt">
			<?php echo esc_html( $article->trimmedExcerpt( 20 ) ); ?>
        </p>

    </a>

</article>
