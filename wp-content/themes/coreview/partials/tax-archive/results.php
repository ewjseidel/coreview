<?php
/**
 * This file should not contain any markup. It is a highly reusable layer between a collection and archive cards.
 */

/* @var \wpscholar\WordPress\PostCollectionBase $results */
/* @var string $fileName */

if ( $results->count() ) {
	foreach ( $results as $article ) {
		$post = $article->post;
		$GLOBALS['post'] = $post;
		setup_postdata( $post );
		\Coreview\loadTemplateWithContext( "partials/tax-archive/{$fileName}", [
			'article' => $article,
		] );
		wp_reset_postdata();
	}
}