<div class="site-header__inner">

    <div class="site-header__utility-bar hide-on-mobile">

        <div class="container container--breakout">

            <div class="grid__row">

                <div class="grid__col col-6/12">
<!--                    --><?php //get_template_part( 'partials/nav', 'utility' ); ?>
                </div>

            </div>

        </div>

    </div>

    <div class="site-header__primary-bar">

        <div class="container container--breakout">

            <div class="grid__row">

                <div class="grid__col col-4/12@md col-auto">
                    <?php get_template_part( 'partials/site-branding' ); ?>
                </div>

                <div class="grid__col col-8/12@md col-auto hide-on-mobile">
	                <?php get_template_part( 'partials/nav', 'utility' ); ?>

	                <?php get_template_part( 'partials/nav', 'primary' ); ?>
                </div>

                <div class="grid__col col-auto show-on-mobile cv-mobile-nav__menu">
                    <button>
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                            viewBox="0 0 32 25" height="25px" width="32px" style="enable-background:new 0 0 32 25;" 
                            xml:space="preserve">
                            <rect width="32" height="3"/>
                            <rect y="11" width="32" height="3"/>
                            <rect y="22" width="32" height="3"/>
                        </svg>
                        <span class='screen-reader-text'>Open Menu</span>
                    </button>
                </div>

            </div>

        </div>

    </div>

</div>