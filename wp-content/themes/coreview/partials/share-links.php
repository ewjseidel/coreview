<?php

use wpscholar\WordPress\SocialSharing;

?>
<h5 class="coreview-share-links__title"><?php esc_html_e( 'Share:', 'coreview' ); ?></h5>
<ul class="coreview-share-links">
    <li>
        <a target="_blank" rel="noopener noreferrer" href="<?php echo esc_url( SocialSharing::facebook() ); ?>">
            <span class="fab fa-facebook-f"></span>
            <span class="screen-reader-text"><?php esc_html_e( 'Share on Facebook', 'coreview' ); ?></span>
        </a>
    </li>
    <li>
        <a target="_blank" rel="noopener noreferrer" href="<?php echo esc_url( SocialSharing::twitter() ); ?>">
            <span class="fab fa-twitter"></span>
            <span class="screen-reader-text"><?php esc_html_e( 'Share on Twitter', 'coreview' ); ?></span>
        </a>
    </li>
    <li>
        <a target="_blank" rel="noopener noreferrer" href="<?php echo esc_url( SocialSharing::linkedin() ); ?>">
            <span class="fab fa-linkedin-in"></span>
            <span class="screen-reader-text"><?php esc_html_e( 'Share on LinkedIn', 'coreview' ); ?></span>
        </a>
    </li>
</ul>
