<?php
use Coreview\Collections\NewsCollection;
use Coreview\PostTypes\NewsItems;
use Coreview\Models\NewsItem;

use function Coreview\loadTemplateWithContext;
use function wpscholar\WordPress\relatedPosts;

if ( ! is_singular( "cv-news-item" ) ) {
	return;
}

$relatedPostIds = relatedPosts( 3, get_post() );
if ( empty( $relatedPostIds ) ) {
	return;
}
$relatedPosts = new NewsCollection();
$relatedPosts->populate( $relatedPostIds );

if ( $relatedPosts->count() ) {
	?>

    <div class="related-announcements">
        <div class="related-announcements__header">
            <p>Related News</p>
        </div>
        <div class="grid__row related-announcements__card-grid">
			<?php
			foreach ( $relatedPosts as $relatedPost ) {
				$post = $relatedPost->post;
				$GLOBALS['post'] = $post;
				setup_postdata( $post ); ?>
                <div class="grid__col col-4/12@lg related-announcements__card">
                    <div class="related-announcements__card-image">
	                    <?php echo $relatedPost->featuredImage( "medium"); ?>

                    </div>
                    <div class="related-announcements__card--type">
                        <p><?php echo $relatedPost->type();?></p>
                    </div>
                    <div class="related-announcements__card-title">
                        <a href="<?php echo esc_url( $relatedPost->postPermalink() ); ?>">
		                    <?php echo esc_html( $relatedPost->postTitle() ); ?>
                        </a>
                    </div>
                    <div class="related-announcements__card-body">
	                    <?php echo esc_html( $relatedPost->trimmedExcerpt( 18 ) ); ?>

                    </div>
                    <div class="related-announcements__cta">
                        <a href="#">
                            Learn More
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 height="10px" width="11px" viewBox="0 0 11 10" style="enable-background:new 0 0 11 10;" xml:space="preserve">
						<rect y="4" width="10.3" height="1"/>
                                <polygon points="6.1,9 5.4,8.3 9.5,4.5 5.4,0.8 6.1,0 11,4.5 "/>
					</svg>
                        </a>
                    </div>

                </div>

				<?php wp_reset_postdata();
			}
			?>
        </div>
    </div>
	<?php
}; ?>

