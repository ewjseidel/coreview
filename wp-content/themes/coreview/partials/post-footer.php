<?php

$footers = \Coreview\PostTypeFooters::getPostFooters();

if ( ! empty( $footers ) ) {
	foreach ( $footers as $footer ) {
		$post = $footer;
		$GLOBALS['post'] = $post;
		setup_postdata( $post );
		the_content();
		wp_reset_postdata();
	}
}
