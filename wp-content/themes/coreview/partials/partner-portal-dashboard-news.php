
<div class="portal-news">
	<div class="portal-news__inner">
		<div class="portal-news__subhead">
			<p>News</p>
		</div>
		<div class="portal-news__body">
			<div class="portal-news__heading">
				<h2><?php echo carbon_get_post_meta(get_the_ID(), "news_heading");?></h2>
			</div>
			<div class="portal-news__collection">
                <?php $newsItems = carbon_get_post_meta(get_the_ID(), "news_item");?>
                <?php foreach( $newsItems as $news ):?>
                <div class="portal-news-item">
                    <div class="portal-news-item__heading">
                        <p><?php echo $news['news_item_heading']?></p>
                    </div>
                    <div class="portal-news-item__copy">
                        <?php echo $news['news_item_body'];?>
                    </div>
                </div>
                <?php endforeach;?>
			</div>
		</div>
	</div>

</div>