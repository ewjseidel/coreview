
<div class="portal-resource-collection">
	<div class="portal-resource-collection__inner">
		<div class="portal-resource-collection__subhead">
			<p>Partner Resources</p>
		</div>
		<div class="partner-resource-collection__body">
			<div class="partner-resource-collection__heading">
				<h2>Resources for MSP Partner</h2>
			</div>
			<div class="partner-resource-collection__item-wrapper">
                <?php
                $terms = get_terms('cv-partner-resource-type');
                foreach($terms as $term) {
	                $posts = get_posts(array(
		                'post_type' => 'cv-partner-resource',
		                'tax_query' => array(
			                array(
				                'taxonomy' => 'cv-partner-resource-type',
				                'field' => 'slug',
				                'terms' => $term->slug
			                )
		                ),
		                'numberposts' => -1
	                ));

	                ?>
                        <div class="partner-resource-collection__item">
                            <div class="partner-resource-collection__resource-type">

                                <div class="partner-resource-collection__resource-label">
                                    <h3><?php echo $term->name;?></h3>
                                </div>
                                <div class="partner-resource-collection__resource-list">
                                    <ul>
                                <?php
                                if ( $term->slug == "demo-video" ) {
                                    $prefix = "Video";
                                } else {
                                    $prefix = "PDF";
                                }
                                foreach( $posts as $post ) { ?>

                                        <li><a href="<?php echo get_permalink( $post->ID );?>"><?php echo $post->post_title;?> <span><?php echo $prefix;?><?php echo file_get_contents( get_template_directory() . "/assets/img/". $term->slug.".svg");?></span></a></li>

                               <?php }?>
                                    </ul>
                                </div>


                            </div>
                        </div>
	                <?php
                }

                ?>

			</div>
			<div class="wp-block-button cv-button">
				<a class="wp-block-button__link has-white-color has-border has-background has-blue-background-color" href="<?php echo get_site_url()?>/partner-portal-resources">See All Resources</a>
			</div>
		</div>
	</div>
</div>