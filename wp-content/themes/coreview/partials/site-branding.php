<div class="site-branding">
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>"
      class="site-logo"
      title="<?php echo esc_attr( get_bloginfo( 'title' ) ); ?>">
      <span class="hide-on-mobile">
        <?php echo file_get_contents( get_template_directory() . "/assets/img/logo.svg")?>
      </span>
      <span class="show-on-mobile">
        <?php echo file_get_contents( get_template_directory() . "/assets/img/mob-logo.svg")?>
      </span>
    </a>
</div><!-- .site-branding -->