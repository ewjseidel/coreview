<?php

$route = '/wp/v2/deals';
$args = [
	'per_page' => 10,
];
$request = new WP_REST_Request( 'GET', $route );
$request->set_query_params( $args );
$response = rest_do_request( $request );
$deals = rest_get_server()->response_to_data( $response, false );

if ( empty( $deals ) ) {
	return;
}

?>

<div class="portal-deals" id="my-deals">

    <div class="portal-deals__inner">

        <div class="portal-deals__subhead">
            <p>My Deals</p>
        </div>

        <div class="portal-deals__body"
             data-vue-app="load-more"
             data-route="<?php echo esc_attr( $route ); ?>"
             data-query="<?php echo esc_attr( wp_json_encode( $args ) ); ?>"
             data-posts="<?php echo esc_attr( wp_json_encode( $deals ) ); ?>"
             data-total="<?php echo esc_attr( absint( $response->headers['X-WP-Total'] ) ); ?>"
        >

            <div class="portal-deals__heading">
                <h2>Registered Deals</h2>
            </div>

            <div class="portal-deals__table-headings">
                <div class="grid__row">
                    <div class="grid__col col-3/12@sm">
                        <div class="portal-deals__column-heading">Date Requested</div>
                    </div>
                    <div class="grid__col col-3/12@sm">
                        <div class="portal-deals__column-heading">Company Name</div>
                    </div>
                    <div class="grid__col col-3/12@sm">
                        <div class="portal-deals__column-heading">Country</div>
                    </div>
                    <div class="grid__col col-3/12@sm">
                        <div class="portal-deals__column-heading">Number of Licenses</div>
                    </div>
                </div>
            </div>

            <div :class="['portal-deals__deals portal-deals__deals-' + index]" v-for="(deal, index) in posts">

                <div class="grid__row">
                    <div class="grid__col col-3/12@sm" v-html="deal.dateRequested"></div>
                    <div class="grid__col col-3/12@sm" v-html="deal.company_name"></div>
                    <div class="grid__col col-3/12@sm" v-html="deal.location"></div>
                    <div class="grid__col col-3/12@sm" v-html="deal.number_365_licenses"></div>
                </div>

            </div>

            <div class="portal-deals__load-more-wrap" v-if="hasMore">
                <a href="#" class="portal-deals__load-more" @click="fetchPosts">Load More</a>
            </div>

        </div>

    </div>

</div>
