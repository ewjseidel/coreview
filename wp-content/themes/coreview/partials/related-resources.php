<?php
use Coreview\Collections\ResourceCollection;
use Coreview\PostTypes\Resources;
use Coreview\Models\Resource;

use function Coreview\loadTemplateWithContext;
use function wpscholar\WordPress\relatedPosts;

if ( ! is_singular( "cv-resource" ) ) {
	return;
}

$relatedPostIds = relatedPosts( 3, get_post() );
if ( empty( $relatedPostIds ) ) {
	return;
}
$relatedPosts = new ResourceCollection();
$relatedPosts->populate( $relatedPostIds );

if ( $relatedPosts->count() ) {
	?>

    <div class="related-posts">
        <div class="related-posts__header">
            <h1>Related Resources</h1>
        </div>
        <div class="grid__row related-posts__card-grid">
			<?php
			foreach ( $relatedPosts as $relatedPost ) {
				$post = $relatedPost->post;
				$GLOBALS['post'] = $post;
				setup_postdata( $post );
				loadTemplateWithContext("partials/archive/card-{$postType}", [
					'article'     => $relatedPost,
					'postType'    => $postType,
				] );
                wp_reset_postdata();
			}
			?>
        </div>
    </div>
	<?php
}; ?>
