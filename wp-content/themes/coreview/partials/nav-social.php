<?php
if ( ! has_nav_menu( 'nav-social' ) ) {
	return;
}
?>
<div class="nav-social" aria-label="<?php esc_attr_e( 'Social links', 'coreview' ); ?>">

	<?php
	wp_nav_menu( [
		'theme_location' => 'nav-social',
		'menu_id'        => 'nav-social',
		'menu_class'     => 'menu',
		'link_before'    => '<span class="screen-reader-text">',
		'link_after'     => '</span>',
	] );
	?>

</div>
