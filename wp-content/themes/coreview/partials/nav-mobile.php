<?php

use function Coreview\getCFi18nThemeOption;

$associationArray = getCFi18nThemeOption( 'contact_us_page' );
$pageLabel = getCFi18nThemeOption( 'contact_us_page_label' );

$buttons = getCFi18nThemeOption( 'header_nav_buttons' );
?>
<nav class="cv-mobile-nav show-on-mobile"
     aria-label="<?php esc_attr_e( 'Mobile navigation', 'coreview' ); ?>"
	 aria-hidden="true">

	<div class="cv-mobile-nav__actions">
		<button type="button" class="cv-mobile-nav__back">
			<?php echo file_get_contents( get_template_directory() . "/assets/img/background-icons/Back-Arrow.svg")?>
			<span class="screen-reader-text"><?php esc_html_e( 'Go back menu', 'coreview' ); ?></span>
		</button>

		<button type="button" class="cv-mobile-nav__close">
			<?php echo file_get_contents( get_template_directory() . "/assets/img/background-icons/Close.svg")?>
			<span class="screen-reader-text"><?php esc_html_e( 'Close menu', 'coreview' ); ?></span>
		</button>
	</div>

    <div class="cv-mobile-nav__menus-wrap">
		<?php
		$menus = [
			'nav-primary',
            'nav-utility'
		];
		foreach ( $menus as $menu ) {
			wp_nav_menu( [
				'theme_location' => $menu,
				'menu_id'        => '',
				'menu_class'     => "menu {$menu}-mobile",
				'depth'          => 3,
				'walker'         => new \Coreview\MobileNavWalker(),
			] );
		}

		if ( ! empty( $associationArray ) ) {
			?>
            <ul class="menu extra-mobile">
				<?php
				foreach ( $associationArray as $page ) {
					$pageObj = get_post( $page['id'] );
					if ( $pageObj ) {
						?>
                        <a href="<?php echo esc_url( get_permalink( $pageObj ) ); ?>">
							<?php echo ! empty( $pageLabel ) ? esc_html( $pageLabel ) : esc_html( get_the_title( $pageObj ) ); ?>
                        </a>
						<?php
					}
				}
				?>
            </ul>
			<?php
		}
		?>
    </div>

    <div class="cv-mobile-nav__extra">
		<?php
		if ( ! empty( $buttons ) ) {
			foreach ( $buttons as $button ) {
				if ( empty( $button['url'] ) || empty( $button['text'] ) ) {
					continue;
				}
				?>
                <a href="<?php echo esc_url( $button['url'] ); ?>"
                   class="coreview-button coreview-button--style-<?php echo esc_attr( $button['color'] ); ?>">
					<?php echo esc_html( $button['text'] ); ?>
                </a>
				<?php
			}
		}
		?>
    </div>

</nav>
