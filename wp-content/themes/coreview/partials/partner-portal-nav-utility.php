<?php
if ( ! has_nav_menu( 'partner-portal-nav-utility' ) ) {
	return;
}

$user = wp_get_current_user();
$userFirst = $user->user_firstname;
$userLast = $user->user_lastname;

$userName = $userFirst." ".$userLast;

$user = wp_get_current_user();
if ( in_array( 'coreview_pp_reseller', (array) $user->roles ) ) {
	$userRole = "Reseller";
}

if ( in_array( 'coreview_pp_msp', (array) $user->roles ) ) {
	$userRole = "MSP";
}

if ( in_array( 'coreview_pp_blended', (array) $user->roles ) ) {
	$userRole = "Blended";
}

if ( in_array( 'administrator', (array) $user->roles ) ) {
	$userRole = "Administrator ";
}

?>
<nav class="portal-nav-utility" aria-label="<?php esc_attr_e( 'Partner Portal Utility Navigation', 'coreview' ); ?>">

    <ul class="menu portal-utility-nav-user">
        <li class="menu-item portal-utility-nav-user__item">
            <a href=""><?php echo $userName.", ".$userRole;?></a>
        </li>
    </ul>

	<?php
	wp_nav_menu( [
		'theme_location' => 'partner-portal-nav-utility',
		'menu_id'        => 'partner-portal-nav-utility',
		'menu_class'     => 'menu portal-nav-utility',
		'depth'          => 3,
	] );

	?>

</nav>
