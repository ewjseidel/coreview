<?php
$user = wp_get_current_user();
if ( in_array( 'coreview_pp_reseller', (array) $user->roles ) ) {
	$userRole = "Reseller";
}

if ( in_array( 'coreview_pp_msp', (array) $user->roles ) ) {
	$userRole = "MSP";
}

if ( in_array( 'coreview_pp_blended', (array) $user->roles ) ) {
	$userRole = "Blended";
}

if ( in_array( 'administrator', (array) $user->roles ) ) {
	$userRole = "Administrator ";
}
?>

<div class="portal-overview">
	<div class="portal-overview__inner">
		<div class="portal-overview__subhead">
			<p>Program Overview</p>
		</div>
		<div class="portal-overview__body">
			<div class="portal-overview__heading">
				<h2><?php echo $userRole;?> <?php echo carbon_get_post_meta(get_the_ID(), "program_heading")?></h2>
				<?php echo carbon_get_post_meta(get_the_ID(), "prgram_section_copy")?>
			</div>
			<div class="portal-overview__collection">
                <?php $benefitItems = carbon_get_post_meta(get_the_ID(), "benefit_item");?>
                <?php foreach( $benefitItems as $benefit ):?>
                    <div class="portal-overview-item">
                        <div class="portal-overview-item__icon">
			                <?php echo wp_get_attachment_image( $benefit['benefit_icon'])?>
                        </div>
                        <div class="portal-overview-item-content">
                            <div class="portal-overview-item__heading">
                                <h3><?php echo $benefit['benefit_heading']?></h3>
                            </div>
                            <div class="portal-overview-item__copy">
                                <?php echo $benefit['benefit_body'];?>
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>

			</div>
			<div class="wp-block-button cv-button">
				<a class="wp-block-button__link has-white-color has-border has-background has-blue-background-color" href="<?php echo carbon_get_post_meta(get_the_ID(), "button_url")?>"><?php echo carbon_get_post_meta(get_the_ID(), "button_text")?></a>
			</div>
		</div>
	</div>

</div>