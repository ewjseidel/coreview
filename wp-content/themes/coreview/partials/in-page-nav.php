<?php

/* @var int $menuId */
if ( ! isset( $menuId ) ) {
	return;
}

?>

<div class="wp-block-narwhal-in-page-nav alignfull">

    <div class="wp-block-narwhal-in-page-nav__inner container">

		<?php
		wp_nav_menu( [
			'menu'       => $menuId,
			// ID SHOULD BE EMPTY!
			'menu_id'    => '',
			'menu_class' => 'in-page-nav',
		] );
		?>

    </div>

</div>
