<div class="site-footer__inner container">
    <div class="grid__row footer-branding">
        <div class="grid__col col-6/12@sm site-footer__footer-logo">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                <?php echo file_get_contents( get_template_directory() . "/assets/img/logo.svg")?>
            </a>
        </div>
        <div class="grid__col col-6/12@sm site-footer__contact-numbers">
            <p><span>EMEA</span> +39.028.725.9395</p>
            <p><span>USA</span> +1.800.436.8183</p>
        </div>
    </div>
    <div class="grid__row site-footer__footer-nav">
        <div class="grid__col col-6/12 site-footer__footer-menu">
            <?php get_template_part("partials/nav", "footer")?>
        </div>
        <div class="grid__col offset-1/12@sm col-5/12 site-footer__footer-form">
            <div class="site-footer__signup-form">
                <p>SIGN UP FOR NEWS AND UPDATES</p>
<!--                --><?php //echo do_shortcode('[marketo_form id="1018" redirect-url="/thank-you" /]')?>
                <script src="//app-sjqe.marketo.com/js/forms2/js/forms2.js"></script>

                <form id="mktoForm_1018"></form>
                <script>
                    MktoForms2.loadForm("//app-ab33.marketo.com", "961-GKL-384", 1018);
                </script>
            </div>
            <div class="site-footer__external-links">
                <div class="site-footer__external-links-partner">
                    <img src="<?php echo get_template_directory_uri();?>/assets/img/CV_Icon_Black_CV_MicrosoftCertWhite@2x.png" alt="">
                </div>
                <div class="site-footer__external-links-social">
                    <a href="https://twitter.com/CoreView_inc"><?php echo file_get_contents( get_template_directory() . "/assets/img/Twitter.svg")?></a>
                    <a href="https://www.linkedin.com/company/4ward365"><?php echo file_get_contents( get_template_directory() . "/assets/img/LinkedIn.svg")?></a>
                </div>
            </div>
        </div>
    </div>
    <div class="grid__row footer-colophon">
        <div class="grid__col">
            <p class="site-footer__copyright"><?php esc_html_e( 'Copyright &copy; CoreView. All rights reserved. VAT number: 10146510960 – Registered Office: Via Pier Luigi da Palestrina 33 – 20124 Milano (Italy)', 'coreview' ); ?></p>
        </div>
    </div>
</div>
<div class="footer-gdpr">
    <div class="footer-gdpr__inner">
		<?php get_template_part("partials/nav", "gdpr")?>
    </div>
</div>