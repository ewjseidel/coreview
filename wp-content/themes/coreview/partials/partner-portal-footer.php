<div class="portal-site-footer__inner container">
	<div class="grid__row portal-footer-branding">
		<div class="grid__col col-6/12@xs portal-site-footer__footer-logo">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
				<?php echo file_get_contents( get_template_directory() . "/assets/img/logo.svg")?>
			</a>
		</div>
		<div class="grid__col col-6/12@xs portal-site-footer__colophon">
			<p class="portal-site-footer__copyright"><?php esc_html_e( 'Copyright &copy; CoreView 2019. All rights reserved.', 'coreview' ); ?></p>
		</div>
	</div>
</div>