<?php

use function Coreview\getCFi18nThemeOption;

$associationArray = getCFi18nThemeOption( 'contact_us_page' );
$pageLabel = getCFi18nThemeOption( 'contact_us_page_label' );

$buttons = getCFi18nThemeOption( 'header_nav_buttons' );

?>
<div class="nav-secondary hide-on-mobile">

	<?php
	if ( ! empty( $associationArray ) ) {
		foreach ( $associationArray as $page ) {
			$pageObj = get_post( $page['id'] );
			if ( $pageObj ) {
				?>
                <a href="<?php echo esc_url( get_permalink( $pageObj ) ); ?>">
					<?php echo ! empty( $pageLabel ) ? esc_html( $pageLabel ) : esc_html( get_the_title( $pageObj ) ); ?>
                </a>
				<?php
			}
		}
	}
	if ( ! is_search() ) {
		?>
        <div class="coreview-header-search">
			<?php get_search_form(); ?>
        </div>
		<?php
	}
	if ( ! empty( $buttons ) ) {
		foreach ( $buttons as $button ) {
			if ( empty( $button['url'] ) || empty( $button['text'] ) ) {
				continue;
			}
			?>
            <a href="<?php echo esc_url( $button['url'] ); ?>"
               class="coreview-button coreview-button--style-<?php echo esc_attr( $button['color'] ); ?>">
				<?php echo esc_html( $button['text'] ); ?>
            </a>
			<?php
		}
	}
	?>

</div>
