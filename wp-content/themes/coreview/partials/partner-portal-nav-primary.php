<?php
if ( ! has_nav_menu( 'partner-portal-nav-primary' ) ) {
	return;
}
?>
<nav class="portal-nav-primary" aria-label="<?php esc_attr_e( 'Partner Portal Primary Navigation', 'coreview' ); ?>">

	<?php
	wp_nav_menu( [
		'theme_location' => 'partner-portal-nav-primary',
		'menu_id'        => 'partner-portal-nav-primary',
		'menu_class'     => 'menu partner-portal-nav-primary',
		'depth'          => 3,
	] );

	?>

</nav>
