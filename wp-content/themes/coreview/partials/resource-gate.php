<div class='resource-gate'>
    <div class="resource-gate__form">
        <h4>Continue Reading</h4>
        <span>We just need a little info from you</span>

        <?php echo do_shortcode('[marketo_form id="' . carbon_get_theme_option( 'marketo_progressive' ) . '" redirect-callback="removeGate" /]'); ?>
    </div>
</div>
