<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="canonical" href="<?php echo get_permalink(get_the_ID());?>" />
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-THW4L8H');</script>
        <!-- End Google Tag Manager -->

		<?php wp_head(); ?>
        <link rel="stylesheet" href="https://use.typekit.net/vui0zyj.css">
        <link rel="icon" href="<?php echo get_site_url();?>/favicon.png" type="image/x-icon" />
        <link rel="shortcut icon" href="<?php echo get_site_url();?>/favicon.png" type="image/x-icon" />


        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-49087358-4"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-49087358-4');
        </script>

    </head>

    <?php
        $ieClass = ""; 
        if (preg_match('~MSIE|Internet Explorer~i', $_SERVER['HTTP_USER_AGENT']) || (strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') !== false)) { 
            $ieClass = " site-ie"; 
        }
    ?>
    <body itemscope itemtype="http://schema.org/WebPage" <?php body_class(); ?>>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-THW4L8H"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
        <div id="page" class="site<?= $ieClass ?>" >
            <header itemscope itemtype="http://schema.org/WPHeader" class="site-header <?php echo headerClass();?> " role="banner">
				<?php get_template_part( 'partials/header' ); ?>
            </header><!-- .site-header -->

            <main id="content" class="site-content">

                <div class="container">