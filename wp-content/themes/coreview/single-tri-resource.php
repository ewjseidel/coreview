<?php

the_post();
get_header();

$resitem = Coreview\Models\Resource::create(get_post());
?>

<article class='single_resource_template <?php echo esc_attr($resitem->resourceClasses()); ?>'>
    <div class='single_resource_template__inner'>
        <header class='single_resource_template__post-header'>
	        <?php echo $resitem->featuredImage( 'masthead' ); ?>
            <div class='container container--breakout'>
                <div class='grid__row'>
                    <div class='grid__col'>
                        <ul class='single_resource_template__post-header__breadcrumbs coreview__breadcrumbs'>
				            <?php coreviewBreadcrumbs(); ?>
                        </ul>
                    </div>
                </div>
                <div class='grid__row'>
                    <div class='grid__col'>
                        <h1 class='single_resource_template__post-header__post-title'><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
        </header>

        <div class="resource-with-sidebar">

            <div class="resource-with-sidebar__inner">

                <div class="resource-with-sidebar__content">
                    <div class="resource-with-sidebar__content-inner">
                        <div class='single_resource_template__post-content'>
                            <div class="coreview-blocks coreview-blocks--in-template">
							    <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="resource-with-sidebar__left">
                    <aside class='single_resource_template__sidebar resource-with-sidebar__left-inner'>

                    </aside>
                </div>

                <div class="resource-with-sidebar__right">
                    <aside class='single_resource_template__right-rail resource-with-sidebar__right-inner'>
                        <div class='single_resource_template__right-rail__related-content-placeholder'>
                            <div data-content-position-id="<?php echo carbon_get_theme_option( 'resource_sidebar_position' )[0]['id']; ?>"></div>
                        </div>
                        <div class='single_resource_template__right-rail__never-miss-placeholder'>
                            <div class="marketo-form-sidebar">
                                <h6 class="marketo-form-sidebar__heading">Never Miss an Update</h6>
                                <p class="marketo-form-sidebar__subheading">Sign Up for Our Newsletter</p>
			                    <?php echo do_shortcode('[marketo_form id="' . carbon_get_theme_option( 'marketo_resource_sidebar' ) . '" redirect-url="SETDYNAMICALLYLATER" /]'); ?>
                            </div>
                        </div>
                        <section class='single_resource_template__right-rail__social-section'>
		                    <?php get_template_part( 'partials/share-links' ); ?>
                        </section>
                    </aside>
                </div>

            </div>

        </div>
    </div>
    <?php
        if ($resitem->isGated()) {
            get_template_part( 'partials/resource-gate' );
        }
    ?>
</article>

<?php
get_footer();
