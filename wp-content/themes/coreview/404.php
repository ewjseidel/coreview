<?php

get_header();

?>

    <div class='template-404 wp-block-narwhal-background alignfull has-background has-grey-light-background-background-color has-text-color has-white-color'>
        <div class='wp-block-narwhal-background__inner template-404__inner'>
            <div class='template-404__text-column'>
                <h1>404</h1>
                <h2>Uh oh! Something's not right.</h2>
                <div class='wp-block-button-cv-button'>
                    <a class='wp-block-button__link has-white-color has-border has-background has-blue-background-color' href='/'>
                        Back To The Homepage
                    </a>
                </div>
            </div>
            <div class='template-404__image-column'>
                <img src='<?php echo get_stylesheet_directory_uri() . '/assets/img/404-Composite.png' ?>'/>
            </div>
        </div>
        <div class='wp-block-narwhal-background__background-underlay'>
            <img class='wp-block-narwhal-background__image' src='<?php echo get_stylesheet_directory_uri() . '/assets/img/404-background.png' ?>'/>
            <div class="wp-block-narwhal-background__image-overlay has-overlay has-dark-overlay" style="opacity:0.8"></div>

        </div>
    </div>



<?php

get_footer();
