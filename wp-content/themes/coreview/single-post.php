<?php

the_post();
get_header();

$postData = \Coreview\Models\Post::create(get_post());

$postTags = $postData->getTags();

$postType = 'post';


?>

    <div class="grid__row">
        <div class="grid__col col-10/12@lg offset-1/12@lg">

            <article class='single-cv-post post-type-template'>
                <header class='single-cv-post__post-header'>
                    <div class="single-cv-post__tax-title alignfull">
                        <div class="container">
                            <p>

	                            <?php if ($postData->getPreviousPost()):?>
                                    <a class="single-cv-news-item__tax-title-prev" href="<?php echo get_the_permalink($postData->getPreviousPost())?>">
                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" height="14px" width="8px" viewBox="0 0 8 14" style="enable-background:new 0 0 8 14;" xml:space="preserve">
                                            <polygon points="8,0.3 8,13.7 0,7 "></polygon>
                                        </svg>
                                    </a>
	                            <?php endif;?>
                                Blog
                                <?php if ($postData->getNextPost()):?>
                                <a class="single-cv-news-item__tax-title-next" href="<?php echo get_the_permalink($postData->getNextPost())?>">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" height="14px" width="8px" viewBox="0 0 8 14" style="enable-background:new 0 0 8 14;" xml:space="preserve">
                                        <polygon points="0,13.7 0,0.3 8,7 "></polygon>
                                    </svg>
                                </a>
                                <?php endif;?>
                            </p>
							<?php //TODO: Add primary taxonomy tag ?>
                        </div>
                    </div>

                    <?php ?>
                    <div class='single-cv-post__breadcrumbs'>
                        <ul class=''>
							<?php //TODO: Add breadcrumb package ?>
                            <li><a href="<?php echo get_site_url();?>">Home</a></li>
                            <li><a href="<?php echo get_site_url("", "/blog");?>">Blog</a></li>
                            <li><a href="#"><?php echo the_title();?></a></li>
                        </ul>
                    </div>
                </header>

                <div class="single-cv-post__main">
                    <div class="single-cv-post__inner">
                        <div class="single-cv-post__post-date">
                            <p><?php echo get_the_date('M j Y');?></p>

                        </div>
                        <div class="single-cv-post__post-title">
                            <h1><?php echo $postData->PostTitle();?></h1>
                        </div>
                        <div class="single-cv-post__post-author">
                            <p>By: <a href="<?php echo $postData->getAuthorLinkedIn();?>" target="_blank"><?php echo $postData->getAuthorFullName();?></a></p>
                        </div>

                        <div class="single-cv-post__content">
							<?php echo $postData->postContent(); ?>


                        </div>

	                    <?php
	                    if ( ! empty( $postTags ) ): ?>
                            <div class="single-cv-post__tags">
                                <h2>Tags</h2>
                                <ul class="">
				                    <?php foreach ( $postTags as $tag ):
					                    ?>
                                        <li><a href="<?php echo site_url("/resources/?general-tags=$tag->term_id#archive-results"); ?>">
							                    <?php echo $tag->name; ?></a></li>
				                    <?php endforeach; ?>
                                </ul>
                            </div>
	                    <?php endif;?>

                        <div class="single-cv-post__social">
                            <p>Share</p>
                            <div class="single-cv-post__social-icons">
                                <div class="single-cv-post__social-icon">
<!--                                    <img src="--><?php //echo get_template_directory_uri();?><!--/assets/img/Twitter.svg" alt="">-->
                                    <a class="share-twitter" href="<?= esc_url( \wpscholar\WordPress\SocialSharing::twitter() ); ?>">
                                        <span class="fa fa-twitter"></span>
                                    </a>
                                </div>
                                <div class="single-cv-post__social-icon">
<!--                                    <img src="--><?php //echo get_template_directory_uri();?><!--/assets/img/LinkedIn.svg" alt="">-->
                                    <a class="share-linkedin" href="<?= esc_url( \wpscholar\WordPress\SocialSharing::linkedin() ); ?>">
                                        <span class="fa fa-linkedin-square"></span>
                                    </a>
                                </div>
                                <div class="single-cv-post__social-icon">
<!--                                    <img src="--><?php //echo get_template_directory_uri();?><!--/assets/img/Facebook.svg" alt="">-->
                                    <a class="share-facebook" href="<?= esc_url( \wpscholar\WordPress\SocialSharing::facebook() ); ?>">
                                        <span class="fa fa-facebook-square"></span>
                                    </a>
                                </div>

                            </div>
                        </div>


                    </div>



                </div>
            </article>

        </div>

    </div>

    <div class="grid__row">
        <div class="grid__col">
			<?php \Coreview\loadTemplateWithContext("partials/related-posts", [
			        'postType' => $postType,
            ]) ?>
        </div>

    </div>


<?php get_footer();

