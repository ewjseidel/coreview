import domReady from '@wordpress/dom-ready';
import $ from 'jquery';

domReady(() => {

    const mobileNavOpen = document.querySelector('.cv-mobile-nav__menu');
    const mobileNav = document.querySelector('.cv-mobile-nav');
    const pageWrap = document.querySelector('body > .site');
    const backNav = mobileNav.querySelector('.cv-mobile-nav__back');
    const btnLink = mobileNav.querySelector('.coreview-button a');
    const btnWrapper = mobileNav.querySelector('.cv-mobile-nav__extra');

    if (!mobileNavOpen || !mobileNav) {
        return;
    }

    const navItemsWithChildren = mobileNav.querySelectorAll('li.menu-item-has-children');

    const navOpen = (e) => {
        e.stopPropagation();
        mobileNav.setAttribute('aria-hidden', 'false');
    };

    const navClose = (e) => {
        e.stopPropagation();
        mobileNav.setAttribute('aria-hidden', 'true');
    };

    const subNavToggle = (e) => {
        e.preventDefault();
        e.stopPropagation();
        const navItem = e.target.closest('li');
        navItem.classList.toggle('-sub-menu-active');
        $(backNav).addClass('active');
    };

    if (navItemsWithChildren && navItemsWithChildren.length) {
        Array.from(navItemsWithChildren).forEach((navItem) => {
            const link = navItem.querySelector('a');
            if (link.getAttribute('href') === '#') {
                link.addEventListener('click', subNavToggle);
            } else {
                const toggle = link.querySelector('.cv-mobile-nav__sub-nav-toggle');
                toggle.addEventListener('click', subNavToggle);
            }
        });
    }

    mobileNavOpen.addEventListener('click', navOpen);
    mobileNav.querySelector('.cv-mobile-nav__close').addEventListener('click', navClose);

    if (pageWrap) {
        pageWrap.addEventListener('click', (e) => {
            if ('false' === mobileNav.getAttribute('aria-hidden')) {
                mobileNav.setAttribute('aria-hidden', 'true');
            }
        });
    }

    if (backNav) {
        backNav.addEventListener('click', (e) => {
            $('.-sub-menu-active:last').toggleClass('-sub-menu-active');
            if($('.-sub-menu-active').length < 1)
                $(backNav).removeClass('active');
        });
    }

    if (btnLink) {
        $(btnLink).appendTo($(btnWrapper));
    }
});
