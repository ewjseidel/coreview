import MQ from '@dfwood/js-mq';

const bp = {
    xs: 480,
    sm: 768,
    md: 980,
    lg: 1100,
    xl: 1300,
};

const mq = new MQ(bp);

export {mq, bp};
