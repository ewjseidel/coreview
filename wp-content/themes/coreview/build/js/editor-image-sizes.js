import {addFilter} from '@wordpress/hooks';

addFilter('narwhal.blocks.background.imageSize', 'coreview.blocks.background.imageSize', () => 'masthead');