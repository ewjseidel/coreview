import domReady from '@wordpress/dom-ready';
import queryString from 'qs';
import Vue from 'vue';

domReady(() => {

    const {
        restUrl,
        restNonce
    } = window.coreView;

    document.querySelectorAll('[data-vue-app="load-more"][data-route][data-query]')
        .forEach(function (el) {

            const posts = (() => {
                let posts = el.getAttribute('data-posts');

                if (posts && !Array.isArray(posts)) {
                    posts = JSON.parse(posts);
                }

                posts.forEach(function (post) {
                    let date = new Date(post.date);
                    post.formattedDate = `${(date.getMonth() + 1)}/${date.getDate()}/${date.getFullYear()}`;
                });

                return posts || [];
            })();

            const app = new Vue({
                el,
                data: {
                    posts,
                    view: 'grid',
                    total: Number(el.getAttribute('data-total')),
                    route: el.getAttribute('data-route'),
                    query: JSON.parse(el.getAttribute('data-query')),
                },
                computed: {
                    hasMore: function () {
                        return this.total > this.posts.length;
                    },
                    queryString: function () {
                        const query = this.query;
                        query.offset = this.posts.length;
                        return '?' + queryString.stringify(this.query);
                    },
                    url: function () {
                        return restUrl + this.route.replace(/^\/+/, '') + this.queryString;
                    }
                },
                methods: {
                    fetchPosts: function () {

                        if (arguments && arguments[0] && arguments[0].preventDefault) {
                            arguments[0].preventDefault();
                        }
                        fetch(this.url, {
                            method: 'GET',
                            credentials: 'same-origin',
                            headers: {
                                'X-WP-Nonce': restNonce
                            }
                        })
                            .then((response) => {
                                if (!response.ok) {
                                    throw Error(response.statusText);
                                }
                                return response;
                            })
                            .then(response => {
                                this.total = Number(response.headers.get('X-WP-Total'));
                                return response.json();
                            })
                            .then((posts) => {
                                const dataType = typeof (posts);

                                if (dataType === 'string') {
                                    this.posts = this.posts.concat(JSON.parse(posts));
                                } else {
                                    this.posts = this.posts.concat(posts);
                                }

                                if (!this.posts[0].formattedDate) {
                                    this.posts.forEach(function (post) {
                                        let date = new Date(post.date);
                                        post.formattedDate = `${(date.getMonth() + 1)}/${date.getDate()}/${date.getFullYear()}`;
                                    });
                                }
                            })
                            .catch(err => console.log(err));
                    },
                },
                created: function () {
                    if (!this.total) {
                        this.fetchPosts();
                    }
                }
            });

        });

});
