import {addFilter} from '@wordpress/hooks';

// Filter post types allowed to have an archive...

addFilter('narwhal.blocks.postTypeArchive.allowedPostTypes', 'coreview.blocks.postTypeArchive.allowedPostTypes', () => [
    'post',
    'cv-team-item',
]);


addFilter('narwhal.blocks.postTypeArchive.postTypeOptions', 'coreview.blocks.postTypeArchive.postTypeOptions', (options) => {
    options.push(
        {
        label: 'All Resources',
        value: 'resource-collection',
        },
        {
        label: 'News & Events',
        value: 'news-events',
        }
    );

    return options;
});

/*

addFilter('narwhal.blocks.taxonomyBasedArchive.allowedTaxonomies', 'coreview.blocks.taxonomyBasedArchive.allowedTaxonomies', () => [
    'tri-solution-type',
    'tri-general-roles',
]);
*/
