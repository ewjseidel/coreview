import domReady from '@wordpress/dom-ready';
import {addAction, doAction} from '@wordpress/hooks';
import qs from 'qs';


domReady(() => {

    const archiveWrap = document.querySelector('.archive');

    if (!archiveWrap) {
        // No archive on this page!
        return;
    }

    const {location} = window;
    const originalQueryString = location.search;
    const params = qs.parse(originalQueryString.substr(1));
    const resultsContainer = archiveWrap.querySelector('.archive__results');
    // const {archiveEndpoint, restUrl} = window.tricentisArchives;

    /**
     * Clears all filters and updates the content.
     */
    function clearFilters() {
        doAction('archive.clearFilters');
        // TODO: Run query to fetch unfiltered content.
    }

    function queryStart() {
        doAction('archive.queryStart');
    }

    function queryEnd() {
        doAction('archive.queryEnd');
    }

    function doQuery() {
        queryStart();

        const queryString = qs.stringify(params);
        const resultsSelector = '#archive-results';
        let href = location.href.replace(location.hash, '');

        if (originalQueryString) {
            location.href = href.replace(originalQueryString, (queryString ? '?' + queryString : '')) + resultsSelector;
        } else {
            location.href = href + (queryString ? '?' + queryString : '') + resultsSelector;
        }

        // TODO: Make REST requests work!
        /*window.fetch(url)
            .then(response => {
                queryEnd();
                if (200 === response.status) {
                    return response.json();
                }
            })
            .then(json => {
                if (json.foundPosts) {
                    resultsContainer.innerHTML += json.html;
                }
            })
            .catch(err => {
                console.log(err);
                queryEnd();
            });*/
    }

    addAction('archive.queryStart', 'archive.queryStart.archiveActions', () => {
        archiveWrap.classList.add('archive--querying');
    });

    addAction('archive.queryEnd', 'archive.queryEnd.archiveActions', () => {
        archiveWrap.classList.remove('archive--querying');
    });

    // Setup filters
    const filters = archiveWrap.querySelectorAll('select.archive-filter');
    if (filters && filters.length) {
        filters.forEach((filter) => {
            const filterQueryVar = filter.getAttribute('data-query-var');

            // Handle disabling/enabling filter based on query status.
            addAction('archive.queryStart', `archive.queryStart.${filterQueryVar}`, () => {
                filter.disabled = true;
            });

            addAction('archive.queryEnd', `archive.queryEnd.${filterQueryVar}`, () => {
                filter.disabled = false;
            });

            // Allow clearing all filters at once.
            addAction('archive.clearFilters', `archive.clearFilters.${filterQueryVar}`, () => {
                filter.value = '';
            });

            // Trigger action when filter is updated.
            filter.addEventListener('change', (e) => {
                const value = e.target.value;
                doAction('archive.filterChange', {
                    filterQueryVar,
                    value,
                });
            });
        });
    }

    // Setup search field
    const searchInput = archiveWrap.querySelector('input[name="search"]');
    const searchButton = archiveWrap.querySelector('.coreview-search-form__submit');
    const searchWrap = archiveWrap.querySelector('.coreview-search-form');
    if (searchInput && searchButton) {
        searchInput.addEventListener('focus', () => {
            searchWrap.classList.add('focus');
        });
        searchButton.addEventListener('focus', () => {
            searchWrap.classList.add('focus');
            searchInput.focus();
        });
        searchInput.addEventListener('blur', () => {
            setTimeout(() => {
                searchWrap.classList.remove('focus');
            }, 250);
        });

        // Handle disabling/enabling filter based on query status.
        addAction('archive.queryStart', 'archive.queryStart.search', () => {
            searchInput.disabled = true;
            searchButton.disabled = true;
        });

        addAction('archive.queryEnd', 'archive.queryEnd.search', () => {
            searchInput.disabled = false;
            searchButton.disabled = false;
        });

        // Allow clearing all filters at once.
        addAction('archive.clearFilters', 'archive.clearFilters.search', () => {
            searchInput.value = '';
        });

        searchInput.addEventListener('keyup', function (e) {
            if (e.keyCode === 13) {
                doAction('archive.searchChange', searchInput.value);
            }
        });

        searchButton.addEventListener('click', function () {
            doAction('archive.searchChange', searchInput.value);
        });
    }

    // Receive and process filter change data.
    addAction('archive.filterChange', 'archive.filterChange.handleChange', (data) => {
        if (data.value) {
            // Set value in params.
            params[data.filterQueryVar] = data.value;
        } else if (params[data.filterQueryVar]) {
            // Param exists, but value is now empty. Remove it.
            delete params[data.filterQueryVar];
        }
        // Clear pagination if set, sends us back to the first page.
        if (params.pg) {
            delete params.pg;
        }
        doQuery();
    });

    addAction('archive.searchChange', 'archive.searchChange.handleSearch', (search) => {
        if (search) {
            params.search = search;
        } else if (params.search) {
            delete params.search;
        }
        // Clear pagination if set, sends us back to the first page.
        if (params.pg) {
            delete params.pg;
        }
        doQuery();
    });


    // TODO: Add pagination link functionality for REST API interaction.

});
