import domReady from '@wordpress/dom-ready';
import $ from 'jquery';

domReady(() => {
    $("option").each(function() {
        var $this = $(this);
        $this.text($this.text().charAt(0).toUpperCase() + $this.text().slice(1));
    });
});