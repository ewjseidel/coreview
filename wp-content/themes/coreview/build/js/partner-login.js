import domReady from '@wordpress/dom-ready';

domReady(() => {
    const loginForm = jQuery('#loginform');

    if(loginForm.length <= 0) return;

    loginForm.parent().children().not('.wp-block-narwhal-background').wrapAll('<div class="login-form"></div>');

    if (loginForm) {
        jQuery('.login-username, .login-password', loginForm).each(function(){

            let $wrapper = jQuery(this);
            let $input = jQuery('input', $wrapper);
            
            function labelUp(wrapper){
                wrapper.addClass('label-up').removeClass('label-down');
            }

            function labelDown(wrapper){
                wrapper.addClass('label-down').removeClass('label-up');
            }

            function applyClass(input,wrapper){
                if(input.val() === ''){
                    labelDown(wrapper);
                } else {
                    labelUp(wrapper);
                }
            }

            applyClass($input,$wrapper);

            $input.on('focus',function(){
                labelUp($wrapper);
            });

            $input.on('blur',function(){
                applyClass($input,$wrapper);
            });

        });
    }

});