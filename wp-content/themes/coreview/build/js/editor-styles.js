import {addFilter} from '@wordpress/hooks';
import {__} from '@wordpress/i18n';
import _ from 'lodash';

addFilter('blocks.registerBlockType', 'coreview/block/paragraph/styles', (settings, name) => {
    if (name === 'core/paragraph') {
        settings.styles = [
            {
                name: 'default',
                label: __('Standard', 'coreview'),
                isDefault: true,
            },
            {
                name: 'intro-paragraph',
                label: __('Intro', 'coreview'),
            },
        ];
    }

    return settings;
});

addFilter('narwhal.blocks.background.overlayOptions', 'coreview.blocks.background.overlayOptions', (options) => {
    options.push({
            label: "light",
            value: "light"
        },
        {
            label: "dark",
            value: "dark"
        }
    );

    return options;
});

addFilter('narwhal.blocks.background.defaultOverlayStyle', 'coreview.blocks.background.defaultOverlayStyle', ()=> 'dark');
