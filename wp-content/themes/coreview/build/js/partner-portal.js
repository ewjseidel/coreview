import domReady from '@wordpress/dom-ready';

domReady(() => {
    const sideNav = jQuery('.portal-nav-primary');
    const menu = jQuery('.partner-portal-nav-primary');
    const win = jQuery(window);

    if(sideNav.length <= 0 && win.width() > 768) return;

    function stickySideNav(){

        let calcTopPosition = (win.scrollTop() + 15) - sideNav.offset().top;
        let calcAvailabeSpace = sideNav.outerHeight() - (calcTopPosition + menu.outerHeight());

        if (win.width() <= 768) { 
            sideNav.removeClass('portal-nav-primary-sticky'); 
        } else {
            sideNav.addClass('portal-nav-primary-sticky');
            if(calcTopPosition > 0 && calcAvailabeSpace > 0){
                menu.css({'top': calcTopPosition});
            } else if ( calcTopPosition <= 0 ) {
                menu.css({'top': 0});
            }
        }

    }

    stickySideNav();

    win.on('scroll', function(){ stickySideNav(); })
    win.on('resize', function(){ stickySideNav(); })

});