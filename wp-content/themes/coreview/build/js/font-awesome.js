import {config, dom, library} from '@fortawesome/fontawesome-svg-core';
import { BrowserDetect} from "./browser";
/*import {
    faChevronDown,
    faChevronUp,
    faSearch,
    faTimes,
} from '@fortawesome/free-solid-svg-icons';*/
import {
    faFacebook,
    faFacebookF,
    faInstagram,
    faLinkedin,
    faLinkedinIn,
    faTwitter,
    faTwitterSquare,
    faVimeoV,
    faYoutube,
} from '@fortawesome/free-brands-svg-icons';

if (BrowserDetect.browser === 'Explorer') {

    window.setTimeout(
        function(){
            config.searchPseudoElements = true;
            library.add(
                faFacebook,
                faFacebookF,
                faInstagram,
                faLinkedin,
                faLinkedinIn,
                faTwitter,
                faTwitterSquare,
                faVimeoV,
                faYoutube,
            );

            dom.watch();
        }, 5000
    );


}

