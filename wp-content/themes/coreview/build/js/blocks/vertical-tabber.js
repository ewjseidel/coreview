import domReady from '@wordpress/dom-ready';

domReady(() => {

    const vtLinks = document.querySelectorAll('.vertical-tabber__nav-item');

    if ( vtLinks && vtLinks.length ) {
        vtLinks.forEach(link => {
            link.addEventListener("click", function (e) {
                let myTabber = this.closest('.vertical-tabber'),
                    myTabLinks = myTabber.querySelectorAll('.vertical-tabber__nav-item'),
                    myTabs = myTabber.querySelectorAll('.vertical-tabber-tab');

                if (!this.classList.contains('active')) {
                    // Remove active from all links under this tabber
                    myTabLinks.forEach(sib => { sib.classList.remove('active'); });
                    this.classList.add('active');
                    myTabs.forEach(sib => { sib.classList.remove('active'); });
                    // Can't query a nodeList directly (without iterating on it by hand), easier to just query on the whole tabber.
                    myTabber.querySelector('.tab-' + this.dataset.tab).classList.add('active');
                }
                e.preventDefault();
            });
        })
    }

});

