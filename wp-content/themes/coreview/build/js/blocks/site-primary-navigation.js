import domReady from '@wordpress/dom-ready';
import $ from 'jquery';
require('jquery-hoverintent');

domReady(() => {
    var $navItem =  $(".cv-main-menu > .menu-item-has-children");
    if ($navItem.length < 1) return;
    $navItem.each( function() {
        var $this = $(this),
            $dropdown = $this.find(">.sub-menu");

        if($dropdown.find("ul").length > 0){
            $dropdown.addClass("menu-multi-column").hide();
        }

        $this.hoverIntent({
            sensitivity: 2,
            interval: 100,
            over: function(){
                $dropdown.slideDown("fast");
                $this.addClass("menu-item-hover");
            },
            timeout: 100,
            out: function(){
                $dropdown.slideUp("fast");
                $this.removeClass("menu-item-hover");
            }
        });
        
    });


    const smoothHashScroll = () => {
        // console.log('scrolling...?')
        // console.log(window.location);
        if(window.location.hash) {
            console.log('has a hash....')
            var hash = window.location.hash.substring(1);
            var headerHeight = $(".site-header").height() + 50;
            // console.log(hash);

            if (hash.length) {
                $('html, body').animate({
                    scrollTop: $('#'+hash).offset().top - headerHeight
                }, 2000);
            }

            return false;

        } else {
            // No hash found
        }
    }

    smoothHashScroll();

    $(window).on('hashchange', () => smoothHashScroll());


    const nav = document.querySelector('.site-header');
    const blocks = document.querySelector('.wp-block-narwhal');

    if (blocks != null && blocks.classList.contains('simple-header')) {
        nav.classList.remove("default-header");
        nav.classList.add("simple-header");
    }
});