import domReady from '@wordpress/dom-ready';

domReady(() => {

    const target = document.querySelector(".gdpr-target");
    const footerGDPR = document.querySelector(".footer-gdpr");
    const gdprConsent = 1;


    if ( footerGDPR && ! localStorage.length > 0 ) {
        footerGDPR.classList.add("slide-up");
    }


    if (target) {
        target.addEventListener("click", function(e) {
            e.preventDefault();
            footerGDPR.classList.toggle("slide-down");
            footerGDPR.classList.remove("slide-up");
            localStorage.setItem(gdprConsent, 'User Cookie Consent');
        });
    }

});

