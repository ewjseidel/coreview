import domReady from '@wordpress/dom-ready';
import $ from 'jquery';


domReady(() => {

    var portalNav = $('.portal-site-header');
    var nav = $('.site-header');


    $(window).scroll(function()
    {
        if ($(this).scrollTop() > 0)
        {
            $(nav).addClass("header-sticky");
            $(portalNav).addClass("header-sticky");

        }
        else
        {
            $(nav).removeClass("header-sticky");
            $(portalNav).removeClass("header-sticky");
        }
    });

    // if ( $('wp-block-narwhal:has(simple-header)'))  {
    //     $(nav).removeClass("default-header");
    //     $(nav).addClass("simple-header");
    //
    // }

    // const portalNav = document.querySelector('.site-header,.portal-site-header');
    // const blocks = document.querySelector('.wp-block-narwhal');

    // window.addEventListener("scroll", function(e) {
    //     if (window.scrollY > 0 ) {
    //         portalNav.classList.add("header-sticky");
    //     } else {
    //         portalNav.classList.remove("header-sticky");
    //     }
    // });

    // if (blocks != null && blocks.classList.contains('simple-header')) {
    //     nav.classList.remove("default-header");
    //     nav.classList.add("simple-header");
    // }

});