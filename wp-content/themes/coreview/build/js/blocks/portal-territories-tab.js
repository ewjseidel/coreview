import domReady from '@wordpress/dom-ready';

domReady(() => {

    const territoryLinks = document.querySelectorAll('.portal-territories__nav-item');


    if (territoryLinks && territoryLinks.length) {
        territoryLinks.forEach(link => {
            link.addEventListener("click", function (e) {

                let myTabber = this.closest('.portal-territories'),
                    myTabLinks = myTabber.querySelectorAll('.portal-territories__nav-item'),
                    myTabs = myTabber.querySelectorAll('.portal-territories-tab');

                if (!this.classList.contains('active')) {
                    // Remove active from all links under this tabber
                    myTabLinks.forEach(sib => { sib.classList.remove('active'); });
                    this.classList.add('active');
                    myTabs.forEach(sib => { sib.classList.remove('active'); });
                    // Can't query a nodeList directly (without iterating on it by hand), easier to just query on the whole tabber.
                    myTabber.querySelector('.tab-' + this.dataset.tab).classList.add('active');
                }
                e.preventDefault();

            });
        })
    }

});

