import domReady from '@wordpress/dom-ready';

domReady(() => {

    const disruptor = document.querySelector('.disruptor');
    const disruptorClose = document.querySelector('.disruptor__close');
    // const disruptorSlide = document.querySelector('.disruptor__hide');

    if (disruptor && disruptorClose) {
        disruptorClose.addEventListener("click", function (e) {
            const $disruptor = jQuery(disruptor);
            $disruptor.css({ "z-index": "-1" });
            $disruptor.animate({"margin-top": "-" + $disruptor.outerHeight(true) + "px"}, function(){
                $disruptor.hide();
            });
        });
    }

});

