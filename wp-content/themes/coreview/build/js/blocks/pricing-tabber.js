import domReady from '@wordpress/dom-ready';

domReady(() => {

    const ptLinks = document.querySelectorAll('.pricing-tabber__nav-item');

    if ( ptLinks && ptLinks.length ) {
        ptLinks.forEach(link => {
            link.addEventListener("click", function (e) {

                let myTabber = this.closest('.pricing-tabber'),
                    myTabLinks = myTabber.querySelectorAll('.pricing-tabber__nav-item'),
                    myTabs = myTabber.querySelectorAll('.pricing-tabber-tab');

                if (!this.classList.contains('active')) {
                    // Remove active from all links under this tabber
                    myTabLinks.forEach(sib => { sib.classList.remove('active'); });
                    this.classList.add('active');
                    myTabs.forEach(sib => { sib.classList.remove('active'); });
                    // Can't query a nodeList directly (without iterating on it by hand), easier to just query on the whole tabber.
                    myTabber.querySelector('.tab-' + this.dataset.tab).classList.add('active');
                }
                e.preventDefault();

            });
        })

    }

});

