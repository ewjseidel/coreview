import domReady from '@wordpress/dom-ready';

const testimonialSlider = document.querySelector('.testimonial-slider__collection');
const testimonialSlide = document.querySelector('.testimonial-slider__slide');

domReady(() => {
    var swiperPagination = new Swiper('.gallery-thumbs', {
        slidesPerView: 3,
        freeMode: false,
        watchOverflow: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        // giving this a higher threshold helps make sure that 'click's aren't inperpreted as 'beginning of swipe's.
        threshold: 3,
        breakpoints: {
            480: {
                slidesPerView: 1,
                spaceBetween: 20
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 30
            }

        }
    });

    var mySwiper = new Swiper('.testimonial-slider__collection', {
        spaceBetween: 10,
        slidesPerView: 1,
        loop:true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        thumbs: {
            swiper: swiperPagination,
        },

    });



});


// Swiper slider http://idangero.us/swiper/api/