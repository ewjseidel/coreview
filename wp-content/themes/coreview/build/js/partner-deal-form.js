import domReady from '@wordpress/dom-ready';

const {usaId} = window.coreView;

domReady(() => {

    const form = document.querySelector('#partner-portal-deal-form');

    if (form) {
        const countrySelector = form.querySelector('select[name="_customer_details[cv-partner-region]"]');
        const stateSelector = form.querySelector('.flex-field-select[data-name="us_state"]');

        const updateStateVisibility = () => {
            if (`${usaId}` === countrySelector.value) {
                stateSelector.removeAttribute('hidden');
            } else {
                stateSelector.setAttribute('hidden', 'hidden');
            }
        };

        updateStateVisibility();

        countrySelector.addEventListener('change', updateStateVisibility);

        jQuery('.flex-field-input, .flex-field-select, .flex-field-textarea', form).each(function(){

            let $wrapper = jQuery(this);
            let $input = jQuery('input,textarea,select', $wrapper);

            function labelUp(wrapper){
                wrapper.addClass('label-up').removeClass('label-down');
            }

            function labelDown(wrapper){
                wrapper.addClass('label-down').removeClass('label-up');
            }

            function applyClass(input,wrapper){
                if(input.val() === ''){
                    labelDown(wrapper);
                } else {
                    labelUp(wrapper);
                }
            }

            applyClass($input,$wrapper);

            $input.on('focus',function(){
                labelUp($wrapper);
            });

            $input.on('blur',function(){
                applyClass($input,$wrapper);
            });

        });
    }

});

