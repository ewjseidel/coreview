import domReady from '@wordpress/dom-ready';
import $ from 'jquery';

domReady(() => {
    const newTabLinks = $('.--open-new-tab > a');
    newTabLinks.attr('target', '_blank');
});