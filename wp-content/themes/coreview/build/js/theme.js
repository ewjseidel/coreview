if (window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = Array.prototype.forEach;
}

if (!Element.prototype.matches) {
    Element.prototype.matches = Element.prototype.msMatchesSelector ||
        Element.prototype.webkitMatchesSelector;
}

if (!Element.prototype.closest) {
    Element.prototype.closest = function(s) {
        var el = this;

        do {
            if (el.matches(s)) return el;
            el = el.parentElement || el.parentNode;
        } while (el !== null && el.nodeType === 1);
        return null;
    };
}

import './ie-fallbacks';
import './blocks/*';
import '../thirdparty/swiper/swiper';
import './mobile-nav';
import './archives';
import './homepage-animation';
import './open-new-tab';
import './partner-deal-form';
import './partner-request-form';
import './partner-portal';
import './partner-login';
import './load-more';
import './option-capitalization';
