import {addFilter} from '@wordpress/hooks';

// Set default background color for background color block.
addFilter('narwhal.blocks.background.defaultBackgroundColor', 'coreview.blocks.background.defaultBackgroundColor', () => 'grey-light-background');
