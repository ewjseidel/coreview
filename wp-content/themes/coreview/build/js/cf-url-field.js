import domReady from '@wordpress/dom-ready';
import $ from 'jquery';

domReady(() => {
    let editor = $('#editor');
    console.log("cf url field file");

    editor.on('focus', '.cf-text__input[type=url]', function(e) {
        var element = $(e.target);
        var id = element.attr('id');
        var parent = element.closest('.cf-field__body');

        var html = [
            '<div class="components-popover is-top is-center is-without-arrow" style="left: 0; position: absolute;" data-id="'+id+'">',
            '<div class="components-popover__content" tabindex="-1" style="transform: none">',
            '<div class="editor-url-input__suggestions" role="listbox">',
            '</div>',
            '</div>',
            '</div>',
        ];

        parent.append(html.join(''));
        parent.css('position', 'relative');
    });

    editor.on('keyup', '.cf-text__input[type=url]', function(e) {
        var element = $(e.target);
        var value = element.val();
        var id = element.attr('id');

        $.getJSON('/wp-json/wp/v2/search?search='+value+'&per_page=20&type=post&_locale=user', function(data) {
            var popover = $('.components-popover[data-id='+id+']');
            var container = $('.editor-url-input__suggestions', popover);
            container.empty();

            var results = [];

            for(var i in data) {
                if(data.hasOwnProperty(i)) {
                    results.push([
                        '<button role="option" tabindex="-1" class="editor-url-input__suggestion editor-url-input__suggestion--cf" aria-selected="false" data-url="'+data[i].url+'">',
                        data[i].title,
                        '</button>',
                    ].join(''));
                }
            }

            container.html(results.join(''));
        });
    });

    editor.on('click', function(e) {
        var element = $(e.target);

        if(!element.closest('.cf-field__body').children('.components-popover').length) {
            var popover = $('.components-popover[data-id]');
            popover.remove();
        }
    });

    editor.on('click', '.editor-url-input__suggestion--cf', function(e) {
        e.preventDefault();
        e.stopPropagation();

        function triggerChange(selector,evt) {
            let ev = new Event(evt, { bubbles: true });
            let el = $(selector)[0];

            el.dispatchEvent(ev);
        }

        var element = $(e.target);
        var parent = element.closest('.components-popover');
        var id = parent.data('id');
        var url = element.data('url');
        var nativeInputValueSetter = Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, 'value').set;
        const inputField = document.querySelector(`input#${id}`);
        nativeInputValueSetter.call(inputField, url);
        inputField.blur();
        triggerChange(`#${id}`,'input');
        parent.remove();
    });
});
