<?php
/**
 * Template Name: Partner Portal: Dashboard
 * Template Post Type: page
 */
$user = wp_get_current_user();
$userFirst = $user->user_firstname;
$userLast = $user->user_lastname;

$userName = $userFirst." ".$userLast;

the_post();

get_header( 'partner-portal' );

?>
    <div class="alignfull partner-portal-dashboard">
        <?php get_template_part( 'partials/partner-portal-nav-primary' ); ?>
        <div class="container">
            <div class="grid__row">
                <div class="grid__col col-12/12@md">
                    <div class="portal-blocks">
                        <div class="grid__row">
                            <div class="grid__col">
                                <h1>Welcome, <?php echo $userName;?>!</h1>
                            </div>
                        </div>
                        <div class="grid__row" id="portal-news">
                            <div class="grid__col col-4/12@sm">
                                <?php get_template_part( 'partials/partner-portal-dashboard-news' ); ?>
                            </div>
                            <div class="grid__col col-8/12@sm">
                                <?php get_template_part( 'partials/partner-portal-dashboard-overview' ); ?>
                            </div>
                        </div>
                        <div class="grid__row">
                            <div class="grid__col" id="portal-deals-form">
                                <?php get_template_part( 'partials/partner-portal-deal-form' ); ?>
                            </div>
                            <div class="grid__col" id="portal-my-deals">
                                <?php get_template_part( 'partials/partner-portal-deals' ); ?>
                            </div>
                        </div>
                        <div class="grid__row">
                            <div class="grid__col col-8/12@sm">
                                <?php get_template_part('partials/partner-portal-dashboard-resources')?>
                            </div>
                            <div class="grid__col col-4/12@sm">
                                <?php get_simple_job_board_template_part('partials/partner-portal-dashboard-contacts')?>
                            </div>
                        </div>
                        <div class="grid__row" id="portal-territories">
                            <div class="grid__col">
                                <?php get_template_part('partials/partner-portal-dashboard-territories')?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer( 'partner-portal' );
