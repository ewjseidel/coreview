<form class="coreview-search-form" action="/" method="get">
    <label class="coreview-search-form__label">
        <span class="coreview-search-form__label-text screen-reader-text"><?php esc_html_e( 'Search CoreView', 'coreview' ); ?></span>
        <input class="coreview-search-form__input"
               type="text" name="s" autocomplete="off"
               placeholder="<?php esc_attr_e( 'Search', 'coreview' ); ?>"
               value="<?php the_search_query(); ?>" />
    </label>
    <button class="coreview-search-form__submit" type="submit" value="<?php esc_attr_e( 'Search', 'coreview' ); ?>">
        <i class="fas fa-search"></i>
    </button>
</form>
