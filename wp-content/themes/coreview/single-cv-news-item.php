<?php

the_post();
get_header();

$postData = \Coreview\Models\NewsItem::create(get_post());

$contactGroup = $postData->getContactInfo();

$postTags = $postData->getTags();

$postType = 'cv-news-item';

?>

    <div class="grid__row">
        <div class="grid__col col-10/12@lg offset-1/12@lg">

            <article class='single-cv-news-item post-type-template'>
                <header class='single-cv-news-item__post-header'>
                    <div class="single-cv-news-item__tax-title alignfull">
                        <div class="container">
                            <p>
	                            <?php if ($postData->getNextPost()):?>
                                    <a class="single-cv-news-item__tax-title-next" href="<?php echo get_the_permalink($postData->getNextPost())?>">
                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" height="14px" width="8px" viewBox="0 0 8 14" style="enable-background:new 0 0 8 14;" xml:space="preserve">
                                            <polygon points="8,0.3 8,13.7 0,7 "></polygon>
                                        </svg>
                                    </a>
	                            <?php endif;?>
                                <?php echo $postData->getMainNewsTypeName();?>
	                            <?php if ($postData->getPreviousPost()):?>
                                    <a class="single-cv-news-item__tax-title-prev" href="<?php echo get_the_permalink($postData->getPreviousPost())?>">
                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" height="14px" width="8px" viewBox="0 0 8 14" style="enable-background:new 0 0 8 14;" xml:space="preserve">
                                            <polygon points="0,13.7 0,0.3 8,7 "></polygon>
                                        </svg>
                                    </a>
	                            <?php endif;?>
                            </p>
                            <?php //TODO: Add primary taxonomy tag ?>
                        </div>
                    </div>

                    <div class='single-cv-news-item__breadcrumbs'>
                        <ul class=''>
	                        <?php //TODO: Add breadcrumb package ?>
                            <li><a href="<?php echo get_site_url();?>">Home</a></li>
                            <li><a href="<?php echo get_site_url("", "/news-events");?>">News & Events</a></li>
                            <li><a href="#"><?php echo the_title();?></a></li>
                        </ul>
                    </div>
                </header>

                <div class="single-cv-news-item__main">
                    <div class="single-cv-news-item__inner">
                        <div class="single-cv-news-item__post-date">
                            <p><?php echo get_the_date('M j Y');?></p>

                        </div>
                        <div class="single-cv-news-item__post-title">
                            <h1><?php echo $postData->PostTitle();?></h1>
                        </div>

                        <div class="single-cv-news-item__content">
	                        <?php the_content(); ?>


                        </div>

                        <div class="single-cv-news-item__post-meta">
                            <?php if (!empty( $contactGroup ) ):?>
                            <div class="single-cv-news-item__contacts">
                                <h2>Contacts</h2>
                                    <?php foreach( $contactGroup as $group):?>
                                        <div class="single-cv-news-item__contact-group">
                                            <div class="single-cv-news-item__contact-company">
                                                <h4><?php echo $group['contact_group']?></h4>
                                            </div>
                                            <div class="single-cv-news-item__contact-entry">
                                                <?php echo $group['contact_info'];?>
                                            </div>

                                        </div>
                                    <?php endforeach;?>
                            </div>
                            <?php endif;?>

	                        <?php
	                        if ( ! empty( $postTags ) ): ?>
                                <div class="single-cv-news-item__tags">
                                    <h2>Tags</h2>
                                <ul class="">
                                    <?php foreach ( $postTags as $tag ):
                                        ?>
                                        <li><a href="<?php echo site_url("/news-events/?general-tags=$tag->term_id#archive-results"); ?>">
                                                <?php echo $tag->name; ?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                            <?php endif;?>
                        </div>


                    </div>



                </div>
            </article>

        </div>

    </div>

    <div class="grid__row">
        <div class="grid__col">
	        <?php \Coreview\loadTemplateWithContext("partials/related-news", [
		        'postType' => $postType,
	        ]) ?>
        </div>

    </div>


<?php get_footer();

