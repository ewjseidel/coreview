<?php

get_header();

the_post();

?>

        <div class="coreview-blocks">
			<?php the_content(); ?>
        </div>

<?php

get_footer();
