<?php

// Please do not add anything to this file!

if ( file_exists( __DIR__ . '/version.php' ) ) {
	require __DIR__ . '/version.php';
}

if ( ! defined( 'COREVIEW_THEME_VERSION' ) ) {
	define( 'COREVIEW_THEME_VERSION', date( 'U' ) );
}

// Easy place to define theme typography.
define( 'COREVIEW_THEME_FONTS', 'https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,700,700i' );

// Automatically load all PHP files in the '/includes' directory
$iterator = new RecursiveDirectoryIterator( __DIR__ . '/includes' );
foreach ( new RecursiveIteratorIterator( $iterator ) as $file ) {
	if ( $file->getExtension() === 'php' ) {
		require $file;
	}
}

// Please do not add anything to this file!
