<?php
/**
 * Template Name: Partner Portal: Resource Archives
 * Template Post Type: page
 */

$results = \Coreview\Archives::getCollection( \Coreview\PostTypes\PartnerResources::POST_TYPE );

get_header( 'partner-portal' ); ?>

<div class="alignfull partner-resources-archive">

	<?php get_template_part( 'partials/partner-portal-nav-primary' ); ?>

	<div class="container">
        <h1>Partner Resources</h1>

		<?php
		\Coreview\loadTemplateWithContext( 'partials/archive/archive.php', [
			'results' => $results,
		] );?>

	</div>

</div>
<?php
get_footer( 'partner-portal' );
