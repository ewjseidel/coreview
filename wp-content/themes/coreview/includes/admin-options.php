<?php

namespace Coreview;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action( 'carbon_fields_register_fields', __NAMESPACE__ . '\addThemeOptionsPage' );

function addThemeOptionsPage() {
	Container::make( 'theme_options', esc_html__( 'Fallback Images', 'coreview' ) )
	         ->set_page_parent( 'options-general.php' )
	         ->add_fields( [
		         Field::make( 'media_gallery', 'coreview_fallback_images', esc_html__( 'Fallback Featured Images', 'coreview' ) )
		              ->set_type( [ 'image' ] )
		              ->set_duplicates_allowed( false ),
	         ] );
}