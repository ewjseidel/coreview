<?php

// Allow custom image sizes to appear in admin image lists with user readable name.
add_filter( 'image_size_names_choose', function ( $sizes ) {
	$sizes['masthead'] = esc_html__( 'Masthead', 'coreview' );
	$sizes['logo'] = esc_html__( 'Logo', 'coreview' );
	$sizes['video-cover'] = esc_html__( 'Video Overlay', 'coreview' );
	$sizes['person-card'] = esc_html__( 'Person Avatar', 'coreview' );
	$sizes['icon'] = esc_html__( 'Icon', 'coreview' );
	$sizes['pdg-square'] = esc_html__( 'Small Square', 'coreview' );
	$sizes['pdg-small'] = esc_html__( 'Medium-Large', 'coreview' );

	return $sizes;
} );
