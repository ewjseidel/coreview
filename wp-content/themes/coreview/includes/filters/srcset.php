<?php

add_filter( 'max_srcset_image_width', function ( $maxWidth, $size_array ) {
	if ( $size_array[0] >= 1760 ) {
		// Allow giant images to have a true 2x size image in srcset attribute.
		$maxWidth = 3520;
	} elseif ( $size_array[0] >= 1200 ) {
		// Allow giant images to have a true 2x size image in srcset attribute.
		$maxWidth = 2400;
	} elseif ( $size_array[0] >= 1120 ) {
		// Allow giant images to have a true 2x size image in srcset attribute.
		$maxWidth = 2240;
	} elseif ( $size_array[0] >= 1024 ) {
		// Allow large images to have a true 2x size image in srcset attribute.
		$maxWidth = 2048;
	} elseif ( $size_array[0] <= 150 ) {
		// Restrict images that are being output at small sizes to not have all available sizes in srcset.
		$maxWidth = 600;
	} elseif ( $size_array[0] <= 300 ) {
		// Restrict images that are being output at small sizes to not have all available sizes in srcset.
		$maxWidth = 900;
	}

	return $maxWidth;
}, 10, 2 );