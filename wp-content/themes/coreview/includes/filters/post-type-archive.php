<?php
add_filter('narwhal/blocks/postTypeArchive/content', function($content, $postType) {
	$results = \Coreview\Archives::getCollection( $postType );
	ob_start();

	\Coreview\loadTemplateWithContext("partials/archive/archive", [
		//name of the variable in the template
		'postType' => $postType,
		'results' => $results
	]);
	$content = ob_get_clean();
	$content = str_replace( [
		"\n",
		"\t",
		"\r",
		"    ",
	], '', $content );
	return $content;
}, 10, 2);