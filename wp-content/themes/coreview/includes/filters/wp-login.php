<?php

add_filter( 'login_headerurl', function () {
	return home_url( '/' );
} );

add_filter( 'login_headertitle', function () {
	return get_bloginfo( 'name' );
} );

