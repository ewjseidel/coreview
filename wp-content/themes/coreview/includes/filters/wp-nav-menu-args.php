<?php

add_filter( 'wp_nav_menu_args', function ( array $args ) {
	// If no depth is set, set it to 1.
	if ( 0 === $args['depth'] ) {
		$args['depth'] = 1;
	}
	switch ( $args['theme_location'] ) {
		// Add cases here for any overrides needed per menu location.
		default:
			$args = array_merge( $args, [
				'container'   => false,
				'fallback_cb' => false,
				'items_wrap'  => '<ul class="%2$s">%3$s</ul>',
			] );
	}

	return $args;
} );