<?php

namespace Coreview;

/**
 * Load a template with contextual variables.
 *
 * @param string|array $template
 * @param array $context
 * @param string $extension
 */
function loadTemplateWithContext( $template, array $context = [], $extension = '.php' ) {
	if ( is_array( $template ) ) {
		foreach ( $template as $key => $value ) {
			if ( ! strpos( $value, $extension, - strlen( $extension ) ) ) {
				$template[ $key ] .= $extension;
			}
		}
	} else {
		if ( ! strpos( $template, $extension, - strlen( $extension ) ) ) {
			$template .= $extension;
		}
	}
	extract( $context, EXTR_SKIP );
	$template_file = locate_template( $template );
	if ( $template_file ) {
		include $template_file;
	}
}


/**
 * Carbon fields helper to get proper i18n suffix.
 *
 * @return string
 */
function crb_get_i18n_suffix() {
    $suffix = '';
    if ( ! defined( 'ICL_LANGUAGE_CODE' ) ) {
        return $suffix;
    }
    $suffix = '_' . ICL_LANGUAGE_CODE;

    return $suffix;
}


/**
 * Returns a localized theme option set by Carbon Fields.
 *
 * @param string $option_name
 *
 * @return mixed
 */
function getCFi18nThemeOption( $option_name ) {
    if ( ! function_exists( 'carbon_get_theme_option' ) ) {
        return '';
    }

    $suffix = crb_get_i18n_suffix();

    return carbon_get_theme_option( $option_name . $suffix );
}


/** Add excerpt support to pages */
add_post_type_support( 'page', 'excerpt' );


/**
 * @param true $done
 *
 * @return bool
 */
function didTemplateOutput( $done = null ) {
    static $didOutput = false;
    if ( true === $done ) {
        $didOutput = true;
    }

    return $didOutput;
}
