<?php

use Coreview\Breadcrumbs;
use Coreview\PostTypes\NewsItems;
use Coreview\PostTypes\Events;
use Coreview\PostTypes\Posts;
use Coreview\PostTypes\Resources;




/**
 * Outputs the Coreview Breadcrumbs.
 */
function coreviewBreadcrumbs() {
	$crumbs = new Breadcrumbs('<li class="breadcrumb">','</li>');
	$crumbs->theCrumbs();
}

function headerClass() {
	$default = "default-header";
	$simpleHeader = "simple-header";
	$singlePosts = [
		NewsItems::POST_TYPE,
		Events::POST_TYPE,
		Resources::POST_TYPE,
		"post"
	];

	if ( is_page() ) {
		return $default;
	} elseif ( is_singular($singlePosts)) {
		return $simpleHeader;
	}
}
