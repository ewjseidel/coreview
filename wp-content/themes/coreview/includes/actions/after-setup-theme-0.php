<?php

/**
 * Defines the content width for the theme.
 */
add_action( 'after_setup_theme', function () {
	$GLOBALS['content_width'] = apply_filters( 'coreview_content_width', 1080 );
}, 0 );
