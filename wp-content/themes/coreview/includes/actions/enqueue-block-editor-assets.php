<?php

add_action( 'enqueue_block_editor_assets', function () {

	$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

	wp_enqueue_style( 'coreview-fonts', COREVIEW_THEME_FONTS, [], COREVIEW_THEME_VERSION );

	wp_enqueue_style( 'coreview-theme-editor', get_template_directory_uri() . "/assets/css/editor{$suffix}.css", [], COREVIEW_THEME_VERSION );

	wp_enqueue_script( 'coreview-theme-editor', get_template_directory_uri() . "/assets/js/editor{$suffix}.js", [ 'wp-blocks', 'lodash' ], COREVIEW_THEME_VERSION );

	wp_enqueue_script( 'coreview-fontawesome', get_template_directory_uri() . "/assets/js/fontawesome{$suffix}.js", [], COREVIEW_THEME_VERSION, true );

} );
