<?php

add_action( 'after_setup_theme', function () {

	load_theme_textdomain( 'coreview', get_template_directory() . '/languages' );

	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'html5', [ 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ] );
	add_theme_support( 'post-formats', [ 'gallery', 'video' ] );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 1200, 9999 );

	add_theme_support( 'editor-styles' );
	add_theme_support( 'responsive-embeds' );
	add_theme_support( 'wp-block-styles' );
	// Allow wide & full block alignment.
	add_theme_support( 'align-wide' );
	// No custom colors.
	add_theme_support( 'disable-custom-colors' );
	// Set list of predefined colors with site colors.
	add_theme_support( 'editor-color-palette',
		[
			[
				'name'  => esc_html__( 'White', 'coreview' ),
				'slug'  => 'white',
				'color' => '#ffffff',
			],
			[
				'name'  => esc_html__( 'Light Grey Background', 'coreview' ),
				'slug'  => 'grey-light-background',
				'color' => '#f1f4f7',
			],
			[
				'name'  => esc_html__( 'Coreview Blue', 'coreview' ),
				'slug'  => 'blue',
				'color' => '#1d63af',
			],
			[
				'name'  => esc_html__( 'Navy', 'coreview' ),
				'slug'  => 'navy',
				'color' => '#0c2949',
			],
			[
				'name'  => esc_html__( 'Orange', 'coreview' ),
				'slug'  => 'orange',
				'color' => '#ef742f',
			],
			[
				'name'  => esc_html__( 'Dark Grey (Text color)', 'coreview' ),
				'slug'  => 'grey-dark',
				'color' => '#81878d',
			],
			[
				'name'  => esc_html__( 'Black', 'coreview' ),
				'slug'  => 'black',
				'color' => '#000000',
			],
		]
	);

	register_nav_menus( [
		'nav-primary'            => esc_html__( 'Header Navigation', 'coreview' ),
		'nav-footer-detail'      => esc_html__( 'Detailed Footer Navigation', 'coreview' ),
		'nav-utility'            => esc_html__( 'Utility Navigation', 'coreview' ),
		'nav-gdpr'               => esc_html__( "GDPR Navigation", 'coreview'),
		'partner-portal-nav-primary'    => esc_html__("Partner Portal Primary Navigation", 'coreview'),
		'partner-portal-nav-utility'    => esc_html__( "Parter Portal Utility Navigation", 'coreview'),
//		'nav-products'           => esc_html__( 'Products Navigation', 'coreview' ),
//		'nav-mega-menu-side'     => esc_html__( 'Mega Menu Right Column Navigation', 'coreview' ),
//		'nav-footer-quick'       => esc_html__( 'Quick Footer Navigation', 'coreview' ),
//		'nav-social'             => esc_html__( 'Social Navigation', 'coreview' ),
//		'nav-legal'              => esc_html__( 'Legal Navigation', 'coreview' ),
	] );

	/** Register image sizes **/

	// Retina versions of default sizes
	add_image_size( 'large-2x', 2048, 4096 );
	add_image_size( 'medium-2x', 600, 1200 );

	// Masthead/testimonial/left-right-block
	add_image_size( 'masthead', 1120, 2240 );
	add_image_size( 'masthead-2x', 2240, 4480 );
	add_image_size( 'masthead-half', 560, 1120 );

	// Logo Slider
	add_image_size( 'logo', 400, 800 );
	add_image_size( 'logo-2x', 800, 1600 );

	// Video cover
	add_image_size( 'video-cover', 1760, 922, [ 'center', 'center' ] );
	add_image_size( 'video-cover-2x', 3520, 1844, [ 'center', 'center' ] );

	// Person card
	add_image_size( 'person-card', 500, 630, [ 'center', 'center' ] );
	add_image_size( 'person-card-2x', 1000, 1260, [ 'center', 'center' ] );

	// Icons
	add_image_size( 'icon', 120, 220 );
	add_image_size( 'icon-2x', 240, 480 );

	// PDG
	add_image_size( 'pdg-square', 320, 320, [ 'center', 'center' ] );
	add_image_size( 'pdg-square-2x', 640, 640, [ 'center', 'center' ] );

	add_image_size( 'pdg-small', 684, 388, [ 'center', 'center' ] );
	add_image_size( 'pdg-small-2x', 1368, 776, [ 'center', 'center' ] );
} );
