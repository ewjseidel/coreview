<?php

add_action( 'wp_enqueue_scripts', function () {

	$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

	wp_enqueue_style( 'coreview-fonts', COREVIEW_THEME_FONTS, [], COREVIEW_THEME_VERSION );

	wp_enqueue_style( 'coreview-theme', get_template_directory_uri() . "/assets/css/theme{$suffix}.css", [], COREVIEW_THEME_VERSION );


	wp_enqueue_script( 'coreview-theme', get_template_directory_uri() . "/assets/js/theme{$suffix}.js", [
		'jquery',
		'wp-hooks'
	], COREVIEW_THEME_VERSION, true );

	wp_enqueue_script( 'coreview-fontawesome', get_template_directory_uri() . "/assets/js/fontawesome{$suffix}.js", [], COREVIEW_THEME_VERSION, true );

	wp_localize_script( 'coreview-theme', 'coreView', [
		'restUrl'   => esc_url_raw( rest_url() ),
		'restNonce' => wp_create_nonce( 'wp_rest' ),
		'usaId'     => \Coreview\Taxonomies\PartnerRegions::usaId(),
	] );

} );