<?php

add_action( 'admin_init', function () {
	add_menu_page( 'Blocks', 'Reusable Blocks', 'edit_pages', 'edit.php?post_type=wp_block', '', 'dashicons-archive' );
} );