<?php

add_action( 'login_enqueue_scripts', function () {

	$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

	wp_enqueue_style( 'coreview-fonts', COREVIEW_THEME_FONTS, [], COREVIEW_THEME_VERSION );

	wp_enqueue_style( 'coreview-fonts2', 'https://use.typekit.net/vui0zyj.css', [], COREVIEW_THEME_VERSION );

	wp_enqueue_style( 'coreview-login', get_template_directory_uri() . "/assets/css/login{$suffix}.css", [], COREVIEW_THEME_VERSION );

} );