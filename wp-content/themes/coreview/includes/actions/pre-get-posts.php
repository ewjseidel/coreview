<?php

add_action( 'pre_get_posts', function ( WP_Query $query ) {
	if ( $query->is_search() && $query->is_main_query() ) {
		// Allow filtering search results by post type.
		$postType = filter_input( INPUT_GET, 'post-type', FILTER_SANITIZE_STRING );
		if ( ! empty( $postType ) && post_type_exists( $postType ) ) {
			$query->set( 'post_type', $postType );
		}
	}
} );