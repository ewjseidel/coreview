<?php

add_action( 'wp_head', function() {

	if (is_singular( 'post' )) {
		?>
		<script type='application/ld+json'>
		{
		"@context": "http://schema.org",
		"@type": "blogPosting",
		"mainEntityOfPage": {
		"@type": "WebPage",
		"@id": "<?php echo get_permalink(get_the_ID());?>"
		},
		"headline": "<?php echo get_the_title(get_the_ID());?>",
		"image": [
		"<?php $image = get_the_post_thumbnail(get_the_ID()); if (!empty($image)){ echo $image; } else { echo get_stylesheet_directory_uri() . '/assets/img/logo-main.png'; } ?>"
		],
		"datePublished": "<?php echo get_the_date("Y-m-d", get_the_ID()); ?>",
		"dateModified": "<?php echo get_the_modified_date("Y-m-d", get_the_ID()); ?>",
		"author": {
		"@type": "Organization",
		"name": "CoreView"
		},
		"publisher": {
		"@type": "Organization",
		"name": "CoreView",
		"logo": {
		"@type": "ImageObject",
		"url": "<?php echo get_stylesheet_directory_uri() ?>/assets/img/logo-main.png"
		}
		},
		"description": "<?php echo get_the_excerpt(get_the_ID()); ?>"
		}
        </script>
        <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "BreadcrumbList",
            "itemListElement":
            [
                {
                "@type": "ListItem",
                "position": 1,
                "name": "HOME",
                "item": "<?php echo get_site_url()?>"
                },
                {
                "@type": "ListItem",
                "position": 2,
                "name": "BLOG",
                "item": "<?php echo get_site_url();?>/blog"
                },
                {
                "@type": "ListItem",
                "position": 3,
                "name": "<?php echo get_the_title(get_the_ID());?>",
                "item": "<?php echo get_permalink(get_the_ID());?>"
                }
            ]
        }
        </script>

		<?php
	}
	elseif (is_page('contact-us')){
		?>
		<script type="application/ld+json">
		{ "@context" : "http://schema.org",
		"@type" : "Organization",
		"url" : "https://www.coreview.com/contact-us/",
		"contactPoint" : [
			{ "@type" : "ContactPoint",
			"telephone" : "+1-800-436-8183",
			"email": "sales@coreview.com",
			"contactType" : "customer service",
			"contactOption" : "TollFree",
			"areaServed" : "US"
			} , {
			"@type" : "ContactPoint",
			"telephone" : "+39-029-926-5813",
			"email": "sales@coreview.com",
			"contactType" : "customer service",
			"contactOption" : "TollFree",
			"areaServed" : "EMEA"
			} ] }
		</script>
		<?php
	}
	elseif (is_page('home')){
		?>
		<script type="application/ld+json">
		{ "@context": "http://schema.org",
		"@type": "Organization",
		"name": "CoreView",
		"legalName" : "CoreView",
		"url": "<?php echo get_site_url();?>",
		"logo": "<?php echo get_stylesheet_directory_uri() ?>/assets/img/logo-main.png",
		"foundingDate": "2014",
		"sameAs": [ 
		"https://twitter.com/CoreView_inc",
		"https://www.linkedin.com/company/4ward365"
		]}
		</script>
		<?php
	}
	else {
		$terms = get_the_terms(get_the_ID(), 'cv-resource-type');
		if (!empty($terms)){
			foreach($terms as $term){
				if ($term->slug === 'demo-video'){
					?>

					<script type="application/ld+json">
					{
					"@context": "https://schema.org",
					"@type": "VideoObject",
					"name": "<?php echo get_the_title(get_the_ID());?>",
					"description": "<?php echo get_the_excerpt(get_the_ID());?>",
					"publisher": {
						"@type": "Organization",
						"name": "CoreView",
						"logo": {
						"@type": "ImageObject",
						"url": "https://www.coreview.com/wp-content/uploads/2018/09/4W_CoreView_NoTag_Horizontal_White_Horizontal.png"
						}
					},
					"embedUrl": "https://youtu.be/7EESRGsjWro"
					}
					</script>

					<?php
				};
			}
		}
		
	}
	if (is_singular( 'cv-resource')) {
		?>
        <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "BreadcrumbList",
            "itemListElement":
            [
                {
                "@type": "ListItem",
                "position": 1,
                "name": "HOME",
                "item": "<?php echo get_site_url()?>"
                },
                {
                "@type": "ListItem",
                "position": 2,
                "name": "RESOURCES",
                "item": "<?php echo get_site_url();?>/resources"
                },
                {
                "@type": "ListItem",
                "position": 3,
                "name": "<?php echo get_the_title(get_the_ID());?>",
                "item": "<?php echo get_permalink(get_the_ID());?>"
                }
            ]
        }
        </script>


		<?php
	}

	if (is_singular( 'cv-news-item')) {
		?>
        <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "BreadcrumbList",
            "itemListElement":
            [
                {
                "@type": "ListItem",
                "position": 1,
                "name": "HOME",
                "item": "<?php echo get_site_url()?>"
                },
                {
                "@type": "ListItem",
                "position": 2,
                "name": "News & Events",
                "item": "<?php echo get_site_url();?>/news-events"
                },
                {
                "@type": "ListItem",
                "position": 3,
                "name": "<?php echo get_the_title(get_the_ID());?>",
                "item": "<?php echo get_permalink(get_the_ID());?>"
                }
            ]
        }
        </script>


		<?php
	}

	if (is_singular( 'cv-event')) {
		?>
        <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "BreadcrumbList",
            "itemListElement":
            [
                {
                "@type": "ListItem",
                "position": 1,
                "name": "HOME",
                "item": "<?php echo get_site_url()?>"
                },
                {
                "@type": "ListItem",
                "position": 2,
                "name": "News & Events",
                "item": "<?php echo get_site_url();?>/news-events"
                },
                {
                "@type": "ListItem",
                "position": 3,
                "name": "<?php echo get_the_title(get_the_ID());?>",
                "item": "<?php echo get_permalink(get_the_ID());?>"
                }
            ]
        }
        </script>


		<?php
	}

} );
