<?php

the_post();
get_header();

$postData = \Coreview\Models\Event::create(get_post());

$postTags = $postData->getTags();


?>

    <div class="grid__row">
        <div class="grid__col col-10/12@lg offset-1/12@lg">

            <article class='single-cv-event post-type-template'>
                <header class='single-cv-event__post-header'>
                    <div class="single-cv-event__tax-title alignfull">
                        <div class="container">
                            <p>
	                            <?php if ($postData->getNextPost()):?>
                                    <a class="single-cv-news-item__tax-title-next" href="<?php echo get_the_permalink($postData->getNextPost())?>">
                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" height="14px" width="8px" viewBox="0 0 8 14" style="enable-background:new 0 0 8 14;" xml:space="preserve">
                                            <polygon points="8,0.3 8,13.7 0,7 "></polygon>
                                        </svg>
                                    </a>
	                            <?php endif;?>
                                Events
                                <?php if ($postData->getPreviousPost()):?>
                                    <a class="single-cv-news-item__tax-title-prev" href="<?php echo get_the_permalink($postData->getPreviousPost())?>">
                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" height="14px" width="8px" viewBox="0 0 8 14" style="enable-background:new 0 0 8 14;" xml:space="preserve">
                                            <polygon points="0,13.7 0,0.3 8,7 "></polygon>
                                        </svg>
                                    </a>
	                            <?php endif;?>
                            </p>
                            <?php //TODO: Add primary taxonomy tag ?>
                        </div>
                    </div>

                    <div class='single-cv-event__breadcrumbs'>
                        <ul class=''>
                            <?php //TODO: Add breadcrumb package ?>
                            <li><a href="<?php echo get_site_url();?>">Home</a></li>
                            <li><a href="<?php echo get_site_url("", "/news-events")?>">News & Events</a></li>
                            <li><a href="#"><?php echo the_title();?></a></li>
                        </ul>
                    </div>
                </header>

                <div class="single-cv-event__main">
                    <div class="single-cv-event__inner">
                        <div class="single-cv-event__post-date">
                            <p><?php echo get_the_date('M j Y');?></p>

                        </div>
                        <div class="single-cv-event__post-title">
                            <h1><?php echo $postData->PostTitle();?></h1>
                        </div>
                        <div class="single-cv-event__post-meta grid__row">
                            <div class="single-cv-event__post-when grid__col col-3/12@lg">
                                <div class="single-cv-event__post-when-image">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
                                        x="0px" y="0px" height="25px" width="25px"
                                        viewBox="-280 372 50 50" xml:space="preserve">
                                        <path d="M-269.5,395.7v2.5h-2.5v-2.5H-269.5z M-253.8,382.5h-2.5v-2.5h2.5V382.5z M-240.6,398.2v-2.5
                                            h2.5v2.5H-240.6z M-256.3,411.4h2.5v2.5h-2.5V411.4z M-236.7,396.9c0-10.2-8.2-18.4-18.4-18.4s-18.4,8.2-18.4,18.4
                                            c0,10.2,8.2,18.4,18.4,18.4S-236.7,407.1-236.7,396.9z M-234.2,396.9c0,11.5-9.4,20.9-20.9,20.9s-20.9-9.4-20.9-20.9
                                            c0-11.5,9.4-20.9,20.9-20.9S-234.2,385.4-234.2,396.9z M-232.6,396.9c0-12.4-10-22.4-22.4-22.4c-12.4,0-22.4,10-22.4,22.4
                                            s10,22.4,22.4,22.4C-242.7,419.4-232.6,409.3-232.6,396.9z M-230.1,396.9c0,13.8-11.2,24.9-24.9,24.9c-13.8,0-24.9-11.2-24.9-24.9
                                            s11.2-24.9,24.9-24.9C-241.3,372-230.1,383.2-230.1,396.9z M-244.6,386.2l1.6,1.9l-12.2,10.6l-8.5-9.8l1.9-1.6l6.8,7.9L-244.6,386.2
                                            z"/>
                                    </svg>
                                </div>
                                <div class="single-cv-event__post-description">
                                    <p id="when-tag">When</p>
                                    <p>Begin: <?php echo $postData->eventStartDate();?></p>
                                    <p>End: <?php echo $postData->eventEndDate()?></p>
                                </div>
                            </div>
                            <div class="single-cv-event__post-where grid__col col-7/12@lg">
                                <div class="single-cv-event__post-where-image">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
                                        x="0px" y="0px" height="25px" width="25px" 
                                        viewBox="-287 372 36 50"  xml:space="preserve">
                                    <path d="M-267.3,412.1c1.9-2.1,3.9-4.3,5.7-6.6c1.8-2.2,3.3-4.4,4.6-6.4c2.3-3.7,3.5-6.8,3.5-9.2
                                        c0-8.5-6.9-15.5-15.5-15.5c-8.6,0-15.5,6.9-15.5,15.5c0,2.3,1.2,5.5,3.5,9.2c1.3,2,2.8,4.2,4.6,6.4c1.8,2.3,3.7,4.5,5.7,6.6
                                        c0.6,0.7,1.2,1.3,1.7,1.8C-268.5,413.4-267.9,412.8-267.3,412.1z M-251,390c0,2.9-1.4,6.4-3.9,10.5c-1.3,2.1-2.9,4.3-4.7,6.6
                                        c-1.9,2.3-3.8,4.6-5.8,6.7c-0.7,0.7-1.3,1.4-1.9,2c-0.2,0.2-0.4,0.4-0.5,0.5c-0.1,0.1-0.2,0.2-0.2,0.2l-0.9,0.9l-0.9-0.9
                                        c0,0-0.1-0.1-0.2-0.2c-0.2-0.2-0.3-0.3-0.5-0.5c-0.6-0.6-1.2-1.3-1.9-2c-2-2.1-4-4.4-5.8-6.7c-1.8-2.3-3.4-4.5-4.7-6.6
                                        c-2.5-4.1-3.9-7.6-3.9-10.5c0-9.9,8-18,18-18C-259.1,372-251,380.1-251,390z M-281.5,421.7v-2.5h25v2.5H-281.5z M-265.2,389.5
                                        c0-2.1-1.7-3.8-3.8-3.8c-2.1,0-3.8,1.7-3.8,3.8c0,2.1,1.7,3.8,3.8,3.8C-266.9,393.3-265.2,391.6-265.2,389.5z M-262.7,389.5
                                        c0,3.5-2.8,6.3-6.3,6.3c-3.5,0-6.3-2.8-6.3-6.3c0-3.5,2.8-6.3,6.3-6.3C-265.5,383.2-262.7,386-262.7,389.5z"/>
                                    </svg>
                                </div>
                                <div class="single-cv-event__post-description">
                                    <p id="where-tag">Where</p>
                                    <?php echo $postData->getVenueData();?>
                                </div>
                            </div>
                        </div>

                        <div class="single-cv-event__content">
                            <?php echo $postData->postContent(); ?>


                        </div>

	                    <?php
	                    if ( ! empty( $postTags ) ): ?>
                            <div class="single-cv-event__tags">
                                <h2>Tags</h2>
                                <ul class="">
				                    <?php foreach ( $postTags as $tag ):
					                    ?>
                                        <li><a href="<?php echo site_url("/news-events/?general-tags=$tag->term_id#archive-results"); ?>">
							                    <?php echo $tag->name; ?></a></li>
				                    <?php endforeach; ?>
                                </ul>
                            </div>
	                    <?php endif;?>


                    </div>



                </div>
            </article>

        </div>

    </div>

    <div class="grid__row">
        <div class="grid__col">
            <?php get_template_part("partials/related", "events"); ?>
        </div>

    </div>


<?php get_footer();

