<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">

		<?php wp_head(); ?>
        <link rel="stylesheet" href="https://use.typekit.net/vui0zyj.css">

    </head>

    <body itemscope itemtype="http://schema.org/WebPage" <?php body_class(); ?>>

        <div id="page" class="site">
            <header itemscope itemtype="http://schema.org/WPHeader" class="portal-site-header <?php echo headerClass();?> " role="banner">
				<?php get_template_part( 'partials/partner-portal-header' ); ?>
            </header><!-- .site-header -->

            <main id="content" class="site-content">

                <div class="container">