<?php
/*
 * Plugin Name: Marketo for Narwhals
 * Description: Shortcodes to embed Marketo forms with support for multi step forms and field prefilling. Also adds a setting screen to input Marketo credentials.
 * Author: Narwhal Digital
 * Author URI: http://www.narwhal.digital
 * Plugin URI: http://www.narwhal.digital
 * Developer: Chris Davis of Narwhal Digital
 * Developer URI: http://www.narwhal.digital
 * Version: 0.1.1
 *
 * Copyright 2017 Narwhal Digital
 */

define( 'MARNARWHAL_PLUGIN_DIR', __DIR__ );
define( 'MARNARWHAL_PLUGIN_VER', '0.1.1' );
define( 'MARNARWHAL_PLUGIN_URL', plugins_url( '', __FILE__ ) );

require( MARNARWHAL_PLUGIN_DIR . '/includes/marketo-ajax.php' );
require( MARNARWHAL_PLUGIN_DIR . '/includes/marketo-api.php' );
require( MARNARWHAL_PLUGIN_DIR . '/includes/marketo-shortcodes.php' );
require( MARNARWHAL_PLUGIN_DIR . '/includes/marketo-assets.php' );
require( MARNARWHAL_PLUGIN_DIR . '/includes/marketo-head.php' );

require( MARNARWHAL_PLUGIN_DIR . '/classes/Options.php' );
