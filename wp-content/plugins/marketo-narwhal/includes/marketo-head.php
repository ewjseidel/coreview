<?php

add_action( 'wp_head', function () {
	?>
    <script type="text/javascript">
        (function () {
            var didInit = false;

            function initMunchkin() {
                if (didInit === false) {
                    didInit = true;
                    Munchkin.init('<?php echo carbon_get_theme_option( 'marketo_id' ); ?>');
                }
            }

            var s = document.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = '//munchkin.marketo.net/munchkin.js';
            s.onreadystatechange = function () {
                if (this.readyState === 'complete' || this.readyState === 'loaded') {
                    initMunchkin();
                }
            };
            s.onload = initMunchkin;
            document.getElementsByTagName('head')[0].appendChild(s);
        })();
    </script>
	<?php
} );
