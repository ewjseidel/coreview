<?php

function marnarwhal_add_js() {
	wp_enqueue_script( 'marketo', MARNARWHAL_PLUGIN_URL . '/js/marketo.js', array( 'jquery' ), false, true );
}

add_action( 'wp_enqueue_scripts', 'marnarwhal_add_js' );
