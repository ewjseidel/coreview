<?php

add_action( 'wp_ajax_nopriv_get_marketo_fields', 'get_marketo_fields' );
add_action( 'wp_ajax_get_marketo_fields', 'get_marketo_fields' );
function get_marketo_fields() {
	$fields = carbon_get_theme_option( 'marketo_prefill' );

	$leads               = new \MarketoApi();
	$leads->filterType   = "cookie";
	$leads->filterValues = array( $_COOKIE['_mkto_trk'] );
	$leads->fields       = explode( ',', $fields );

	wp_send_json( $leads->getData() );
}
