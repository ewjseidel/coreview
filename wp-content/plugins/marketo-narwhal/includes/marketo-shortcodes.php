<?php

/**
 * Creates a shortcode to add marketo form to page.
 * Includes support for Shortcakes
 */

add_action( 'init', 'sc_marketoform_register' );
function sc_marketoform_register() {
	add_shortcode( 'marketo_form', 'sc_marketoform' );
}

function sc_marketoform( $attr ) {
	$atts = shortcode_atts( array(
		'id'                => 0,
		'next-id'           => null,
		'prefill'           => false,
		'paywall'           => false,
		'captcha'           => false,
		'redirect-url'      => null,
		'redirect-callback' => null,
	), $attr );

	if ( $atts['next-id'] === '' ) {
		$atts['next-id'] = null;
	}

	if ( $atts['redirect-url'] === '' ) {
		$atts['redirect-url'] = null;
	}

	if ( $atts['redirect-callback'] === '' ) {
		$atts['redirect-callback'] = null;
	}

	if ( is_admin() ) {
		return '<h3>Loading Marketo Form</h3>';
	}

	wp_enqueue_script( 'marketo-forms', '//' . carbon_get_theme_option( 'marketo_base_url' ) . '/js/forms2/js/forms2.min.js', array( 'jquery' ), 1, true );
	if ( $atts['captcha'] === 'true' ) {
		wp_enqueue_script( 'recaptcha', 'https://www.google.com/recaptcha/api.js', array(), false, true );
	}

	ob_start();
	?>
    <form id="mktoForm_<?php echo $atts['id']; ?>"></form>
	<?php if ( $atts['next-id'] ) : ?>
        <form id="mktoForm_<?php echo $atts['next-id']; ?>"></form>
	<?php endif; ?>

	<?php if ( $atts['captcha'] === 'true' ) : ?>
        <div id="marketo_captcha"></div>
	<?php endif; ?>

    <script>
        jQuery(function () {
            Marketo.init({
                prefill:        <?php echo $atts['prefill'] === 'true' ? 'true' : 'false'; ?>,
                paywall:        <?php echo $atts['paywall'] === 'true' ? 'true' : 'false'; ?>,
                marketo_url:    "<?php echo carbon_get_theme_option( 'marketo_base_url' ) ?>",
                marketo_id:     "<?php echo carbon_get_theme_option( 'marketo_id' ) ?>",
                form_id:        <?php echo $atts['id']; ?>,
                next_form_id:   <?php echo isset( $atts['next-id'] ) ? $atts['next-id'] : 'null'; ?>,
                redirect_url:   <?php echo isset( $atts['redirect-url'] ) ? '"' . $atts['redirect-url'] . '"' : 'null'; ?>,
                redirect_callback:   <?php echo isset( $atts['redirect-callback'] ) ? '"' . $atts['redirect-callback'] . '"' : 'null'; ?>,
                invalid_domains: <?php echo json_encode( explode(',', carbon_get_theme_option( 'marketo_blacklist' ) ) ); ?>,

            });
        });
    </script>

	<?php
	return ob_get_clean();
}
