<?php

class MarketoApi {
	private $host;
	private $clientId;
	private $clientSecret;
	public $filterType; //field to filter off of, required
	public $filterValues; //one or more values for filter, required
	public $fields;//one or more fields to return
	public $batchSize;
	public $nextPageToken;//token returned from previous call for paging

	public function __construct() {
		$this->host         = 'https://' . carbon_get_theme_option( 'marketo_id' ) . '.mktorest.com/';
		$this->clientId     = carbon_get_theme_option( 'marketo_client_id' );
		$this->clientSecret = carbon_get_theme_option( 'marketo_client_secret' );
	}

	public function getData() {
		$url = $this->host . "/rest/v1/leads.json?access_token=" . $this->getToken()
		       . "&filterType=" . $this->filterType . "&filterValues=" . $this::csvString( $this->filterValues );

		if ( isset( $this->batchSize ) ) {
			$url = $url . "&batchSize=" . $this->batchSize;
		}
		if ( isset( $this->nextPageToken ) ) {
			$url = $url . "&nextPageToken=" . $this->nextPageToken;
		}
		if ( isset( $this->fields ) ) {
			$url = $url . "&fields=" . $this::csvString( $this->fields );
		}

		return file_get_contents( $url );
	}

	public function getToken() {
		$cache_key = 'marketo_rest_token';

		if ( get_transient( $cache_key ) ) {
			return get_transient( $cache_key );
		}

		$response = file_get_contents( $this->host . "/identity/oauth/token?grant_type=client_credentials&client_id=" . $this->clientId . "&client_secret=" . $this->clientSecret );
		$response = json_decode( $response );


		set_transient( $cache_key, $response->access_token, $response->expires_in );

		return $response->access_token;
	}

	private static function csvString( $fields ) {
		$csvString = '';
		$i         = 0;

		foreach ( $fields as $field ) {
			if ( $i > 0 ) {
				$csvString = $csvString . "," . $field;
			} elseif ( $i === 0 ) {
				$csvString = $field;
			}
			$i ++;
		}

		$csvString = str_replace( '&', '%26', $csvString );

		return $csvString;
	}
}
