<?php

namespace Marketo;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

class Options {
	/**
	 * Setup hooks
	 */
	public static function initialize() {
		add_action( 'carbon_fields_register_fields', [ __CLASS__, 'register_fields' ] );
	}

	public static function register_fields() {
		Container::make( 'theme_options', 'Marketo Settings' )
		         //->set_page_parent( 'pdg_dev_menu' )
		         ->add_fields( array(
			         Field::make( 'text', 'marketo_base_url', 'Base URL' ),
			         Field::make( 'text', 'marketo_id', 'ID' ),
			         Field::make( 'text', 'marketo_client_id', 'Client ID' ),
			         Field::make( 'text', 'marketo_client_secret', 'Client Secret' ),
			         Field::make( 'textarea', 'marketo_prefill', 'Prefill Fields' )
			              ->set_help_text( 'A comma-separated list of field names that will be used to prefill Marketo user data' ),
			         Field::make( 'textarea', 'marketo_blacklist', 'Rejected Domains' )
			              ->set_help_text( 'A comma-separated list of domains that will be rejected when used in form email addresses' ),


//			         Field::make( 'separator', 'marketo_global_positions', 'Global Form Placements' ),
//			         Field::make( 'text', 'marketo_newsletter_form', 'Newsletter Form ID' )
//			              ->set_width( 50 ),
//			         Field::make( 'text', 'marketo_newsletter_thankyou', 'Newsletter Form Thank You Page' )
//			              ->set_width( 50 )
//			              ->set_attribute( 'type', 'url' ),
//			         Field::make( 'text', 'marketo_progressive', 'Progressive Form ID' ),
//			         Field::make( 'text', 'marketo_blog_sidebar', 'Blog Sidebar Form ID' )
//			              ->set_width( 50 ),
//			         Field::make( 'text', 'marketo_blog_thank_you', 'Blog Sidebar Thank You URL' )
//			              ->set_width( 50 )
//		                  ->set_attribute( 'type', 'url' ),
//			         Field::make( 'text', 'marketo_marketplace_sidebar', 'Marketplace Sidebar Form ID' ),
//			         Field::make( 'text', 'marketo_resource_sidebar', 'Resource Sidebar Form ID' ),
		         ) );
	}
}

Options::initialize();
