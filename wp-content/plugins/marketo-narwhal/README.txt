=== Plugin Name ===
Contributors: Chris Davis
Tags: posts, SEO, Marketo
Requires at least: 4.8.3
Tested up to: 4.8.3
Stable tag: 4.8.3

Shortcodes to embed Marketo forms with support for multi step forms and field prefilling. Also adds a setting screen to input Marketo credentials.

== Description ==

Shortcodes to embed Marketo forms with support for multi step forms and field prefilling. Also adds a setting screen to input Marketo credentials.
