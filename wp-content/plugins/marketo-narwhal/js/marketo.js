function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

var MarketoCleanup = (function($) {
    var t = {};

    t.checkFields = function() {
        $('.mktoField').each(function() {
            var field = $(this);
            if(field.val() === '') {
                t.labelDown(field);
            } else {
                t.labelUp(field);
            }
        });
    };

    t.labelUp = function(field) {
        field.parents('.mktoFieldWrap').addClass('label-up').removeClass('label-down');
    };

    t.labelDown = function(field) {
        field.parents('.mktoFieldWrap').addClass('label-down').removeClass('label-up');
    };

    t.cleanSelects = function() {
        $('select.mktoField option[value=""]').text('');
    };

    t.init = function() {
        $('#mktoForms2BaseStyle, #mktoForms2ThemeStyle, .mktoForm style').remove();
        $('.mktoForm [style], .mktoForm').attr('style', '');

        $('.mktoFormRow').each(function() {
            var row = $(this);
            var fields = $('.mktoFormCol', this);

            row.addClass('mktoFormRow--count-' + fields.length);
            row.addClass('mktoFormRow--has-fields');
        });

        //t.cleanSelects();

        //$('input.mktoField').attr('placeholder', '');

        var textareas = $('textarea.mktoField');

        textareas.parents('.mktoFieldWrap').addClass('label-static');

        textareas.closest('.mktoFormCol').addClass('no-border');

        $('.mktoCheckboxList').parents('.mktoFieldWrap')
            .addClass('label-basic checkbox-list')
            .closest('.mktoFormCol').addClass('mktoFormCol--no-line');

        $('.mktoButtonWrap').addClass('wp-block-button is-style-squared');
        $('.mktoButton').addClass('wp-block-button__link has-background has-blue-background-color');

        var forms = $('form[id^=mktoForm]');

        forms.on('focus', '.mktoField', function(e) {
            t.labelUp($(this));
        });

        forms.on('blur', '.mktoField', function(e) {
            var field = $(this);
            if(field.val() === '') {
                t.labelDown(field);
            } else {
                t.labelUp(field);
            }
        });

        var style_blocks = $('head style');
        style_blocks.each(function() {
            var style_block = $(this);

            if(style_block.text().indexOf('.mktoForm') !== -1) {
                style_block.remove();
            }
        });

        // forms.on('change', 'select.mktoField', function(e) {
        //     setTimeout(t.cleanSelects, 100);
        // });
    };

    return t;
})(jQuery);

var MarketoErrors = (function () {
    var t = {};

    t.errors = {};

    t.addError = function (key, message) {
        t.errors[key] = message;
    };

    t.removeError = function (key) {
        delete t.errors[key];
    };

    t.hasErrors = function () {
        for (var key in t.errors) {
            if (t.errors.hasOwnProperty(key)) {
                return true;
            }
        }
        return false;
    };

    t.init = function () {
        t.errors = {};
    };

    return t;
})();

var Marketo = (function ($) {
    var t = {};

    t.form_data = {};

    t.form = null;

    t.redirect_callback = null;

    t.redirect_url = null;

    t.invalid_domains = [];

    t.fixCheckboxValidation = function (form) {
        form.onValidate(function (native) {
            var formEl = form.getFormElem()[0],
                topInvalid = formEl.querySelector(".mktoInvalid"),
                topInvalidCheckbox = formEl.querySelector(".mktoInvalid.mktoCheckboxList"),
                errorMessage = 'This field is required.';

            if (!native && topInvalidCheckbox === topInvalid) {
                form.showErrorMessage(errorMessage, MktoForms2.$(topInvalidCheckbox));
            }
        });
    };

    t.checkEmail = function (form) {
        form.onValidate(function () {
            t.validateBusinessEmail(form);
        });

        $('html,body').on('blur','#Email', function () {
            t.validateBusinessEmail(form);
        });
    };

    t.validateBusinessEmail = function (form) {
        var email = form.vals().Email;

        if (email) {
            if (!t.isGoodEmail(email)) {
                form.submitable(false);
                var emailElem = form.getFormElem().find("#Email");
                form.showErrorMessage("Please use a business email.", emailElem);
            } else {
                form.submitable(true);
            }
        }
    };

    t.isGoodEmail = function (email) {
        var invalidDomains = t.invalid_domains;
        for (var i = 0; i < invalidDomains.length; i++) {
            var domain = invalidDomains[i].trim();
            if (email.indexOf(domain) !== -1) {
                return false;
            }
        }

        return true;
    };

    t.prefill = function (form) {
        var mkt_usr;

        $.post("/wp-admin/admin-ajax.php", {
            action: 'get_marketo_fields',
        }).done(function (data) {
            var mkt_obj = $.parseJSON(data);

            var status = mkt_obj.success;
            if ($.trim(status) === "true") {
                var mkt_array = mkt_obj.result;
                mkt_usr = mkt_array[0];
            }

            if (mkt_usr !== undefined) {
                //TODO: Update this map with all prefill fields
                var form_values = {
                    "FirstName": mkt_usr.firstName,
                    "LastName": mkt_usr.lastName,
                    "Email": mkt_usr.email,
                    "Company": mkt_usr.company,
                    "Country": mkt_usr.country,
                    "Phone": mkt_usr.phone,
                    "emailOptin": mkt_usr.emailOptin,
                    "Unsubscribed": mkt_usr.unsubscribed
                };
            }

            form.vals(form_values);
            MarketoCleanup.checkFields();

            t.checkEmail(form);
            // t.fixCheckboxValidation(form);
        });
    };

    t.collectData = function (fields) {
        for (var i in fields) {
            t.form_data[i] = fields[i];
        }
    };

    // MarketoErrors.hasErrors() always returns 'false'
    // This function doesn't get called anymore because it allows bad forms to be submitted due to this issue.
    t.onValidate = function (form) {
        form.onValidate(function () {
            if (MarketoErrors.hasErrors()) {
                form.submittable(false);
            } else {
                form.submittable(true);
            }
        });
    };

    t.processSecondForm = function (form, options) {
        t.collectData(form.getValues());

        form.getFormElem().hide();

        MktoForms2.loadForm("//" + options.marketo_url, options.marketo_id, options.next_form_id, function (form) {

            MarketoErrors.init();

            MarketoCleanup.init();

            if (options.prefill) {
                t.prefill(form);
            }
            else {
                t.checkEmail(form);
            }

            // t.onValidate(form);

            form.onSuccess(function () {
                t.collectData(form.getValues());

            });

        });

        var top = ($("#mktoForm_" + options.next_form_id).offset().top) - 40;

        $('html, body').animate({
            scrollTop: top
        }, 250);
    };

    t.skipStep2 = function () {
        return false;
    };

    t.init = function (options) {
        if (options.redirect_url) {
            t.redirect_url = options.redirect_url;
        }

        if (options.redirect_callback) {
            t.redirect_callback = options.redirect_callback;
        }

        t.invalid_domains = options.invalid_domains;

        MktoForms2.loadForm("//" + options.marketo_url, options.marketo_id, options.form_id, function (form) {
            t.form = form;

            MarketoErrors.init();

            MarketoCleanup.init();

            form.onValidate(
                function(){
                    t.validateBusinessEmail(form)
                }
            );

            form.onSuccess(function () {
                console.log("false");
                t.collectData(form.getValues());

                if(typeof window.dataLayer !== 'undefined' && window.location.pathname === "/coresuite-trial/") {
                    window.dataLayer.push({
                        "event": "trialFormSubmission",
                    });
                }

                if (t.redirect_url !== null) {
                    window.location = t.redirect_url;
                    return false;
                }

                if (t.redirect_callback !== null) {
                    window[t.redirect_callback](form);
                    return false;
                }
                return false;
            });

            if (options.prefill) {
                t.prefill(form);
            } else {
                t.checkEmail(form);
            }

            if (options.next_form_id) {
                form.onSuccess(function () {
                    if (!t.skipStep2()) {
                        t.processSecondForm(form, options);
                        return false;
                    }

                    return true;
                });
            }
            // I commented this out because it was overwriting the 'submittable' boolean no matter what the email validation returned.
            // sorry.
            // t.onValidate(form);
        });

    };

    return t;
})(jQuery);
