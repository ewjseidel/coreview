<?php

namespace Coreview;

add_action( 'after_setup_theme', function () {

	// Initialize Post Types
	PostTypes\Deals::initialize();
	PostTypes\Events::initialize();
	PostTypes\NewsItems::initialize();
	PostTypes\PartnerResources::initialize();
	PostTypes\PartnershipRequests::initialize();
	PostTypes\Posts::initialize();
	PostTypes\Resources::initialize();
	PostTypes\TeamItems::initialize();


	// Register models by post type name for easy lookup
	PostTypeModelRegistry::add( Models\Page::POST_TYPE, Models\Page::class );
	PostTypeModelRegistry::add( Models\Post::POST_TYPE, Models\Post::class );
	PostTypeModelRegistry::add( Models\PartnerResource::POST_TYPE, Models\PartnerResource::class );
	PostTypeModelRegistry::add( Models\Resource::POST_TYPE, Models\Resource::class );
	PostTypeModelRegistry::add( Models\NewsItem::POST_TYPE, Models\NewsItem::class );
	PostTypeModelRegistry::add( Models\Event::POST_TYPE, Models\Event::class );


	// Initialize Taxonomies
	Taxonomies\NavMenus::initialize();
	Taxonomies\NewsTypes::initialize();
	Taxonomies\PartnerRegions::initialize();
	Taxonomies\PartnerResourceTypes::initialize();
	Taxonomies\ResourceTypes::initialize();
	Taxonomies\EventTypes::initialize();
	Taxonomies\GeneralTags::initialize();
	Taxonomies\GeneralTopic::initialize();
	Taxonomies\AnnouncementCategories::initialize();


	// Custom modules
	AllowedBlocks::initialize();
	ContentRestrictions::initialize();
	PartnerPortal::initialize();
	PartnerRoles::initialize();
	PostTypeFooters::initialize();

	// REST API modifications

	// Language Stuff

} );

add_action( 'init', function () {

	// Forms
	require dirname( __DIR__ ) . '/forms/partner-portal-deal-form.php';
	require dirname( __DIR__ ) . '/forms/partner-request-form.php';

}, 12 );
