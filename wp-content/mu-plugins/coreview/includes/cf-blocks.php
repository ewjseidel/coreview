<?php

add_action( 'carbon_fields_register_fields', function () {

	// Automatically load all PHP files in the '/cf-blocks' directory
	$iterator = new RecursiveDirectoryIterator( dirname( __DIR__ ) . '/cf-blocks' );
	foreach ( new RecursiveIteratorIterator( $iterator ) as $file ) {
		if ( $file->getExtension() === 'php' ) {
			require $file;
		}
	}

} );

function getResourceLabel($id, $postType) {

	if ($postType == "cv-resource") {
		$terms = get_the_terms($id, 'cv-resource-type');

		if ( !empty($terms) ) {
			$name = $terms[0]->name;
		} else {
			$name = "Resource";

		}


		return $name;
	}

	if ($postType == "post") {

		$name = "Blog Post";
		return $name;
	}

	if ($postType == "cv-news-item") {

		$name = "Company News";
		return $name;
	}

	return $postType;

}

function getResourceSlug($id, $postType) {

	if ($postType == "cv-resource") {
		$terms = get_the_terms($id, 'cv-resource-type');

		if ( !empty($terms) ) {
			$slug = $terms[0]->slug;
		} else {
			$slug = "resource";
		}

		return $slug;
	}

	if ($postType == "post") {

		$slug = "post";
		return $slug;
	}

	if ($postType == "cv-news-item") {

		$slug = "cv-news-item";
		return $slug;
	}

	return $postType;

}