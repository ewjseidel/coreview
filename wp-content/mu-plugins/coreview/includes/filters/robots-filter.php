<?php
add_filter('robots_txt', 'custom_robots_txt', 10,  2);

function custom_robots_txt($output, $public) {

	$robots_txt =  "Disallow: /wp-content/uploads ";

	return $robots_txt;
}