<?php

add_filter( 'rest_' . \Coreview\PostTypes\Deals::POST_TYPE . '_query', function ( $args ) {
	if ( is_user_logged_in() ) {
		// Let users ONLY access their own deals via the REST API.
		$args['author'] = get_current_user_id();
	} else {
		// Force no results. Redundant security if user is not authenticated.
		$args['post__in'] = [ 0 ];
	}

	return $args;
} );