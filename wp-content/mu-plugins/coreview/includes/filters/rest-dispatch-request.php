<?php

/**
 * Ensure the deals endpoint is only accessible to logged in users.
 */
add_filter( 'rest_dispatch_request', function ( $dispatchResult, WP_REST_Request $request, string $route ) {

	$targetBase = '/wp/v2/deals';

	if ( strpos( $route, $targetBase ) !== 0 ) {
		return $dispatchResult;
	}

	if ( is_user_logged_in() ) {
		return $dispatchResult;
	}

	if ( WP_REST_Server::READABLE !== $request->get_method() ) {
		return $dispatchResult;
	}

	return new WP_Error( 'rest_forbidden', esc_html__( 'Sorry, you are not allowed to do that.' ), [
		'status' => 403,
	] );

}, 10, 3 );
