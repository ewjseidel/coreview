<?php


add_filter( 'show_admin_bar' , function($content) {
	return ( current_user_can( 'administrator' ) ) ? $content : false;
});