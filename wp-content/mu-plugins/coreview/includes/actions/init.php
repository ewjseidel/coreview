<?php

add_action( 'init', function () {
	register_block_type( 'narwhal/marketo-form', [
		// NOTE: $args will only contain non-default values. If a value is default, it will not be set in the array.
		'render_callback' => function ( array $args ) {
			$defaults = [
				'formId'      => null,
				'title'       => '',
				'nextFormId'  => null,
				'prefill'     => true,
				'redirectUrl' => null,
				'redirectCallback' => null,
			];

			$args = wp_parse_args( $args, $defaults );

			$args['prefill'] = ($args['prefill']) ? 'true' : 'false';

			return do_shortcode( '[marketo_form id="' . $args['formId'] . '" next-id="' . $args['nextFormId'] . '" captcha="false" prefill="' . $args['prefill'] . '" redirect-url="' . $args['redirectUrl'] . '" redirect-callback="' . $args['redirectCallback'] . '" /]' );
		},
	] );

	// Initialize FlexFields. Without this no form will ever be processed.
	\FlexFields\Forms\FormHandler::initialize();
} );
