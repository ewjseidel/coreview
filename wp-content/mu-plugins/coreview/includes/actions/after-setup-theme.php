<?php

add_action( 'after_setup_theme', function () {
	\Carbon_Fields\Carbon_Fields::boot();
} );

function unregister_post_tag() {
	unregister_taxonomy_for_object_type( 'post_tag', 'post' );
}
add_action( 'init', 'unregister_post_tag' );

function detail_page_submission_endpoint() {
	add_rewrite_endpoint( 'resource-ungated', 1);
	flush_rewrite_rules();
}

add_action('init', 'detail_page_submission_endpoint');

function page_submission_endpoint() {
	add_rewrite_endpoint( 'form-submit', 4096);
	flush_rewrite_rules();
}

add_action('init', 'page_submission_endpoint');
