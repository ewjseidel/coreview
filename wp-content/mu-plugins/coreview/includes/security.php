<?php

// Disable XML-RPC.
add_filter( 'xmlrpc_enabled', '__return_false' );

// Enforce CORS on REST API.
add_action( 'rest_api_init', function () {

	remove_filter( 'rest_pre_serve_request', 'rest_send_cors_headers' );
	add_filter( 'rest_pre_serve_request', function ( $value ) {
		// https://joshpress.net/access-control-headers-for-the-wordpress-rest-api/
		// Only allow GET requests, no authentication options. This heavily restricts the REST API.
		header( 'Access-Control-Allow-Origin: ' . esc_url_raw( site_url() ) );
		header( 'Access-Control-Allow-Methods: GET' );

		return $value;
	} );

}, 15 );

// Prevent iFraming site.
add_filter( 'wp_headers', function ( $headers ) {
	$headers['X-Frame-Options'] = 'SAMEORIGIN';

	return $headers;
} );
