<?php

namespace Coreview\Forms;

use FlexFields\Fields\Field;

/**
 * Map nested data from one array to another array.
 *
 * @param array $source An array containing source data.
 * @param array $map An associative array containing keys (destinations) and values (sources) in dot notation.
 * @param array $defaults Optional. An array containing a default data structure.
 *
 * @return array
 */
function mapData( array $source, array $map, array $defaults = [] ) {
	$destination = $defaults;
	foreach ( $map as $key => $value ) {
		if ( array_has( $source, $value ) ) {
			array_set( $destination, $key, array_get( $source, $value ) );
		}
	}

	return $destination;
}

/**
 * FlexFields validation function - Used when a field is required
 *
 * @param Field $field
 */
function isRequired( Field $field ) {
	$value = $field->value;
	switch ( gettype( $value ) ) {
		case 'string':
			$value = trim( $value );
			break;
		case 'array':
			$value = array_filter( $value );
			break;
	}
	if ( empty( $value ) ) {
		$field->addError( esc_html__( 'Field is required.', 'coreview' ) );
	}
}

/**
 * Converts an array of WP_Terms to a FlexFields options array.
 *
 * @param array $terms
 *
 * @return array
 */
function wpTermArrayToFlexFieldOptions( array $terms ): array {
	$options = [];

	foreach ( $terms as $term ) {
		/* @var \WP_Term $term */
		$options[] = [
			'label' => $term->name,
			'value' => $term->term_id,
		];
	}

	return $options;
}

function isValidEmail( Field $field ) {
	$value = $field->value;
	$blacklist = explode(',', carbon_get_theme_option('marketo_blacklist'));

	foreach ($blacklist as $blacklisted_domain) {
		if (preg_match("/(" . trim($blacklisted_domain) . ")/", $value)){
			$field->addError( esc_html__( 'Please use your business email address.', 'coreview' ) );
			break;
		}
	}
}