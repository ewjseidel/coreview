<?php

namespace wpscholar\WordPress;

/**
 * Class SocialSharing
 *
 * @package wpscholar\WordPress
 */
class SocialSharing {

	/**
	 * Get a share link for Digg.
	 *
	 * @param array $args
	 *
	 * @return string
	 */
	public static function digg( array $args = [] ) {
		return 'http://digg.com/submit?' .
		       self::queryString(
			       array_merge(
				       [
					       'title' => get_the_title(),
					       'url'   => get_the_permalink(),
				       ],
				       $args
			       )
		       );
	}

	/**
	 * Get a share link for email.
	 *
	 * @param array $args
	 *
	 * @return string
	 */
	public static function email( array $args = [] ) {
		return 'mailto:?' .
		       self::queryString(
			       array_merge(
				       [
					       'subject' => get_the_title(),
					       'body'    => get_the_permalink(),
				       ],
				       $args
			       )
		       );
	}

	/**
	 * Get a share link for Facebook.
	 *
	 * @param array $args
	 *
	 * @return string
	 */
	public static function facebook( array $args = [] ) {
		return 'http://www.facebook.com/sharer.php?' .
		       self::queryString(
			       array_merge(
				       [
					       't' => get_the_title(),
					       'u' => get_the_permalink(),
				       ],
				       $args
			       )
		       );
	}

	/**
	 * Get share link for Google+. Default implementation recommends an onclick attribute.
	 *
	 * @link https://developers.google.com/+/web/share/#sharelink
	 *
	 * @param array $args
	 *
	 * @return string
	 */
	public static function googlePlus( array $args = [] ) {
		return 'https://plus.google.com/share?' .
		       self::queryString(
			       array_merge(
				       [
					       't'   => get_the_title(),
					       'url' => get_the_permalink(),
				       ],
				       $args
			       )
		       );
	}

	/**
	 * Get a share link for LinkedIn.
	 *
	 * @param array $args
	 *
	 * @return string
	 */
	public static function linkedin( array $args = [] ) {
		return 'http://www.linkedin.com/shareArticle?' .
		       self::queryString(
			       array_merge(
				       [
					       'mini'  => 'true',
					       'title' => get_the_title(),
					       'url'   => get_the_permalink(),
				       ],
				       $args
			       )
		       );
	}

	/**
	 * Get a share link for Pinterest.
	 *
	 * @param array $args
	 *
	 * @return string
	 */
	public static function pinterest( array $args = [] ) {
		return 'http://pinterest.com/pin/create/button/?' .
		       self::queryString(
			       array_merge(
				       [
					       'media' => wp_get_attachment_image_url( get_post_thumbnail_id(), 'full' ),
					       'url'   => get_the_permalink(),
				       ],
				       $args
			       )
		       );
	}

	/**
	 * Get a share link for Reddit.
	 *
	 * @param array $args
	 *
	 * @return string
	 */
	public static function reddit( array $args = [] ) {
		return 'http://www.reddit.com/submit?' .
		       self::queryString(
			       array_merge(
				       [
					       'title' => get_the_title(),
					       'url'   => get_the_permalink(),
				       ],
				       $args
			       )
		       );
	}

	/**
	 * Get a share link for Stumbleupon.
	 *
	 * @param array $args
	 *
	 * @return string
	 */
	public static function stumbleupon( array $args = [] ) {
		return 'http://www.stumbleupon.com/submit?' .
		       self::queryString(
			       array_merge(
				       [
					       'title' => get_the_title(),
					       'url'   => get_the_permalink(),
				       ],
				       $args
			       )
		       );
	}

	/**
	 * Get a share link for Twitter.
	 *
	 * @param array $args
	 *
	 * @return string
	 */
	public static function twitter( array $args = [] ) {
		return 'http://twitter.com/home/?' .
		       self::queryString(
			       array_merge(
				       [
					       'status' => get_the_title() . ' - ' . get_the_permalink(),
				       ],
				       $args
			       )
		       );
	}

	/**
	 * Convert an array into a query string.
	 *
	 * @param $params
	 *
	 * @return string
	 */
	protected static function queryString( $params ) {
		return http_build_query( array_filter( $params ), '', '&' );
	}

}
