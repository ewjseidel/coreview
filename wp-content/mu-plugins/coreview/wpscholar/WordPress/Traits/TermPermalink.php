<?php

namespace wpscholar\WordPress\Traits;

/**
 * Class TermPermalink
 *
 * @package wpscholar\WordPress\Traits
 */
trait TermPermalink {

	/**
	 * Get the term permalink.
	 *
	 * @return string
	 */
	public function termPermalink() {
		return get_term_link( $this->term, self::TAXONOMY );
	}

}
