<?php

namespace wpscholar\WordPress\Traits;

/**
 * Class TermId
 *
 * @package wpscholar\WordPress\Traits
 */
trait TermName {

	public function termName() {
		return $this->term->name;
	}

}
