<?php

namespace wpscholar\WordPress\Traits;

/**
 * Class TermId
 *
 * @package wpscholar\WordPress\Traits
 */
trait TermId {

	public function termId() {
		return $this->term->term_id;
	}

}
