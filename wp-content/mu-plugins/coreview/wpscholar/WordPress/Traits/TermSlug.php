<?php

namespace wpscholar\WordPress\Traits;

/**
 * Class TermSlug
 *
 * @package wpscholar\WordPress\Traits
 */
trait TermSlug {

	public function termSlug() {
		return $this->term->slug;
	}

}
