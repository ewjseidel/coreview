<?php

namespace wpscholar\WordPress;

/**
 * Class PostCollectionBase
 *
 * @package FDOC
 */
abstract class PostCollectionBase extends CollectionBase {

	/**
	 * Post type name
	 *
	 * @var string
	 */
	const POST_TYPE = null;

	/**
	 * Default query args
	 *
	 * @var array
	 */
	protected $default_args = [
		'ignore_sticky_posts' => true,
	];

	public $found_posts = 0;

	public $max_num_pages = 0;

	public $page = 0;

	/**
	 * Fetch items
	 *
	 * @param array|string $args
	 */
	public function fetch( $args = [] ) {

		$args = wp_parse_args( $args );

		$items = [];

		$query_args = array_merge(
			$this->default_args,
			$args,
			array_merge(
				[
					'fields'    => 'ids',
					'post_type' => static::POST_TYPE,
				],
				$this->required_args
			)
		);

		$query = new \WP_Query( $query_args );

		$this->found_posts = (int) $query->found_posts;
		$this->max_num_pages = (int) $query->max_num_pages;
		$this->page = (int) $query->get( 'paged' );

		if ( $query->have_posts() ) {
			$items = $query->posts;
		}

		$this->populate( $items );
	}

	/**
	 * Get the found objects
	 */
	public function objects() {
		return $this->collection()->map( 'get_post' );
	}

}
