<?php

use Carbon_Fields\Block;
use Carbon_Fields\Field;
use Coreview\BlockFields;
use Coreview\BlockMarkup;

Block::make( __( 'Left-Right', 'coreview' ) )

    ->add_fields( [
            Field::make( 'image', 'logo', __( 'Logo', 'coreview' ) ),
    ] )
    ->add_fields( BlockFields::headerGroup() )
    ->add_fields( [
		    Field::make( 'select', 'background_style', __('Media Background Style', 'coreview' ) )
		         ->add_options( array(
			         'none'  => __( 'None' ),
			         'light' => __( 'Light' ),
			         'dark' => __( 'Dark' ),
		         ) ),
		    Field::make( 'image', 'media_background', __( "Media Background Image ", 'coreview' ) )
    ])
	->add_fields(  [
		BlockFields::buttonGroup(1, "Left Right CTA")
	] )
    ->set_render_callback( function ( $block ) {
        $defaults = [
            'logo'      => null,
            'title'     => 'default title (FIXME!)',
            'align'     => 'text-left',
            'content'   => 'default content (FIXME!)',
            'image'     => null,
        ];

        $mediaStyle = $block[ 'background_style'];

        if ( empty( $mediaStyle )) {
	        $mediaStyle = 'dark';
        }

        $i = 0;

        $mediaBGImg = $block['media_background'];

        if ( empty ($mediaBGImg) ) {
	        $mediaBGImg = " ";
        };

        $block = wp_parse_args($block, $defaults);
        ?>
        <div class="left-right left-right__ <?php print ($block['align']); ?> wp-block-narwhal">
            <div class="left-right__inner">
                <div class="left-right__content">
                    <div class="left-right__subhead-asset">
                        <?php print wp_get_attachment_image($block['logo'],'thumb', [ 'class' => 'left-right__logo' ]); ?>
                    </div>
                    <h4 class="left-right__title"><?php print $block['title']; ?></h4>
                    <?php print $block['content']; ?>
	                <?php BlockMarkup::buttonGroup( $block['buttons'] );?>

                </div>
                <?php if ($block['image']): ?>
                    <div class="left-right__media-wrap">
                        <div class="left-right__image-wrap media-background-style--<?php echo $mediaStyle;?>">
                            <div class="left-right__image-wrap-inner">
                                <div class="left-right__image-wrap-background" style='background-image: url("<?php echo wp_get_attachment_url($mediaBGImg);?>")';></div>
                                <div class="left-right__image-wrap-dotted-circle">
                                    <?php echo file_get_contents( get_template_directory() . "/assets/img/background-icons/Big-Circle.svg");?>
                                </div>
                                <div class="left-right__image-wrap-dotted-square">
                                    <?php echo file_get_contents( get_template_directory() . '/assets/img/background-icons/square.svg' );?>
                                </div>
                            </div>
                            <?php print wp_get_attachment_image($block['image'],'large', [ 'class' => 'left-right__image' ]); ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <?php
    } );