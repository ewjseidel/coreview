<?php

use Carbon_Fields\Block;
use Carbon_Fields\Field;

// Make block here, 1 block per file.

/*
Block::make( __( 'Block name', 'coreview' ) )
     ->add_fields( [
	     Field::make( 'text', 'name', __( 'Label', 'coreview' ) ),
     ] )
     ->set_render_callback( function ( $block ) {
	     ?>
         <h1>Hello world!</h1>
	     <?php
     } );
 */