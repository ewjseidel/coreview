<?php

use Carbon_Fields\Block;
use Carbon_Fields\Field;

Block::make( __( 'Simple Header', 'coreview' ) )
     ->add_fields( [
	     Field::make( 'text', 'title', __( 'Title', 'coreview' ) ),
     ] )
     ->set_render_callback( function ( $block ) {
	     $defaults = [
		     'title'     => 'default title (FIXME!)',
	     ];

	     $block = wp_parse_args($block, $defaults);
	     ?>
	     <div class="simple-header wp-block-narwhal alignfull">
		     <div class="simple-header__inner">
			     <div class="simple-header__content">
				     <h1 class="simple-header__title"><?php print $block['title']; ?></h1>
			     </div>
		     </div>
	     </div>
	     <?php
     } );