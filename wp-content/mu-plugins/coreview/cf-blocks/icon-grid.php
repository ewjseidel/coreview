<?php

use Carbon_Fields\Block;
use Carbon_Fields\Field;
use Coreview\BlockFields;
use Coreview\BlockMarkup;

Block::make( __( 'Icon Grid', 'coreview' ) )
     ->add_fields(BlockFields::sectionHeading())
     ->add_fields(  [
	     BlockFields::buttonGroup(1, "Section CTA")
     ] )
     ->add_fields( BlockFields::grid( [
	     Field::make( 'image', 'icon', __( "Icon Item Image"), 'coreview' ),
	     Field::make( 'text', 'headline', __( "Icon Item Headline", 'coreview' ) ),
	     Field::make( 'rich_text', 'body', __( "Icon Item Body Copy", 'coreview' ) ),
	     BlockFields::buttonGroup(1, "Icon Item CTA")
     ], [
	     'columns' => 4,
     ] ) )
     ->set_render_callback( function ( $block ) {
	     ?>
	     <div class="icon-grid wp-block-narwhal">
		     <div class="icon-grid__inner">

			     <?php

			     BlockMarkup::sectionHeading( $block );
			     BlockMarkup::grid( $block, function ( $card ) {
				     ?>
				     <div class="icon-item">
							 <div class="icon-item__image">
								 <?php echo wp_get_attachment_image( $card['icon'], "thumbnail" ); ?>
							 </div>
					     <div class="icon-item__inner">
						     <div class="icon-item__headline">
							     <?php echo esc_html( $card['headline'])?>
						     </div>
						     <div class="icon-item__body">
							     <?php echo $card['body'];?>
						     </div>
						     <?php BlockMarkup::buttonGroup( $card['buttons'] );?>
					     </div>

				     </div>
				     <?php
			     } );
			     BlockMarkup::buttonGroup( $block['buttons'] );
			     ?>

		     </div>
	     </div>
	     <?php
     } );