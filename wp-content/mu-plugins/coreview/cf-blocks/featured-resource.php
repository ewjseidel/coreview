<?php

use Carbon_Fields\Block;
use Carbon_Fields\Field;
use Coreview\BlockFields;
use Coreview\BlockMarkup;

Block::make( __( 'Featured Resource', 'coreview' ) )

     ->add_fields( [
	     Field::make( 'text', 'subhead_text', __( 'Subhead Text', 'coreview' ) )

     ] )
     ->add_fields( BlockFields::headerGroup() )
     ->add_fields(  [
	     BlockFields::buttonGroup(1, "Left Right CTA")
     ] )
     ->set_render_callback( function ( $block ) {
	     $defaults = [
		     'subhead_type' => 'none',
		     'logo'      => null,
		     'title'     => 'default title (FIXME!)',
		     'align'     => 'text-left',
		     'content'   => 'default content (FIXME!)',
		     'image'     => null,
	     ];


	     $block = wp_parse_args($block, $defaults);
	     ?>
	     <div class="featured-resource <?php print ($block['align']); ?> wp-block-narwhal">
		     <div class="featured-resource__inner">
			     <div class="featured-resource__content">
				     <div class="featured-resource__subhead-asset">
					     <p><?php echo $block['subhead_text'];?></p>
				     </div>
				     <h4 class="featured-resource__title"><?php print $block['title']; ?></h4>
				     <?php print $block['content']; ?>
				     <?php BlockMarkup::buttonGroup( $block['buttons'] );?>

			     </div>
			     <?php if ($block['image']): ?>
				     <div class="featured-resource__media-wrap">
					     <div class="featured-resource__image-wrap";>
						     <?php print wp_get_attachment_image($block['image'],'large', [ 'class' => 'featured-resource__image' ]); ?>
					     </div>
				     </div>
			     <?php endif; ?>
		     </div>
	     </div>
	     <?php
     } );