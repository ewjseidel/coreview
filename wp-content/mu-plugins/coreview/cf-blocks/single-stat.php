<?php

use Carbon_Fields\Block;
use Carbon_Fields\Field;
use Coreview\BlockFields;

Block::make( __('Single Stat', 'coreview'))
	->add_fields( [
		Field::make( 'text', 'subhead', __( 'Subhead', 'coreview' ) ),
		Field::make( 'rich_text', 'body', __( 'Stat Copy', 'coreview' ) ),
		Field::make( 'complex', 'link_text_group', __( 'Link Text Group', 'coreview' ) )
			->set_layout( 'tabbed-vertical' )
			->add_fields( [
				Field::make( 'rich_text', 'link_text', __( 'Link Text', 'coreview' ) )
			])
	])
	->set_render_callback( function ( $block ) {
		$defaults = [
			'subhead'   => 'Placeholder Subhead text',
			'body'      => 'Placeholder body text',
		];

		$block = wp_parse_args( $block, $defaults);
		$linkGroup = $block['link_text_group'];
	?>
		<div class="single-stat wp-block-narwhal">
			<div class="single-stat__inner">
                <div class="single-stat__big-circle">
					<?php echo file_get_contents( get_template_directory() . '/assets/img/background-icons/Big-Circle.svg'   )?>
                </div>
                <div class="single-stat__dotted-square">
					<?php echo file_get_contents( get_template_directory() . '/assets/img/background-icons/square.svg' );?>

                </div>
				<div class="single-stat__subhead">
					<p><?php echo esc_html( $block['subhead'] );?></p>
				</div>
				<div class="single-stat__body">
					<p><?php echo esc_html( $block['body'] );?></p>
				</div>
				<div class="single-stat__link-text-group">
					<?php foreach($linkGroup as $link):?>
                        <?php echo $link[ 'link_text' ];?>
                    <?php endforeach;?>
				</div>
			</div>
		</div>

	<?php
	});