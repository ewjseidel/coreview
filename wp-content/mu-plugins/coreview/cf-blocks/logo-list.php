<?php

use Carbon_Fields\Block;
use Carbon_Fields\Field;
use Coreview\BlockFields;
use Coreview\BlockMarkup;

Block::make( __( 'Logo List', 'coreview' ) )
	->add_fields(BlockFields::sectionHeading())
	->add_fields( [
		Field::make( 'complex', 'list_row', __( 'List Row', 'coreview' ) )
			->set_max(3)
			->set_layout( 'tabbed-horizontal' )
			->add_fields( [
				Field::make( 'complex', 'logo_item', __( 'Logo Item', 'coreview' ) )
					->set_max(4)
					->set_layout( 'tabbed-vertical' )
					->add_fields( [
						Field::make( 'image', 'logo_image', __( 'Logo Image', 'coreview' ) )
					])
			] )
	] )
	->set_render_callback( function( $block )  {
			$listRow = $block['list_row'];
			?>
				<div class="logo-list wp-block-narwhal">
					<div class="logo-list__inner">
						<?php echo BlockMarkup::sectionHeading( $block );?>
						<div class="logo-list__collection">

							<?php foreach( $listRow as $row ):?>
								<div class="logo-list__row">
									<?php $logoItem = $row['logo_item']?>
									<?php foreach( $logoItem as $logo):?>
										<div class="logo-list__logo-image">
											<?php echo wp_get_attachment_image( $logo['logo_image'], "medium");?>
										</div>
									<?php endforeach;?>
								</div>
							<?php endforeach;?>
						</div>
					</div>
				</div>

			<?php
			} );

