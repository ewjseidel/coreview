<?php

use Carbon_Fields\Block;
use Carbon_Fields\Field;
use Coreview\BlockFields;
use Coreview\BlockMarkup;

Block::make( __( 'Regular Header', 'coreview' ) )
    ->add_fields( [ Field::make( 'text', 'subtitle', __( 'Subtitle', 'coreview' ) ) ])
    ->add_fields( BlockFields::headerGroup() )
        ->add_fields(   [
        Field::make( 'checkbox', 'bottom_align_image', __( 'Align Image To Bottom Of Container', 'coreview' ) )
        ->set_option_value( 'yes' )
    ] )
	->add_fields(  [
		BlockFields::buttonGroup(2, "Regular Header CTA")
	] )
    ->set_render_callback( function ( $block ) {
        $defaults = [
            'subtitle'  => '',
            'title'     => 'default title (FIXME!)',
            'align'     => 'text-left',
            'content'   => 'default content (FIXME!)',
            'image'     => null,
            'bottom_align_image' => false,
        ];

        $block = wp_parse_args($block, $defaults);
        ?>
        <div class="regular-header <?php print ($block['align']); ?> wp-block-narwhal">
            <div class="regular-header__inner">
                <div class="regular-header__content">
                    <p class="regular-header__subtitle"><?php print $block['subtitle']; ?></p>
                    <h1 class="regular-header__title"><?php print $block['title']; ?></h1>
                    <?php print $block['content']; ?>
	                <?php BlockMarkup::buttonGroup( $block['buttons'] );?>

                </div>
                <?php if ($block['image']): ?>
                    <div class="regular-header__media-wrap <?php if($block['bottom_align_image']){ echo '--bottom-align-image'; } ?>">
                        <div class="regular-header__image-wrap">
                            <?php print wp_get_attachment_image($block['image'],'large', [ 'class' => 'regular-header__image']); ?>
                        </div>

                        <div class="regular-header__underlay">
                            <div class="regular-header__big-circle">
			                    <?php // echo file_get_contents( get_template_directory() . '/assets/img/Big-Circle.svg'   )?>
                            </div>

                            <div class="regular-header__dotted-square">
			                    <?php // echo file_get_contents( get_template_directory() . '/assets/img/square.svg' );?>

                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <?php
    } );