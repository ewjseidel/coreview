<?php

use Carbon_Fields\Block;
use Carbon_Fields\Field;
use Coreview\BlockFields;
use Coreview\BlockMarkup;

Block::make( __( 'Contact Elements Map', 'coreview' ) )
	->add_fields(BlockFields::sectionHeading())
	->add_fields( BlockFields::grid( [
		Field::make( 'text', 'title', __( "Contact Title", 'coreview' ) ),
		Field::make( 'rich_text', 'body_copy', __( "Contact Body Copy", 'coreview' ) ),
		Field::make( 'text', 'email', __( "Contact Email", 'coreview' ) ),
	], [
		'columns' => 3,
	] ) )
	->set_render_callback( function ( $block ) {


		?>
		<div class="contact-elements-map wp-block-narwhal alignfull">

			<div class="contact-elements-map__inner">
				<?php BlockMarkup::sectionHeading( $block );?>
				<div class="contact-elements-map__content">
					<div class="contact-elements-map__content-inner">
						<?php 
							BlockMarkup::grid( $block, function ( $card ) { ?>

								<div class="contact-map-item">
									<div class="contact-map__inner">
										<div class="contact-map__title">
											<?php echo $card['title']; ?>
										</div>
										<div class="contact-map__body-copy">
											<?php echo $card['body_copy'] ?>
										</div>
										<div class="contact-map__email">
											<a href="mailto:<?php echo $card['email'];?>"><?php echo $card['email']; ?></a>
										</div>
									</div>

								</div>
								<?php
							} );
						?>
					</div>
				</div>
			</div>
		</div>
		<?php
	} );