<?php

use Carbon_Fields\Block;
use Carbon_Fields\Field;
use Coreview\BlockFields;
use Coreview\BlockMarkup;

Block::make( __('CTA Block', 'coreview'))
     ->add_fields( [
     	 Field::make( 'text', 'subhead', __( 'CTA Block Subhead', 'coreview' ) ),
	     Field::make( 'text', 'title', __( 'CTA Title', 'coreview' ) ),
	     Field::make( 'rich_text', 'copy', __( 'CTA Block Copy', 'coreview' ) ),

     ])
     ->add_fields(  [
		BlockFields::buttonGroup(1, "Section CTA")
	 ] )
     ->set_render_callback( function ( $block ) {

	     ?>
	     <div class="cta-block wp-block-narwhal">
		     <div class="cta-block__inner cta-block__layout--column">

			     <div class="cta-block__body">
                     <?php if ( !empty ( $block['subhead'])):?>
                         <div class="cta-block__subhead">
                            <p><?php echo $block['subhead'];?></p>

                         </div>
                     <?php endif;?>

				     <div class="cta-block__title">
					     <h2><?php echo $block['title'];?></h2>
				     </div>
				     <div class="cta-block__copy">
					     <p><?php echo $block[ 'copy' ];?></p>
				     </div>
				     <?php BlockMarkup::buttonGroup( $block['buttons'] );
				     ?>
			     </div>

		     </div>
			 <div class="cta-block__underlay">
				<div class="cta-block__underlay__big-circle">
					<?php echo file_get_contents( get_template_directory() . '/assets/img/background-icons/Big-Circle.svg'   )?>
				</div>
				<div class="cta-block__underlay__solid-square">
					<?php echo file_get_contents( get_template_directory() . '/assets/img/background-icons/Solid-Square.svg'   )?>
				</div>
				<div class="cta-block__underlay__coreview-c">
					<?php echo file_get_contents( get_template_directory() . '/assets/img/background-icons/Coreview-C.svg' );?>
				</div>
				<div class="cta-block__underlay__dotted-square">
					<?php echo file_get_contents( get_template_directory() . '/assets/img/background-icons/square.svg' );?>
				</div>
			 </div>
	     </div>

	     <?php
     });