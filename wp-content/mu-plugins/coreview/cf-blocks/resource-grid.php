<?php

use Carbon_Fields\Block;
use Carbon_Fields\Field;
use Coreview\BlockFields;
use Coreview\BlockMarkup;

Block::make( __( 'Resource Grid', 'coreview' ) )
     ->add_fields( [
	     Field::make( 'text', 'section_heading', __( 'Section Heading', 'coreview' ) ),
	     Field::make( 'select', 'background_style', __( 'Card Background Style', 'coreview' ) )
	          ->add_options( array(
		          'light' => __( 'Light' ),
		          'dark'  => __( 'Dark' ),
	          ) ),
	     Field::make( 'association', 'resource_post', __( 'Resource Post', 'coreview' ) )
	          ->set_max( 3 )
	          ->set_max( 3 )
	          ->set_types( array(
		          array(
			          'type'      => 'post',
			          'post_type' => 'post',
		          ),
		          array(
			          'type'      => 'post',
			          'post_type' => 'cv-resource',
		          ),
		          array(
			          'type'      => 'post',
			          'post_type' => 'cv-news-item',
		          )
	          ) )

     ] )
     ->set_render_callback( function ( $block ) {
	     $bgStyle = $block['background_style'];

	     if ( empty( $block['background_style'] ) ) {
		     $bgStyle = 'light';
	     }

	     ?>
         <div class="resource-grid wp-block-narwhal">
             <div class="resource-grid__section-heading">
                 <h2><?php echo $block['section_heading']; ?></h2>
             </div>

             <div class="resource-grid__inner grid__row">
			     <?php $resourceItems = $block['resource_post'] ?>
			     <?php foreach ( $resourceItems as $resource ): ?>
				     <?php $resourceData = get_post( $resource['id'] ); ?>
                     <div class="grid__col col-4/12@sm resource-grid__card resource-grid__type--<?php echo getResourceSlug( $resource['id'], $resource['subtype'] ) ?>">
                         <div class="resource-grid__card-background background-style--<?php echo $bgStyle; ?>">
                             <div class="resource-grid__card-image resource-grid__card-image--<?php echo getResourceSlug( $resource['id'], $resource['subtype'] ) ?>"></div>
                             <div class="resource-grid__big-circle resource-grid__big-circle--<?php echo getResourceSlug( $resource['id'], $resource['subtype'] ) ?>"></div>
                             <div class="resource-grid__big-square resource-grid__big-square--<?php echo getResourceSlug( $resource['id'], $resource['subtype'] ) ?>"></div>
                             <div class="resource-grid__small-square resource-grid__small-square--<?php echo getResourceSlug( $resource['id'], $resource['subtype'] ) ?>"></div>
                             <div class="resource-grid__coreview-c resource-grid__coreview-c--<?php echo getResourceSlug( $resource['id'], $resource['subtype'] ) ?>"></div>
                             <div class="resource-grid__dotted-square resource-grid__dotted-square--<?php echo getResourceSlug( $resource['id'], $resource['subtype'] ) ?>"></div>
                             <div class="resource-grid__card-content">
                                 <div class="resource-grid__card--type resource-grid__tag--<?php echo getResourceSlug( $resource['id'], $resource['subtype'] ) ?>">
                                     <span><?php $type = getResourceLabel( $resource['id'], $resource['subtype'] );
									     echo $type ?></span>
                                 </div>
                                 <div class="resource-grid__card-title">
                                     <a href="<?php echo get_permalink( $resource['id'] ); ?>">
                                         <p><?php echo wp_trim_words( $resourceData->post_title, 10 ) ?></p></a>
                                 </div>
                                 <div class="resource-grid__card-body">
                                     <p><?php echo wp_trim_words( $resourceData->post_excerpt, 20 ); ?></p>
                                 </div>
                                 <div class="resource-grid__cta">
                                     <a href="<?php echo get_permalink( $resource['id'] ); ?>">
									     <?php
									     if ( $type === 'Demo Video' ) {
										     echo 'Watch Video';
									     } elseif ( $type === 'Webinar' ) {
										     echo 'Watch Webinar';
									     } else {
										     echo 'Read More';
									     }
									     ?>
									     <?php echo file_get_contents( get_template_directory() . "/assets/img/background-icons/link-arrow.svg" ) ?>
                                     </a>
                                 </div>
                             </div>
                         </div>
                     </div>

			     <?php endforeach; ?>
             </div>
         </div>
	     <?php
     } );
