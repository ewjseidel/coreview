<?php

use Carbon_Fields\Block;
use Carbon_Fields\Field;
use Coreview\BlockFields;
use Coreview\BlockMarkup;

Block::make( __( 'Contact Elements', 'coreview' ) )
	->add_fields([
		Field::make( 'select', 'background_style', __('Module Background Style', 'coreview' ) )
			->add_options( array(
				'none'  => __( 'None' ),
				'light' => __( 'Light' ),
				'dark' => __( 'Dark' ),
			) ),
		Field::make( 'image', 'background_image', __( 'Background Image', 'coreview' ) )
			->set_conditional_logic([
				[ 'field' => 'background_style', 'value' => 'dark']
			])

	])
	->add_fields( BlockFields::grid( [
		Field::make( 'text', 'subhead', __( "Contact Subhead", 'coreview' ) ),
		Field::make( 'text', 'title', __( "Contact Title", 'coreview' ) ),
		Field::make( 'rich_text', 'body_copy', __( "Contact Body Copy", 'coreview' ) ),
		BlockFields::buttonGroup(1, "Bullet CTA")
	], [
		'columns' => 3,
	] ) )
	->set_render_callback( function ( $block ) {
		$defaults = [
			'background_style'     => 'light',
		];

		$block = wp_parse_args($block, $defaults);

		$moduleBG = wp_get_attachment_url($block['background_image']);

		?>
		<div class="contact-elements wp-block-narwhal">
			<div class="contact-elements__inner<?php if ( $block['background_style'] == "dark") {?> contact-elements__inner-dark<?php } ?>"> 
				<?php if ( $block['background_style'] == "dark") {?>
					<div class="contact-elements__inner-dark-overlay" style="background-image: url('<?php echo $moduleBG ?>')"></div>
					<div class="contact-elements__inner-dark-left-square">
					<?php echo file_get_contents(get_template_directory() . "/assets/img/background-icons/Solid-Square.svg");?>
					</div>
					<div class="contact-elements__inner-dark-right-square">
						<?php echo file_get_contents( get_template_directory() . "/assets/img/background-icons/Solid-Square.svg")?>
					</div>
					<div class="contact-elements__inner-dark-dotted-circle">
						<?php echo file_get_contents( get_template_directory() . "/assets/img/background-icons/Big-Circle.svg");?>
					</div>
				<?php } else { ?>
					<div class="contact-elements__inner-light-left-square">
					<?php echo file_get_contents(get_template_directory() . "/assets/img/background-icons/Solid-Square.svg");?>
					</div>
					<div class="contact-elements__inner-light-right-square">
						<?php echo file_get_contents( get_template_directory() . "/assets/img/background-icons/Solid-Square.svg")?>
					</div>
					<div class="contact-elements__inner-light-dotted-circle">
						<?php echo file_get_contents( get_template_directory() . "/assets/img/background-icons/Big-Circle.svg");?>
					</div>
				<?php } ?>
				<?php BlockMarkup::grid( $block, function ( $card ) { ?>
					<div class="contact-item">
						<div class="contact-item__inner">
							<div class="contact-item__subhead <?php if ( $card['subhead'] == "Demo") {?> contact-item__subhead-green<?php } ?>">
								<?php echo $card['subhead']; ?>
							</div>
							<div class="contact-item__headline">
								<?php echo $card['title'] ?>
							</div>
							<div class="contact-item__body">
								<?php echo $card['body_copy']; ?>
							</div>
							<?php BlockMarkup::buttonGroup( $card['buttons'] );?>
						</div>
					</div>
				<?php } ); ?>
			</div>
		</div>
		<?php 
	} );