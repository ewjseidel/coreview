<?php

use Carbon_Fields\Block;
use Carbon_Fields\Field;
use Coreview\BlockFields;
use Coreview\BlockMarkup;

Block::make( __( 'Vertical Tabber', 'tricentis' ) )
    ->add_fields( array(
        Field::make( 'text', 'heading', __( 'Tabber Heading', 'tricentis' ) ),
        Field::make( 'complex', 'tabs', __( 'Tabs', 'tricentis' ) )
            ->set_layout( 'tabbed-horizontal' )
            ->add_fields( array(
                Field::make( 'text', 'short_title', __( 'Tab Short Title', 'tricentis' ) ),
                Field::make( 'text', 'full_title', __( 'Tab Full Title', 'tricentis' ) ),
                Field::make( 'rich_text', 'content', __( 'Tab Content', 'tricentis' ) ),
                Field::make( 'image', 'image', __( 'Tab Image', 'tricentis' ) ),
                Field::make( 'image', 'icon', __( 'Tab Icon', 'tricentis' ) ),
                BlockFields::buttonGroup(1, "Left Right CTA")

            ) )

    ) )
    ->set_render_callback( function ( $block ) {
        ?>
        <div class="vertical-tabber wp-block-narwhal vertical-tabber--style-side-button-nav">
            <?php if(!empty( $block['heading'])) : ?>
                <div class="vertical-tabber__heading">
                    <h2><?php echo esc_html( $block['heading'] ); ?></h2>
                </div>
            <?php endif; ?>
            <div class="vertical-tabber__columns">
                <div class="vertical-tabber__nav">
                    <?php foreach ( $block['tabs'] as $i => $tab ) : ?>
                        <a class="vertical-tabber__nav-item <?php print ($i == 0 ? 'active' : '');?>" href="#" data-tab="<?php print $i; ?>"><h4><?php print esc_html($tab['short_title']); ?></h4></a>
                    <?php endforeach; ?>
                </div>
                <div class="vertical-tabber__tabs">
                    <?php foreach ( $block['tabs'] as $i => $tab ) : ?>
                        <div class="vertical-tabber-tab tab-<?php print $i; ?> <?php print ($i == 0 ? 'active' : '');?>">
                            <div class="vertical-tabber-tab__inner">
                                <div class="vertical-tabber-tab__content">
                                    <div class="vertical-tabber__mobile-short-title">
                                        <?php if ( ! empty( $tab['icon'] ) ) : ?>
                                            <figure class="vertical-tabber__mobile-icon">
                                                <?php echo wp_get_attachment_image( (int) $tab['icon'], 'large-2x' ); ?>
                                            </figure>
                                        <?php endif; ?>
                                        <h5><?php print esc_html($tab['short_title']); ?></h5>
                                    </div>
                                    <div class="vertical-tabber__small-square">
                                        <?php echo file_get_contents( get_template_directory() . "/assets/img/background-icons/Solid-Square.svg")?>
                                    </div>
                                    <h4 class="vertical-tabber-tab__title"><?php echo esc_html( $tab['full_title'] ); ?></h4>
                                    <?php echo apply_filters( 'the_content', $tab['content'] ); ?>
                                    <?php BlockMarkup::buttonGroup( $tab['buttons'] );?>
                                </div>
                                <?php if ( ! empty( $tab['image'] ) ) : ?>
                                <div class="vertical-tabber-tab__media-wrap">
                                    <div class="vertical-tabber__dotted-small-circle">
                                        <?php echo file_get_contents( get_template_directory() . "/assets/img/background-icons/Small-Circle.svg");?>
                                    </div>
                                    <div class="vertical-tabber__dotted-square-left">
                                        <?php echo file_get_contents( get_template_directory() . "/assets/img/background-icons/square.svg");?>
                                    </div>
                                    <div class="vertical-tabber__dotted-square-right">
                                        <?php echo file_get_contents( get_template_directory() . "/assets/img/background-icons/square.svg");?>
                                    </div>
                                    <div class="vertical-tabber__dotted-square-bottom">
                                        <?php echo file_get_contents( get_template_directory() . "/assets/img/background-icons/square.svg");?>
                                    </div>
                                    <figure class="vertical-tabber-tab__image<?php if ( ! empty( $tab['icon'] ) ) : ?> vertical-tabber-tab__image-has-icon<?php endif; ?>">
                                        <?php echo wp_get_attachment_image( (int) $tab['image'], 'large-2x' ); ?>
                                        <?php if ( ! empty( $tab['icon'] ) ) : ?>
                                            <figure class="vertical-tabber-tab__icon">
                                                <?php echo wp_get_attachment_image( (int) $tab['icon'], 'large-2x' ); ?>
                                            </figure>
                                        <?php endif; ?>
                                    </figure>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <?php
    } );
