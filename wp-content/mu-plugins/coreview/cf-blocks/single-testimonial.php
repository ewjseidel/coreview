<?php

use Carbon_Fields\Block;
use Carbon_Fields\Field;
use Coreview\BlockFields;

Block::make( __('Single Testimonial', 'coreview'))
     ->add_fields( [
     	 Field::make( 'image', 'logo', __( 'Logo Image', 'coreview' ) ),
	     Field::make( 'rich_text', 'testimonial', __( 'Testimonial Copy', 'coreview' ) ),
         Field::make( 'text', 'credit', __( 'Credit Copy', 'coreview'))

     ])
     ->set_render_callback( function ( $block ) {

	     ?>
	     <div class="single-testimonial wp-block-narwhal">
            <!--  background color for this module will be set by the background color gutenberg module -->
		     <div class="single-testimonial__inner">

			     <div class="single-testimonial__logo">
				     <p><?php echo wp_get_attachment_image( $block['logo'], "thumb")?></p>
			     </div>
			     <div class="single-testimonial__body">
                     <div class="single-testimonial__testimonial-copy">
                         <p><?php echo $block[ 'testimonial' ];?></p>
                     </div>
                     <div class="single-testimonial__testimonial-credit">
                         <p><?php echo $block['credit'];?></p>
                     </div>
			     </div>

		     </div>
	     </div>

	     <?php
     });