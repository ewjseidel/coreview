<?php

use Carbon_Fields\Block;
use Carbon_Fields\Field;
use Coreview\BlockFields;
use Coreview\BlockMarkup;

Block::make( __( 'Top Level Header', 'coreview' ) )
    ->add_fields( BlockFields::headerGroup() )
	->add_fields(  [
		BlockFields::buttonGroup(1, "Top Level Header CTA")
	] )
    ->set_render_callback( function ( $block ) {
        $defaults = [
            'title'     => 'default title (FIXME!)',
            'align'     => 'text-left',
            'content'   => 'default content (FIXME!)',
            'image'     => null,
        ];

        $block = wp_parse_args($block, $defaults);
?>
    <div class="top-level-header <?php print ($block['align']); ?> wp-block-narwhal">
        <div class="top-level-header__inner">
            <div class="top-level-header__content">
                <h1 class="top-level-header__title"><?php print $block['title']; ?></h1>
                <?php print $block['content']; ?>
	            <?php BlockMarkup::buttonGroup( $block['buttons'] );?>
            </div>
            <?php if ($block['image']): ?>
            <div class="top-level-header__media-wrap">
                <div class="top-level-header__image">
                    <?php print wp_get_attachment_image($block['image'],'large'); ?>
                </div>
                <div class="top-level-header__underlay">
                    <div class="top-level-header__big-circle">
                        <?php echo file_get_contents( get_template_directory() . '/assets/img/background-icons/Big-Circle.svg'   )?>
                    </div>
                    <div class="top-level-header__big-square">
                        <?php echo file_get_contents(get_template_directory() . "/assets/img/background-icons/Solid-Square.svg");?>

                    </div>
                    <div class="top-level-header__small-square">
                        <?php echo file_get_contents( get_template_directory() . '/assets/img/background-icons/Solid-Square.svg'   )?>
                    </div>
                    <div class="top-level-header__coreview-c">
	                    <?php echo file_get_contents( get_template_directory() . '/assets/img/background-icons/Coreview-C.svg' );?>

                    </div>
                    <div class="top-level-header__dotted-square">
		                <?php echo file_get_contents( get_template_directory() . '/assets/img/background-icons/square.svg' );?>

                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
<?php
    } );