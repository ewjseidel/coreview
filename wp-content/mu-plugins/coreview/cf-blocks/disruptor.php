<?php

use Carbon_Fields\Block;
use Carbon_Fields\Field;
use Coreview\BlockFields;
use Coreview\BlockMarkup;

Block::make( __( 'Disruptor', 'coreview' ) )
    ->add_fields( [
        Field::make('text', 'content', __( 'Content', 'coreview'))
    ] )
	->add_fields(  [
		BlockFields::buttonGroup(1, "Disruptor CTA")
	] )
    ->set_render_callback( function ( $block ) {
        $defaults = [
            'content'   => 'Disruptor Content! (FIXME)'
        ];

        $block = wp_parse_args($block, $defaults);
        ?>
        <div class="disruptor alignfull ">
            <div class="disruptor__content">
                <div class="disruptor__content-meta">
	                <?php print $block['content']; ?>
                </div>
                <?php BlockMarkup::buttonGroup( $block['buttons'] );?>

            </div>
            <span class="disruptor__close"></span>
        </div>
        <?php
    } );