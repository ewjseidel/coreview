<?php

use Carbon_Fields\Block;
use Carbon_Fields\Field;
use Coreview\BlockFields;
use Coreview\BlockMarkup;

Block::make( __( 'Form Block', 'coreview' ) )
     ->add_fields( [
	     Field::make( 'radio', 'module_layout', __( 'Module Layout', 'coreview' ) )
	          ->add_options( [
		          'column' => __( 'Column' ),
		          'row'    => __( 'Row' )
	          ] ),
	     Field::make( 'text', 'subhead', __( 'CTA Block Subhead', 'coreview' ) ),

     ] )
     ->add_fields( BlockFields::sectionHeading() )
     ->add_fields( BlockFields::formGroup() )
     ->set_render_callback( function ( $block ) {
	     $moduleLayout = $block['module_layout'];
	     if ( empty( $moduleLayout ) ) {
		     $moduleLayout = "column";
	     }

	     ?>
         <div class="form-block wp-block-narwhal">
             <div class="form-block__inner form-block__layout--<?php echo $moduleLayout; ?>">

                 <div class="form-block__body">
				     <?php if ( ! empty ( $block['subhead'] ) ): ?>
                         <div class="form-block__subhead">
                             <p><?php echo $block['subhead']; ?></p>

                         </div>
				     <?php endif; ?>

				     <?php BlockMarkup::sectionHeading( $block ); ?>

                 </div>
                 <div class="form-block__form">
				 	 <?php if ( $block['form_id'] === 'partnership_request' ) {
					     $formName = 'partner-request-form';
					     $formHandler = \FlexFields\Forms\FormHandler::getInstance();
					     $form = $formHandler->forms->getForm( $formName );
					     if ( $form ) {
							?>
								<div class="partner-request-form__bg">
                                    <div class="partner-request-form__body">
                                        <?php
                                             $form->render();
                                        ?>
                                    </div>
								</div>
							<?php
					     }
				     } else {
					     BlockMarkup::form( $block );
					 } ?>
					</div> 


             </div>
         </div>

	     <?php
     } );