<?php

use Carbon_Fields\Block;
use Carbon_Fields\Field;
use Coreview\BlockFields;
use Coreview\BlockMarkup;

Block::make( __( 'Bullet Grid', 'coreview' ) )
	->add_fields(BlockFields::sectionHeading())
	 ->add_fields(  [
		 BlockFields::buttonGroup(1, "Section CTA")
	 ] )
     ->add_fields( BlockFields::grid( [
	     Field::make( 'select', 'bullet_color', __( 'Bullet Color', 'coreview'))
			->set_options( [
			    'bullet-coreview-none'  => __( 'None' ),
				'bullet-coreview-red'   => __( 'Red' ),
				'bullet-coreview-orange'=> __( 'Orange' ),
				'bullet-coreview-green' => __( 'Green' ),
				'bullet-coreview-blue'  => __( 'Blue' ),
				'bullet-coreview-yellow'=> __( 'Yellow' )
			]),
	     Field::make( 'text', 'bullet_headline', __( "Bullet Headline", 'coreview' ) ),
	     Field::make( 'rich_text', 'bullet_body', __( "Bullet Body Copy", 'coreview' ) ),
	     BlockFields::buttonGroup(1, "Bullet CTA")
     ], [
	     'columns' => 3,
     ] ) )
     ->set_render_callback( function ( $block ) {
     	?>
        <div class="bullet-grid wp-block-narwhal">
	        <div class="bullet-grid__inner">

	    <?php

     	 BlockMarkup::sectionHeading( $block );
	     BlockMarkup::grid( $block, function ( $card ) {
		     if ( empty( $card['bullet_color'] ) ) {
			     $card['bullet_color']  = 'bullet-coreview-none';
		     };
		     ?>
		     <div class="bullet-item" data-bullet-color="<?php echo esc_html( $card['bullet_color'] );?>">
			     <div class="bullet-item__inner">
				     <div class="bullet-item__headline">
					     <?php echo esc_html( $card['bullet_headline'])?>
				     </div>
				     <div class="bullet-item__body">
					     <?php echo $card['bullet_body'];?>
				     </div>
				     <?php BlockMarkup::buttonGroup( $card['buttons'] );?>
			     </div>

		     </div>
		     <?php
	     } );
	     BlockMarkup::buttonGroup( $block['buttons'] );
	     ?>

	        </div>
        </div>
		<?php
} );