<?php

use Carbon_Fields\Block;
use Carbon_Fields\Field;

Block::make( __( 'Small Header', 'coreview' ) )
    ->add_fields( [
        Field::make( 'text', 'title', __( 'Title', 'coreview' ) ),
    ] )
    ->set_render_callback( function ( $block ) {
        $defaults = [
            'title'     => 'default title (FIXME!)',
        ];

        $block = wp_parse_args($block, $defaults);
        ?>
        <div class="small-header wp-block-narwhal alignfull">
            <div class="small-header__inner">
                <div class="small-header__content">
                    <h1 class="small-header__title"><?php print $block['title']; ?></h1>
                </div>
            </div>
        </div>
        <?php
    } );