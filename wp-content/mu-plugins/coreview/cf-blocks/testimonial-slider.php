<?php

use Carbon_Fields\Block;
use Carbon_Fields\Field;
use Coreview\BlockFields;
use Coreview\BlockMarkup;

Block::make( __( 'Testimonial Slider', 'coreview' ) )
     ->add_fields( [
     	 Field::make( 'text', 'section_heading', __( 'Section Heading', 'coreview' ) ),
	     Field::make( 'complex', 'testimonial_slides', __( 'Testimonials', 'coreview' ) )
	          ->set_max(6)
	          ->set_layout( 'tabbed-horizontal' )
	          ->add_fields( [
		          Field::make( 'image', 'person_image', __( 'Person Image', 'coreview') ),
		          Field::make( 'rich_text', 'copy', __( 'Testimonial Copy', 'coreview' ) ),
		          Field::make( 'text', 'person_name', __( 'Person Name', 'coreview' ) ),
		          Field::make( 'text', 'person_title', __( 'Person Title' ) ),
		          Field::make( 'text', 'company_name', __( 'Company Name' ) ),
		          Field::make( 'image', 'logo', __( 'Testimonial Logo' ) )
	          ] )
     ] )
     ->set_render_callback( function( $block )  {
	     ?>
	     <div class="testimonial-slider wp-block-narwhal">
		     <div class="testimonial-slider__inner">
			     <div>
				     <h2><?php echo $block['section_heading']?></h2>
			     </div>
			     <div class="testimonial-slider__collection swiper-container">
				     <?php $slides = $block['testimonial_slides'] ?>
                     <div class="swiper-wrapper">

                         <?php foreach( $slides as $slide ):?>
                             <div class="testimonial-slider__slide swiper-slide">
                                 <div class="testimonial-slider__image">
                                     <?php echo wp_get_attachment_image( $slide['person_image'], "medium");?>
                                 </div>
                                 <div class="testimonial-slider__body">
                                     <div class="testimonial-slider__copy">
                                         <p><?php echo $slide['copy'];?></p>
                                     </div>
                                     <div class="testimonial-slider__name">
                                         <p><?php echo $slide['person_name']?></p>
                                     </div>
                                     <div class="testimonial-slider__title">
                                         <p><?php echo $slide['person_title']?></p>
                                     </div>
                                     <div class="testimonial-slider__company">
                                         <p><?php echo $slide['company_name']?></p>
                                     </div>

                                 </div>
                             </div>
                         <?php endforeach;?>
                     </div>

			     </div>
                 <div class="swiper-container gallery-thumbs">
                     <div class="swiper-wrapper">
	                     <?php foreach( $slides as $slide ):?>
                             <div class="testimonial-slider__image-pagination swiper-slide">
                                 <div class="testimonial-slider__media">

                                     <?php echo wp_get_attachment_image( $slide['logo'], "medium");?>
                                 </div>
                             </div>
	                     <?php endforeach;?>
                     </div>
                     <div class="swiper-button-prev">
                        <?php echo file_get_contents( get_template_directory() . "/assets/img/swiper-button-prev.svg")?>  
                     </div>
                     <div class="swiper-button-next">
                        <?php echo file_get_contents( get_template_directory() . "/assets/img/swiper-button-next.svg")?>  
                     </div>
                 </div>
		     </div>
	     </div>

	     <?php
     } );

