<?php

use Carbon_Fields\Block;
use Carbon_Fields\Field;
use Coreview\BlockFields;
use Coreview\BlockMarkup;


Block::make( __( 'Left-Right Icon Grid', 'coreview' ) )
    ->add_fields( [
	    Field::make( 'select', 'background_style', __('Module Background Style', 'coreview' ) )
	         ->add_options( array(
		         'light' => __( 'Light' ),
		         'dark' => __( 'Dark' ),
	         ) ),
	    Field::make( 'select', 'align', __( 'Alignment', 'coreview' ) )
	         ->set_options([
			         'text-left'  => __('Text Left', 'coreview'),
			         'text-right' => __('Text Right', 'coreview'),
		         ]
	         ),
        Field::make( 'text', 'title', __( 'Title', 'coreview' ) ),
        Field::make( 'rich_text', 'content', __( 'Content', 'coreview' ) )
    ] )
	->add_fields(  [
		BlockFields::buttonGroup(1, "Text CTA")
	] )
    ->add_fields( [
	    Field::make( 'complex', 'icon_items', __( 'List Row', 'coreview' ) )
	         ->set_max(4)
	         ->set_layout( 'tabbed-horizontal' )
	         ->add_fields( [
		         Field::make( 'image', 'icon_image', __('Item Icon', 'coreview')),
		         Field::make( 'text', 'icon_title', __('Item Title', 'coreview')),
		         Field::make( 'rich_text', 'icon_content', __('Item Content', 'coreview')),
	         ] )
    ])

    ->set_render_callback( function ( $block ) {

	    $iconItems = $block['icon_items'];

	    ?>
        <div class="left-right-icon-grid <?php print ($block['align']); ?> wp-block-narwhal alignfull background-style--<?php print ($block['background_style'])?>">
            <div class="container">
                <div class="left-right-icon-grid__inner grid__row">
                    <div class="left-right-icon-grid__content grid__col col-6/12@sm">
                        <div class="left-right-icon-grid__content-inner">
                            <h2 class="left-right-icon-grid__title"><?php print $block['title']; ?></h2>
                            <?php print $block['content']; ?>
                            <div class="left-right-icon-grid__desktop">
                                <?php BlockMarkup::buttonGroup( $block['buttons'] );?>
                            </div>
                        </div>
                    </div>
                    <div class="left-right-icon-grid__grid-wrap grid__col col-6/12@sm">
	                    <?php foreach( $iconItems as $iconItem ):?>
                            <div class="left-right-icon-grid__grid-item grid-item">
                                <div class="left-right-icon-grid__grid-item-image">
	                                <?php print wp_get_attachment_image($iconItem['icon_image'],'thumbnail', [ 'class' => 'grid-item__icon' ]); ?>
                                </div>

                                <div class="left-right-icon-grid__grid-item-content">
                                    <p class="left-right-icon-grid__grid-item-title"><?php print $iconItem['icon_title']; ?></p>
                                    <p class="left-right-icon-grid__grid-item-body"><?php print $iconItem['icon_content']; ?>
                                    </p>
                                </div>
                            </div>
                        <?php endforeach;?>
                        <div class="left-right-icon-grid__mobile">
                            <?php BlockMarkup::buttonGroup( $block['buttons'] );?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    } );
