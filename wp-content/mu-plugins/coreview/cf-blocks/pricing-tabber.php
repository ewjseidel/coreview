<?php

use Carbon_Fields\Block;
use Carbon_Fields\Field;
use Coreview\BlockFields;
use Coreview\BlockMarkup;

Block::make( __( 'Pricing Tabber', 'coreview' ) )
    ->add_fields( array(
        Field::make( 'text', 'header_title', __( 'Header Title', 'coreview' ) ),
        Field::make( 'rich_text', 'header_content', __( 'Header Content', 'coreview' ) ),
        Field::make( 'complex', 'tabs', __( 'Tabs', 'coreview' ) )
            ->set_layout( 'tabbed-vertical' )
            ->add_fields([
                Field::make( 'text', 'tab_title', __('Tab Title', 'coreview')),
                Field::make( 'complex', 'tab_blocks', __('Blocks', 'coreview'))
                    ->set_layout('tabbed-horizontal')
                    ->add_fields([
                        Field::make('image','block_logo', __('Logo','coreview')),
                        Field::make( 'text', 'block_subline', __( 'Subline', 'coreview' ) ),
                        Field::make( 'text', 'block_title', __( 'Title', 'coreview' ) ),
                        Field::make( 'rich_text', 'block_content', __( 'Content', 'coreview' ) ),
	                    BlockFields::buttonGroup(1, "Bullet CTA")

                    ])
            ])

    ))
    ->set_render_callback( function ( $block ) {
        ?>
        <div class="pricing-tabber">
            <div class="pricing-tabber__header">
                <h2 class="pricing-tabber__header-title"><?php echo esc_html( $block['header_title'] ); ?></h2>
                <div class="pricing-tabber__header-content">
                    <?php echo apply_filters( 'the_content', $block['header_content'] ); ?>
                </div>
                <div class="pricing-tabber__nav-links">
                    <?php foreach ( $block['tabs'] as $i => $tab ) : ?>
                        <a class="pricing-tabber__nav-item <?php print ($i == 0 ? 'active' : '');?>" href="#" data-tab="<?php print $i; ?>"><h4><?php print esc_html($tab['tab_title']); ?></h4></a>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="pricing-tabber__tabs">
                <?php $counter = 1; ?>
                <?php foreach ( $block['tabs'] as $i => $tab ) : ?>
                    <div class="pricing-tabber-tab tab-<?php print $i; ?> <?php print ($i == 0 ? 'active' : '');?>">
                        <?php foreach ( $tab['tab_blocks'] as $j => $tab_block): ?>
                        <div class="pricing-tabber-tab-block block-<?php print $i; ?>-<?php print $j; ?>">
                            <?php if ($counter == 1): ?>
                                <div class="pricing-tabber-tab-block__dotted-circle">
                                    <?php echo file_get_contents( get_template_directory() . "/assets/img/background-icons/Big-Circle.svg");?>
                                </div>
                            <?php endif; ?>
                            <?php if ($counter == 2): ?>
                                <div class="pricing-tabber-tab-block__right-top-square-1">
                                    <?php echo file_get_contents(get_template_directory() . "/assets/img/background-icons/square.svg");?>
                                </div>
                                <div class="pricing-tabber-tab-block__right-top-square-2">
                                    <?php echo file_get_contents( get_template_directory() . "/assets/img/background-icons/square.svg")?>
                                </div>
                            <?php endif; ?>
                            <?php if ($counter == 3): ?>
                                <div class="pricing-tabber-tab-block__right-square">
                                    <?php echo file_get_contents(get_template_directory() . "/assets/img/background-icons/square.svg");?>
                                </div>
                            <?php endif; ?>
                            <?php if ($i == 0 && $j == 0): // First tab first block is "Most Popular", absoluted to top ?>
                            <div class="pricing-tabber-tab-block__popular">Most Popular</div>
                            <?php endif; ?>
                            <figure class="pricing-tabber-tab-block__logo">
                                <img src="<?php echo wp_get_attachment_url($tab_block['block_logo']);?>" alt="">
                            </figure>
                            <h5 class="pricing-tabber-tab-block__subline"><?php print $tab_block['block_subline']; ?></h5>
                            <h4 class="pricing-tabber-tab-block__title"><?php print $tab_block['block_title']; ?></h4>
                            <div class="pricing-tabber-tab-block__content">
                                <?php print apply_filters('the_content', $tab_block['block_content']); ?>
                            </div>
	                        <?php BlockMarkup::buttonGroup( $tab_block['buttons'] );?>
                        </div>
                        <?php 
                            if ($counter == 3) $counter = 0;
                            $counter++; 
                        ?>
                        <?php endforeach; ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <?php
    } );
