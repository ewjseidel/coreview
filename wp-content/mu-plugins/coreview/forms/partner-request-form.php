<?php

namespace Coreview\Forms;

use Coreview\Models\PartnershipRequest;
use Coreview\PostTypes\PartnershipRequests;
use Coreview\Taxonomies\PartnerRegions;
use FlexFields\Forms\Form;
use FlexFields\Forms\FormHandler;
use FlexFields\Make;
use wpscholar\WordPress\Email;

$formName = basename( __FILE__, '.php' );

$formHandler = FormHandler::getInstance();

$form = Make::Form( $formName, [
	'title'   => esc_html__( 'Contact Us About Partnership Opportunities', 'coreview' ),
	'atts'    => [
		'id'     => $formName,
		'method' => 'POST',
	],
	'handler' => function ( Form $form ) {
		if ( validate_flex_form( $form ) ) {
			$formData = $form->fields->asArray();

			// Maps form fields to post fields/meta. Key is the key used to insert via wp_insert_post, value is the name of the field in the form.
			$map = [
				'meta_input.first_name'                 => 'first_name',
				'meta_input.last_name'                  => 'last_name',
				'meta_input.company_name'               => 'company_name',
				'meta_input.phone_number'               => 'phone_number',
				'meta_input.email_address'              => 'email_address',
				'meta_input.partner_type'               => 'partner_type',
				'meta_input.market_focus'               => 'market_focus',
				'meta_input.percent_ms_related'         => 'percent_ms_related',
				'meta_input.partnership_interest'       => 'partnership_interest',
				'tax_input.' . PartnerRegions::TAXONOMY => PartnerRegions::TAXONOMY,
			];

			// Convert raw form data into post data format.
			$data = mapData( $formData, $map );

			// Set title
			$data['post_title'] = "{$data['meta_input']['first_name']} {$data['meta_input']['last_name']}, {$data['meta_input']['company_name']}";

			// Set status and post type.
			$data['post_status'] = 'publish';
			$data['post_type'] = PartnershipRequests::POST_TYPE;

			// Insert request into DB
			$requestId = wp_insert_post( $data );

			if ( $requestId ) {
				$request = new PartnershipRequest( get_post( $requestId ) );
				$regionModel = $request->regionModel();
				if ( $regionModel ) {
					$emailData = [
						'from'    => get_option( 'admin_email' ),
						'to'      => $regionModel->regionEmailAddresses(),
						'subject' => 'New Partnership Request',
						'message' => $request->emailContents(),
					];
					$email = new Email( $emailData );
					$email->send();

				}
			}

			// TODO: Redirect to a thank you page?
			wp_redirect( get_site_url()."/thank-you" );
			exit();
		}
	},
	'fields'  => [

		'first_name'             => [
			'label' => esc_html__( 'First Name', 'coreview' ),
			'rules' => [
				__NAMESPACE__ . '\\isRequired',
			],
		],
		'last_name'              => [
			'label' => esc_html__( 'Last Name', 'coreview' ),
			'rules' => [
				__NAMESPACE__ . '\\isRequired',
			],
		],
		'company_name'           => [
			'label' => esc_html__( 'Organization', 'coreview' ),
			'rules' => [
				__NAMESPACE__ . '\\isRequired',
			],
		],
		'phone_number'           => [
			'label' => esc_html__( 'Phone Number', 'coreview' ),
			'type'  => 'tel',
			'rules' => [
				__NAMESPACE__ . '\\isRequired',
			],
		],
		'email_address'          => [
			'label' => esc_html__( 'Email Address', 'coreview' ),
			'type'  => 'email',
			'rules' => [
				__NAMESPACE__ . '\\isRequired',
				__NAMESPACE__ . '\\isValidEmail',
			],
		],
		'partner_type'           => [
			'label'   => esc_html__( 'Partner Type', 'coreview' ),
			'field'   => 'select',
			'options' => [
				'MSP',
				'System Integrator',
				'Value Added Reseller',
			],
			'rules'   => [
				__NAMESPACE__ . '\\isRequired',
			],
		],
		'market_focus'           => [
			'label'   => esc_html__( 'Market Focus', 'coreview' ),
			'field'   => 'select',
			'options' => [
				'SMB',
				'Mid Market',
				'Enterprise',
			],
		],
		PartnerRegions::TAXONOMY => [
			'label'             => esc_html__( 'Country', 'coreview' ),
			'field'             => 'select',
			'options'           => \call_user_func( function () {
				$options = wpTermArrayToFlexFieldOptions( PartnerRegions::getAllCountryTerms() );
				array_unshift( $options, [
					'label' => '',
					'value' => 0,
				] );

				return $options;
			} ),
			'sanitize_callback' => 'absint',
			'rules'             => [
				__NAMESPACE__ . '\\isRequired',
			],
		],
		'percent_ms_related'     => [
			'label'   => esc_html__( 'Percentage of your business that is Microsoft related?', 'coreview' ),
			'field'   => 'select',
			'options' => [
				'75-100%',
				'50-75%',
				'25-50%',
				'less than 25%',
			],
		],
		'partnership_interest'   => [
			'label' => esc_html__( 'Why are you interested in partnering with CoreView?', 'coreview' ),
			'field' => 'textarea',
		],

		'_submit' => [
			'value'      => 'Request a Partnership',
			'type'       => 'submit',
			'field_atts' => [
				'class' => 'wp-block-button',
			],
			'atts'       => [
				'class' => 'wp-block-button__link has-white-color has-border has-background has-blue-background-color',
			],
		],

	],
] );

$formHandler::registerForm( $form );
