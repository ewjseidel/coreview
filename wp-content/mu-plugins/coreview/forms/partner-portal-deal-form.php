<?php

namespace Coreview\Forms;

use Coreview\Models\Deal;
use Coreview\PostTypes\Deals;
use Coreview\Taxonomies\PartnerRegions;
use FlexFields\Forms\Form;
use FlexFields\Forms\FormHandler;
use FlexFields\Make;
use wpscholar\WordPress\Email;

$formName = basename( __FILE__, '.php' );

$formHandler = FormHandler::getInstance();

$form = Make::Form( $formName, [
	'title'   => esc_html__( 'Register an Opportunity', 'coreview' ),
	'atts'    => [
		'id'     => $formName,
		'method' => 'POST',
	],
	'handler' => function ( Form $form ) {
		if ( validate_flex_form( $form ) ) {
			$formData = $form->fields->asArray();

			// Maps form fields to post fields/meta. Key is the key used to insert via wp_insert_post, value is the name of the field in the form.
			$map = [
				'post_title'                            => '_customer_details.company_name',
				'meta_input.company_name'               => '_customer_details.company_name',
				'meta_input.company_website'            => '_customer_details.company_website',
				'meta_input.number_365_licenses'        => '_customer_details.number_365_licenses',
				'meta_input.us_state'                   => '_customer_details.us_state',
				'meta_input.budget'                     => '_customer_details.budget',
				'meta_input.decision_makers'            => 'decision_makers',
				'meta_input.ms_licensing'               => '_needs_challenges.ms_licensing',
				'meta_input.current_licensing'          => '_needs_challenges.current_licensing',
				'meta_input.challenges'                 => '_needs_challenges.challenges',
				'meta_input.solutions'                  => '_needs_challenges.solutions',
				'meta_input.next_steps'                 => '_needs_challenges.next_steps',
				'tax_input.' . PartnerRegions::TAXONOMY => '_customer_details.' . PartnerRegions::TAXONOMY,
			];

			// Convert raw form data into post data format.
			$data = mapData( $formData, $map );

			// Set status and post type.
			$data['post_status'] = 'publish';
			$data['post_type'] = Deals::POST_TYPE;

			// Insert deal into DB
			$dealId = wp_insert_post( $data );

			if ( $dealId ) {
				$deal = Deal::create( get_post( $dealId ) );
				$emailData = [
					'from'    => get_option( 'admin_email' ),
					'to'      => $deal->managerEmailAddresses(),
					'subject' => 'New Deal Request',
					'message' => $deal->emailContents(),
				];
				$email = new Email( $emailData );
				$email->send();

			} else {
				// TODO: Show some sort of error message!
			}

			wp_redirect( get_site_url()."/partner-portal#my-deals" );
			exit();
		}
	},
	'fields'  => [

		'_customer_details' => [
			'label'  => esc_html__( 'Prospective Customer Details', 'coreview' ),
			'field'  => 'group',
			'fields' => [
				'company_name'           => [
					'label' => esc_html__( 'Company Name', 'coreview' ),
					'rules' => [
						__NAMESPACE__ . '\\isRequired',
					],
				],
				'company_website'        => [
					'label' => esc_html__( 'Company Website', 'coreview' ),
					'type'  => 'url',
					'rules' => [
						__NAMESPACE__ . '\\isRequired',
					],
				],
				'number_365_licenses'    => [
					'label'   => esc_html__( 'Number of Office 365 Licenses', 'coreview' ),
					'field'   => 'select',
					'options' => [
						[
							'label' => '',
							'value' => '',
						],
						'less than 1000',
						'1000-25000',
						'25001-50000',
						'50001-100000',
						'more than 100000',
						'not currently using Office 365',
					],
				],
				PartnerRegions::TAXONOMY => [
					'label'             => esc_html__( 'HQ Country', 'coreview' ),
					'field'             => 'select',
					'options'           => \call_user_func( function () {
						$options = wpTermArrayToFlexFieldOptions( PartnerRegions::getAllCountryTerms() );
						array_unshift( $options, [
							'label' => '',
							'value' => 0,
						] );

						return $options;
					} ),
					'sanitize_callback' => 'absint',
					'rules'             => [
						__NAMESPACE__ . '\\isRequired',
					],
				],
				'us_state'               => [
					'label'      => esc_html__( 'State', 'coreview' ),
					'field'      => 'select',
					'field_atts' => [
						'hidden' => 'hidden',
					],
					'options'    => [
						''   => '',
						'AL' => 'Alabama',
						'AK' => 'Alaska',
						'AZ' => 'Arizona',
						'AR' => 'Arkansas',
						'CA' => 'California',
						'CO' => 'Colorado',
						'CT' => 'Connecticut',
						'DE' => 'Delaware',
						'DC' => 'District Of Columbia',
						'FL' => 'Florida',
						'GA' => 'Georgia',
						'HI' => 'Hawaii',
						'ID' => 'Idaho',
						'IL' => 'Illinois',
						'IN' => 'Indiana',
						'IA' => 'Iowa',
						'KS' => 'Kansas',
						'KY' => 'Kentucky',
						'LA' => 'Louisiana',
						'ME' => 'Maine',
						'MD' => 'Maryland',
						'MA' => 'Massachusetts',
						'MI' => 'Michigan',
						'MN' => 'Minnesota',
						'MS' => 'Mississippi',
						'MO' => 'Missouri',
						'MT' => 'Montana',
						'NE' => 'Nebraska',
						'NV' => 'Nevada',
						'NH' => 'New Hampshire',
						'NJ' => 'New Jersey',
						'NM' => 'New Mexico',
						'NY' => 'New York',
						'NC' => 'North Carolina',
						'ND' => 'North Dakota',
						'OH' => 'Ohio',
						'OK' => 'Oklahoma',
						'OR' => 'Oregon',
						'PA' => 'Pennsylvania',
						'RI' => 'Rhode Island',
						'SC' => 'South Carolina',
						'SD' => 'South Dakota',
						'TN' => 'Tennessee',
						'TX' => 'Texas',
						'UT' => 'Utah',
						'VT' => 'Vermont',
						'VA' => 'Virginia',
						'WA' => 'Washington',
						'WV' => 'West Virginia',
						'WI' => 'Wisconsin',
						'WY' => 'Wyoming',
					],
					'rules'      => [
						function ($field, Form $form) {
							$regionField = $form->fields->getField('_customer_details')->fields->getField(PartnerRegions::TAXONOMY);
							if ($regionField && (int) $regionField->value === PartnerRegions::usaId()){
								$state = $field->value;
								if (empty($state)){
									$field->addError('State is Required.');
								}
							}
						}
					]
				],
				'budget'                 => [
					'label'   => esc_html__( 'Budget Availability', 'coreview' ),
					'field'   => 'select',
					'options' => [
						[
							'label' => '',
							'value' => '',
						],
						'Yes',
						'No',
						'Unsure',
					],
				],
			],
		],

		'decision_makers' => [
			'label'           => esc_html__( 'Key Decision Makers', 'coreview' ),
			'field'           => 'repeating',
			'repeating_field' => [
				'field'  => 'group',
				'fields' => [
					'first_name'    => [
						'label' => esc_html__( 'First Name', 'coreview' ),
						'rules' => [
							__NAMESPACE__ . '\\isRequired',
						],
					],
					'last_name'     => [
						'label' => esc_html__( 'Last Name', 'coreview' ),
						'rules' => [
							__NAMESPACE__ . '\\isRequired',
						],
					],
					'job_title'     => [
						'label' => esc_html__( 'Job Title', 'coreview' ),
					],
					'phone_number'  => [
						'label' => esc_html__( 'Phone Number', 'coreview' ),
						'type'  => 'tel',
					],
					'email_address' => [
						'label' => esc_html__( 'Email Address', 'coreview' ),
						'type'  => 'email',
						'rules' => [
							__NAMESPACE__ . '\\isRequired',
						],
					],
				],
				'rules'  => [
					// TODO: Make sure there is always at least 1 decision maker.
					// TODO: Ensure all fields in this group have proper required checks.
				],
			],
		],

		'_needs_challenges' => [
			'label'  => esc_html__( 'Current Microsoft Needs & Challenges', 'coreview' ),
			'field'  => 'group',
			'fields' => [
				'ms_licensing'      => [
					'label' => esc_html__( 'Microsoft Environment/Licensing', 'coreview' ),
				],
				'current_licensing' => [
					'label' => esc_html__( 'Current Licensing or Security Solutions', 'coreview' ),
				],
				'challenges'        => [
					'label' => esc_html__( 'Challenges you believe CoreView can help address', 'coreview' ),
					'field' => 'textarea',
					'rules' => [
						__NAMESPACE__ . '\\isRequired',
					],
				],
				'solutions'         => [
					'label' => esc_html__( 'What solutions do they require to meet their challenges', 'coreview' ),
					'field' => 'textarea',
					'rules' => [
						__NAMESPACE__ . '\\isRequired',
					],
				],
				'next_steps'        => [
					'label' => esc_html__( 'Next steps & timeframe for a decision', 'coreview' ),
					'field' => 'textarea',
					'rules' => [
						__NAMESPACE__ . '\\isRequired',
					],
				],
			],
		],

		'_submit' => [
			'value'      => 'Register Deal',
			'type'       => 'submit',
			'field_atts' => [
				'class' => 'wp-block-button',
			],
			'atts'       => [
				'class' => 'wp-block-button__link has-white-color has-border has-background has-blue-background-color',
			],
		],

	],
] );

$formHandler::registerForm( $form );
