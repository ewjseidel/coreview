<?php

namespace Coreview\PostTypes;

use Coreview\Models\Deal;
use FlexFields\Make;

/**
 * Class Deals
 * @package Coreview\PostTypes
 */
class Deals {

	/**
	 * @const string POST_TYPE Defines the internal post type name.
	 */
	const POST_TYPE = 'cv-deal';

	/**
	 * Add any applicable action hooks and filters.
	 */
	public static function initialize(): void {
		add_action( 'init', [ __CLASS__, 'registerPostType' ] );
		add_action( 'init', [ __CLASS__, 'restApiFields' ] );
		add_action( 'init', [ __CLASS__, 'addMetaBoxes' ] );
	}

	/**
	 * Registers the post type.
	 */
	public static function registerPostType(): void {
		$args = [
			'labels'       => [
				'name'               => esc_html_x( 'Deals', 'post type general name', 'coreview' ),
				'singular_name'      => esc_html_x( 'Deal', 'post type singular name', 'coreview' ),
				'menu_name'          => esc_html_x( 'Deals', 'admin menu', 'coreview' ),
				'name_admin_bar'     => esc_html_x( 'Deal', 'add new on admin bar', 'coreview' ),
				'add_new_item'       => esc_html__( 'Add New Deal', 'coreview' ),
				'new_item'           => esc_html__( 'New Deal', 'coreview' ),
				'edit_item'          => esc_html__( 'Edit Deal', 'coreview' ),
				'view_item'          => esc_html__( 'View Deal', 'coreview' ),
				'all_items'          => esc_html__( 'All Deals', 'coreview' ),
				'search_items'       => esc_html__( 'Search Deals', 'coreview' ),
				'archives'           => esc_html__( 'Deal Archives', 'coreview' ),
				'not_found'          => esc_html__( 'No Deals found.', 'coreview' ),
				'not_found_in_trash' => esc_html__( 'No Deals found in trash.', 'coreview' ),
			],
			'public'       => false,
			'has_archive'  => false,
			'show_ui'      => true,
			'menu_icon'    => 'dashicons-thumbs-up',
			'supports'     => [
				'title',
			],
			'rewrite'      => false,
			'show_in_rest' => true,
			'rest_base'    => 'deals',
		];

		$args = apply_filters( 'post_type_args_' . self::POST_TYPE, $args );

		register_post_type( self::POST_TYPE, $args );
	}

	public static function restApiFields() {
		$exposeInRest = [
			'company_name',
			'number_365_licenses',
			'us_state',
			'budget',
			// 'decision_makers', // Handled separately; data is stored as an array.
			'ms_licensing',
			'current_licensing',
			'challenges',
			'solutions',
			'next_steps',
		];

		foreach ( $exposeInRest as $field ) {
			register_rest_field( self::POST_TYPE, $field, [
				'get_callback' => function ( array $post ) use ( $field ) {
					return get_post_meta( $post['id'], $field, true );
				}
			] );
		}

		register_rest_field( self::POST_TYPE, 'location', [
			'get_callback' => function ( array $post ) {
				$deal = Deal::create( get_post( $post['id'] ) );

				return $deal->location();
			}
		] );

		register_rest_field( self::POST_TYPE, 'dateRequested', [
			'get_callback' => function ( array $post ) {
				$deal = Deal::create( get_post( $post['id'] ) );

				return $deal->dateRequested();
			}
		] );

		register_rest_field( self::POST_TYPE, 'decision_makers', [
			'get_callback' => function ( array $post ) {
				$data = get_post_meta( $post['id'], 'decision_makers', true );

				return wp_json_encode( $data );
			},
		] );
	}

	public static function addMetaBoxes() {
		$metaBox = Make::MetaBox( 'customer-details', 'Prospective Customer Details', self::POST_TYPE );
		$metaBox->fields = Make::FieldContainer( [
			'company_name'        => [
				'label' => 'Company Name',
				'atts'  => [
					'readOnly' => 1,
				],
			],
			'number_365_licenses' => [
				'label' => esc_html__( 'Number of Office 365 Licenses', 'coreview' ),
				'atts'  => [
					'readOnly' => 1,
				],
			],
			'budget'              => [
				'label' => esc_html__( 'Budget Availability', 'coreview' ),
				'atts'  => [
					'readOnly' => 1,
				],
			],
		] );

		$metaBox = Make::MetaBox( 'decision-makers', 'Key Decision Makers', self::POST_TYPE );
		$metaBox->fields = Make::FieldContainer( [
			'decision_makers' => [
				'field' => 'html',
				'value' => function () {
					$decisionMakers = get_post_meta( get_the_ID(), 'decision_makers', true );
					foreach ( $decisionMakers as $decisionMaker ) {
						?>
                        <p>
                            <b><?php echo esc_html( $decisionMaker['first_name'] . ' ' . $decisionMaker['last_name'] ); ?></b>
                            <br />
							<?php echo esc_html( $decisionMaker['job_title'] ); ?>
                            <br />
							<?php echo esc_html( $decisionMaker['phone_number'] ); ?>
                            <br />
							<?php echo esc_html( $decisionMaker['email_address'] ); ?>
                        </p>
						<?php
					}
				},
			],
		] );

		$metaBox = Make::MetaBox( 'needs-challenges', 'Current Microsoft Needs & Challenges', self::POST_TYPE );
		$metaBox->fields = Make::FieldContainer( [
			'ms_licensing'      => [
				'label' => esc_html__( 'Microsoft Environment/Licensing', 'coreview' ),
				'atts'  => [
					'readOnly' => 1,
				],
			],
			'current_licensing' => [
				'label' => esc_html__( 'Current Licensing or Security Solutions', 'coreview' ),
				'atts'  => [
					'readOnly' => 1,
				],
			],
			'challenges'        => [
				'label' => esc_html__( 'Challenges you believe CoreView can help address', 'coreview' ),
				'field' => 'textarea',
				'atts'  => [
					'readOnly' => 1,
				],
			],
			'solutions'         => [
				'label' => esc_html__( 'What solutions do they require to meet their challenges', 'coreview' ),
				'field' => 'textarea',
				'atts'  => [
					'readOnly' => 1,
				],
			],
			'next_steps'        => [
				'label' => esc_html__( 'Next steps & timeframe for a decision', 'coreview' ),
				'field' => 'textarea',
				'atts'  => [
					'readOnly' => 1,
				],
			],
		] );
	}

}