<?php

namespace Coreview\PostTypes;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

class TeamItems {

	/**
	 * @const string POST_TYPE Defines the internal post type name.
	 */
	const POST_TYPE = 'cv-team-item';

	/**
	 * Add any applicable action hooks and filters.
	 */
	public static function initialize() {
		add_action( 'init', [ __CLASS__, 'registerPostType' ] );
        add_action( 'carbon_fields_register_fields', [ __CLASS__, 'registerFields' ] );
	}

	/**
	 * Registers the post type.
	 */
	public static function registerPostType() {
		$args = [
			'labels'       => [
				'name'               => esc_html_x( 'Team Items', 'post type general name', 'coreview' ),
				'singular_name'      => esc_html_x( 'Team Item', 'post type singular name', 'coreview' ),
				'menu_name'          => esc_html_x( 'Team', 'admin menu', 'coreview' ),
				'name_admin_bar'     => esc_html_x( 'Team', 'add new on admin bar', 'coreview' ),
				'add_new_item'       => esc_html__( 'Add New Team Item', 'coreview' ),
				'new_item'           => esc_html__( 'New Team Item', 'coreview' ),
				'edit_item'          => esc_html__( 'Edit Team Item', 'coreview' ),
				'view_item'          => esc_html__( 'View Team Item', 'coreview' ),
				'all_items'          => esc_html__( 'All Team Items', 'coreview' ),
				'search_items'       => esc_html__( 'Search Team Items', 'coreview' ),
				'archives'           => esc_html__( 'Team Archives', 'coreview' ),
				'not_found'          => esc_html__( 'No Team Items found.', 'coreview' ),
				'not_found_in_trash' => esc_html__( 'No Team Items found in trash.', 'coreview' ),

			],
			'public'       => true,
			'has_archive'  => false,
			'show_ui'      => true,
			'menu_icon'    => 'dashicons-groups',
			'supports'     => [
				'title',
				'editor',
				'revisions',
				'excerpt',
				'thumbnail'
			],
			'rewrite' => [
				'with_front' => false,
				'slug' => 'team',
			],
			'show_in_rest' => true,
			'rest_base'    => 'team',
		];

		$args = apply_filters( 'post_type_args_' . self::POST_TYPE, $args );

		register_post_type( self::POST_TYPE, $args );
	}


    /**
     * Registers the Carbon Fields definitions.
     */
    public static function registerFields() {

        Container::make('post_meta','Team Member Info')
            ->where('post_type', '=', self::POST_TYPE)
            ->set_context('normal')
            ->set_priority('default')
            ->add_fields([
                Field::make('text','team_name','Name'),
                Field::make('image','team_photo','Photo'),
                Field::make('text','team_position','Position'),
                Field::make('text','team_linkedin','LinkedIn URL'),
            ]);
    }
}
