<?php

namespace Coreview\PostTypes;

use Carbon_Fields\Container;
use Carbon_Fields\Field;
use Coreview\Models\NewsItem;

class NewsItems {

	/**
	 * @const string POST_TYPE Defines the internal post type name.
	 */
	const POST_TYPE = 'cv-news-item';

	/**
	 * Add any applicable action hooks and filters.
	 */
	public static function initialize() {
		add_action( 'init', [ __CLASS__, 'registerPostType' ] );
		add_action( 'carbon_fields_register_fields', [ __CLASS__, 'registerFields' ] );
	}

	/**
	 * Registers the post type.
	 */
	public static function registerPostType() {
		$args = [
			'labels'       => [
				'name'               => esc_html_x( 'News Items', 'post type general name', 'coreview' ),
				'singular_name'      => esc_html_x( 'News Item', 'post type singular name', 'coreview' ),
				'menu_name'          => esc_html_x( 'Company News', 'admin menu', 'coreview' ),
				'name_admin_bar'     => esc_html_x( 'News', 'add new on admin bar', 'coreview' ),
				'add_new_item'       => esc_html__( 'Add News Item', 'coreview' ),
				'new_item'           => esc_html__( 'New News Item', 'coreview' ),
				'edit_item'          => esc_html__( 'Edit News Item', 'coreview' ),
				'view_item'          => esc_html__( 'View News Item', 'coreview' ),
				'all_items'          => esc_html__( 'All News Items', 'coreview' ),
				'search_items'       => esc_html__( 'Search News Item', 'coreview' ),
				'archives'           => esc_html__( 'News Archives', 'coreview' ),
				'not_found'          => esc_html__( 'No News Item found.', 'coreview' ),
				'not_found_in_trash' => esc_html__( 'No News Item found in trash.', 'coreview' ),

			],
			'public'       => true,
			'has_archive'  => false,
			'show_ui'      => true,
			'menu_icon'    => 'dashicons-rss',
			'supports'     => [
				'title',
				'editor',
				'revisions',
				'excerpt',
				'thumbnail'
			],
			'rewrite'      => [
				'with_front' => false,
				'slug'       => 'news-event',
			],
			'show_in_rest' => true,
			'rest_base'    => 'news-post',
		];

		$args = apply_filters( 'post_type_args_' . self::POST_TYPE, $args );

		register_post_type( self::POST_TYPE, $args );
	}


	/**
	 * Registers the Carbon Fields definitions.
	 */
	public static function registerFields() {

		Container::make( 'post_meta', 'Additional Info' )
		         ->where( 'post_type', '=', self::POST_TYPE )
		         ->set_context( 'normal' )
		         ->set_priority( 'default' )
		         ->add_fields( [
			         Field::make( 'complex', 'contact_field', __( 'Contacts', 'coreview' ) )
			            ->set_layout( 'tabbed-horizontal' )
			            ->add_fields( [
			            	Field::make( 'text', 'contact_group', __( 'Contact Group', 'coreview' ) ),
				            Field::make( 'rich_text', 'contact_info', __( 'Contact Info', 'coreview' ) )

			            ] ),
		         ] );

	}


}
