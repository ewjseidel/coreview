<?php

namespace Coreview\PostTypes;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

class Events {

	/**
	 * @const string POST_TYPE Defines the internal post type name.
	 */
	const POST_TYPE = 'cv-event';

	/**
	 * Add any applicable action hooks and filters.
	 */
	public static function initialize() {
		add_action( 'init', [ __CLASS__, 'registerPostType' ] );
        add_action( 'carbon_fields_register_fields', [ __CLASS__, 'registerFields' ] );
	}

	/**
	 * Registers the post type.
	 */
	public static function registerPostType() {
		$args = [
			'labels'       => [
				'name'               => esc_html_x( 'Events', 'post type general name', 'coreview' ),
				'singular_name'      => esc_html_x( 'Event', 'post type singular name', 'coreview' ),
				'menu_name'          => esc_html_x( 'Events', 'admin menu', 'coreview' ),
				'name_admin_bar'     => esc_html_x( 'Event', 'add new on admin bar', 'coreview' ),
				'add_new_item'       => esc_html__( 'Add New Event', 'coreview' ),
				'new_item'           => esc_html__( 'New Event', 'coreview' ),
				'edit_item'          => esc_html__( 'Edit Event', 'coreview' ),
				'view_item'          => esc_html__( 'View Event', 'coreview' ),
				'all_items'          => esc_html__( 'All Events', 'coreview' ),
				'search_items'       => esc_html__( 'Search Events', 'coreview' ),
				'archives'           => esc_html__( 'Event Archives', 'coreview' ),
				'not_found'          => esc_html__( 'No Events found.', 'coreview' ),
				'not_found_in_trash' => esc_html__( 'No Events found in trash.', 'coreview' ),

			],
			'public'       => true,
			'has_archive'  => false,
			'show_ui'      => true,
			'menu_icon'    => 'dashicons-calendar-alt',
			'supports'     => [
				'title',
				'editor',
				'revisions',
				'excerpt',
				'thumbnail'
			],
			'rewrite' => [
				'with_front' => false,
				'slug' => 'news-events',
			],
			'show_in_rest' => true,
			'rest_base'    => 'events',
		];

		$args = apply_filters( 'post_type_args_' . self::POST_TYPE, $args );

		register_post_type( self::POST_TYPE, $args );
	}


    /**
     * Registers the Carbon Fields definitions.
     */
    public static function registerFields() {

        Container::make('post_meta','Additional Info')
            ->where('post_type', '=', self::POST_TYPE)
            ->set_context('normal')
            ->set_priority('default')
            ->add_fields([
                Field::make('date_time','start_date', __( 'Start Date', 'coreview'))
			        ->set_storage_format('U'),
		        Field::make('date_time','end_date', __( 'End Date', 'coreview' ) )
			        ->set_storage_format('U'),
				Field::make('rich_text','venue', __( 'Venue', 'coreview' ) )

            ]);
    }
}
