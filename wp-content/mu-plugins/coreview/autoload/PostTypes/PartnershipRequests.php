<?php

namespace Coreview\PostTypes;

use Carbon_Fields\Container;
use Carbon_Fields\Field;
use Coreview\Taxonomies\PartnerRegions;

/**
 * Class PartnershipRequests
 * @package Coreview\PostTypes
 */
class PartnershipRequests {

	/**
	 * @const string POST_TYPE Defines the internal post type name.
	 */
	const POST_TYPE = 'cv-partnership-req';

	/**
	 * Add any applicable action hooks and filters.
	 */
	public static function initialize(): void {
		add_action( 'init', [ __CLASS__, 'registerPostType' ] );
		add_action( 'carbon_fields_register_fields', [ __CLASS__, 'addUserMeta' ] );
	}

	/**
	 * Registers the post type.
	 */
	public static function registerPostType(): void {
		$args = [
			'labels'       => [
				'name'               => esc_html_x( 'Partnership Requests', 'post type general name', 'coreview' ),
				'singular_name'      => esc_html_x( 'Partnership Request', 'post type singular name', 'coreview' ),
				'menu_name'          => esc_html_x( 'Partnership Requests', 'admin menu', 'coreview' ),
				'name_admin_bar'     => esc_html_x( 'Partnership Request', 'add new on admin bar', 'coreview' ),
				'add_new_item'       => esc_html__( 'Add New Partnership Request', 'coreview' ),
				'new_item'           => esc_html__( 'New Partnership Request', 'coreview' ),
				'edit_item'          => esc_html__( 'Edit Partnership Request', 'coreview' ),
				'view_item'          => esc_html__( 'View Partnership Request', 'coreview' ),
				'all_items'          => esc_html__( 'All Partnership Requests', 'coreview' ),
				'search_items'       => esc_html__( 'Search Partnership Requests', 'coreview' ),
				'archives'           => esc_html__( 'Partnership Request Archives', 'coreview' ),
				'not_found'          => esc_html__( 'No Partnership Requests found.', 'coreview' ),
				'not_found_in_trash' => esc_html__( 'No Partnership Requests found in trash.', 'coreview' ),
			],
			'public'       => false,
			'has_archive'  => false,
			'show_ui'      => false,
			'menu_icon'    => 'dashicons-id',
			'supports'     => [
				'title',
			],
			'rewrite'      => false,
			'show_in_rest' => false,
		];

		$args = apply_filters( 'post_type_args_' . self::POST_TYPE, $args );

		register_post_type( self::POST_TYPE, $args );
	}

	public static function addUserMeta(): void {
		Container::make( 'user_meta', 'Partner Information' )
		         ->add_fields( [
			         Field::make( 'select', 'cv_partner_region', esc_html__( 'Assigned Region', 'coreview' ) )
			              ->set_options( PartnerRegions::getUserRegionOptions() ),
		         ] );
	}

}