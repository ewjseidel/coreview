<?php

namespace Coreview\PostTypes;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

class Posts {

    /**
     * @const string POST_TYPE Defines the internal post type name.
     */
    const POST_TYPE = 'post';

    /**
     * Add any applicable action hooks and filters.
     */
    public static function initialize() {
        add_action( 'carbon_fields_register_fields', [ __CLASS__, 'registerFields' ] );
	    add_filter( 'wp_editor_settings', [ __CLASS__, 'carbonEditorOptions' ], 10, 2 );

    }

    /**
     * Registers the Carbon Fields definitions.
     */
    public static function registerFields() {

	    Container::make('post_meta','Additional Info')
	             ->where('post_type', '=', self::POST_TYPE)
	             ->set_context('normal')
	             ->set_priority('default')
	             ->add_fields([
		             Field::make('text','li_url', __( 'Author LinkedIn URL', 'coreview'))

	             ]);

    }

	public static function carbonEditorOptions($settings, $editor_id) {
		if($editor_id === 'carbon_settings') {
			$settings = [
				'media_buttons' => false,
				'tinymce'       => [
					'content_css' => '',
					'menubar' => false,
					'toolbar' => 'undo redo | styleselect | bold italic | link | alignleft aligncenter alignright | bullist numlist',
					'statusbar' => false,
					'cache_suffix' => '',
					'plugins' => 'lists link',
				],
				'quicktags' => false
			];
		}

		return $settings;
	}

}
