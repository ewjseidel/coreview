<?php

namespace Coreview\PostTypes;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

class Resources {

	/**
	 * @const string POST_TYPE Defines the internal post type name.
	 */
	const POST_TYPE = 'cv-resource';

	/**
	 * Add any applicable action hooks and filters.
	 */
	public static function initialize() {
		add_action( 'init', [ __CLASS__, 'registerPostType' ] );
        add_action( 'carbon_fields_register_fields', [ __CLASS__, 'registerFields' ] );
	}

	/**
	 * Registers the post type.
	 */
	public static function registerPostType() {
		$args = [
			'labels'       => [
				'name'               => esc_html_x( 'Resources', 'post type general name', 'coreview' ),
				'singular_name'      => esc_html_x( 'Resource', 'post type singular name', 'coreview' ),
				'menu_name'          => esc_html_x( 'Resources', 'admin menu', 'coreview' ),
				'name_admin_bar'     => esc_html_x( 'Resource', 'add new on admin bar', 'coreview' ),
				'add_new_item'       => esc_html__( 'Add New Resource', 'coreview' ),
				'new_item'           => esc_html__( 'New Resource', 'coreview' ),
				'edit_item'          => esc_html__( 'Edit Resource', 'coreview' ),
				'view_item'          => esc_html__( 'View Resource', 'coreview' ),
				'all_items'          => esc_html__( 'All Resources', 'coreview' ),
				'search_items'       => esc_html__( 'Search Resources', 'coreview' ),
				'archives'           => esc_html__( 'Resource Archives', 'coreview' ),
				'not_found'          => esc_html__( 'No Resources found.', 'coreview' ),
				'not_found_in_trash' => esc_html__( 'No Resources found in trash.', 'coreview' ),

			],
			'public'       => true,
			'has_archive'  => false,
			'show_ui'      => true,
			'menu_icon'    => 'dashicons-book',
			'supports'     => [
				'title',
				'editor',
				'revisions',
				'excerpt',
				'thumbnail',
				'author'
			],
			'rewrite' => [
				'with_front' => false,
				'slug' => 'resources',
			],
			'show_in_rest' => true,
			'rest_base'    => 'resources',
		];

		$args = apply_filters( 'post_type_args_' . self::POST_TYPE, $args );

		register_post_type( self::POST_TYPE, $args );
	}


    /**
     * Registers the Carbon Fields definitions.
     */
    public static function registerFields() {

        Container::make('post_meta','Additional Info')
            ->where('post_type', '=', self::POST_TYPE)
            ->set_context('normal')
            ->set_priority('default')
            ->add_fields([
	            Field::make('checkbox','downloadable', __( 'Downloadable Resource?' ) ),
	            Field::make('file','file_download', __( 'Resource File' ) )
	                 ->set_conditional_logic([ [ 'field' => 'downloadable', 'value' => true ] ]),
                Field::make( 'checkbox' , 'gated' , __( 'Gate with Marketo Progressive Form?', 'coreview')),
	            Field::make( 'text', 'marketo_form_id', __( 'Marketo Form ID', 'coreview' ) )
	                ->set_conditional_logic( [ ['field' => 'gated', 'value' => true] ]),
	            Field::make( 'text', 'marketo_form_title', __( 'Marketo Form Title', 'coreview' ) )
	                 ->set_conditional_logic( [ ['field' => 'gated', 'value' => true] ]),
	            Field::make( 'textarea', 'marketo_form_description', __( 'Marketo Form Description', 'coreview' ) )
	                 ->set_conditional_logic( [ ['field' => 'gated', 'value' => true] ]),
            ]);
    }

}
