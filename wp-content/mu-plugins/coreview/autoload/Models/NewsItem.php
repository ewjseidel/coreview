<?php

namespace Coreview\Models;

use Coreview\PostTypes\NewsItems;
use Coreview\Taxonomies\GeneralTags;
use Coreview\Taxonomies\NewsTypes;
use Coreview\Traits\FeaturedImage;
use Coreview\Traits\GeneralType;
use Narwhal\WordPress\Traits\PostAuthor;
use Narwhal\WordPress\Traits\PostDate;
use wpscholar\WordPress\PostModelBase;
use wpscholar\WordPress\Traits\PostContent;
use wpscholar\WordPress\Traits\PostExcerpt;
use wpscholar\WordPress\Traits\PostId;
use wpscholar\WordPress\Traits\PostPermalink;
use wpscholar\WordPress\Traits\PostThumbnail;
use wpscholar\WordPress\Traits\PostTitle;

/**
 * Class Resource
 * @package Coreview\Models
 */
class NewsItem extends PostModelBase {

	use PostAuthor,
		PostContent,
		PostDate,
		PostExcerpt,
		PostId,
		PostTitle,
		PostPermalink,
		PostThumbnail,
		GeneralType,
		FeaturedImage {
		PostContent::postContent as content;
		PostExcerpt::postExcerpt as excerpt;
		PostId::postId as id;
		PostTitle::postTitle as title;
	}

	const POST_TYPE = NewsItems::POST_TYPE;

	private $typeTaxonomy = NewsTypes::TAXONOMY;

	public $tagTaxonomy = GeneralTags::TAXONOMY;


	/**
	 * Check if post has content.
	 *
	 * @return bool
	 */
	public function hasContent() {
		return ! empty( $this->content() );
	}

	public function trimmedExcerpt( $length = 55, $more = '&hellip;' ) {
		return wp_trim_words( $this->excerpt(), $length, $more );
	}

	public function url() {
		return $this->postPermalink();
	}

	public function getContactInfo() {
		return carbon_get_post_meta( $this->id(), 'contact_field');
	}

	public function getNextPost() {
		return get_next_post();
	}

	public function getPreviousPost() {
		return get_previous_post();
	}

	public function getTags() {
		$terms = wp_get_post_terms( $this->id(), "general-tags");

		return $terms;
	}

	public function getMainNewsTypeName() {
		$terms = wp_get_post_terms( $this->id(), "cv-news-type");
		if (!empty($terms)){
			$primaryTerm = $terms[0];
			$primaryTermName = $primaryTerm->name;
			return $primaryTermName;
		} else {
			return 'News and Events';
		}
	}

}
