<?php

namespace Coreview\Models;

use Coreview\PostTypes\Resources;
use Coreview\Taxonomies\ResourceTypes;
use Coreview\Traits\FeaturedImage;
use Coreview\Traits\GeneralType;
use Narwhal\WordPress\Traits\PostAuthor;
use Narwhal\WordPress\Traits\PostDate;
use wpscholar\WordPress\PostModelBase;
use wpscholar\WordPress\Traits\PostContent;
use wpscholar\WordPress\Traits\PostExcerpt;
use wpscholar\WordPress\Traits\PostId;
use wpscholar\WordPress\Traits\PostPermalink;
use wpscholar\WordPress\Traits\PostThumbnail;
use wpscholar\WordPress\Traits\PostTitle;

/**
 * Class Resource
 * @package Coreview\Models
 */
class Resource extends PostModelBase {

	use PostAuthor,
		PostContent,
		PostDate,
		PostExcerpt,
		PostId,
		PostTitle,
		PostPermalink,
		PostThumbnail,
		GeneralType,
		FeaturedImage {
		PostContent::postContent as content;
		PostExcerpt::postExcerpt as excerpt;
		PostId::postId as id;
		PostTitle::postTitle as title;
	}

	const POST_TYPE = Resources::POST_TYPE;

	private $typeTaxonomy = ResourceTypes::TAXONOMY;


	/**
	 * Check if post has content.
	 *
	 * @return bool
	 */
	public function hasContent() {
		return ! empty( $this->content() );
	}

	public function trimmedExcerpt( $length = 55, $more = '&hellip;' ) {
		return wp_trim_words( $this->excerpt(), $length, $more );
	}

	public function url() {
		return $this->postPermalink();
	}

	/**
	 * @deprecated Use `url()` instead.
	 */
	public function link() {
		return $this->url();
	}

	public function isGated() {

		return carbon_get_post_meta( $this->id(), 'gated' );
	}

	public function formID() {
		return carbon_get_post_meta( $this->id(), "marketo_form_id");
	}

	public function formTitle() {
		return carbon_get_post_meta( $this->id(), "marketo_form_title");
	}

	public function formDescription() {
		return carbon_get_post_meta( $this->id(), "marketo_form_description");
	}

	public function isDownload() {
		return carbon_get_post_meta( $this->id(), "downloadable");
	}

	public function downloadFile() {
		$fileID = carbon_get_post_meta( $this->id(), "file_download");
		 $asset = wp_get_attachment_url( $fileID );

		 return $asset;

	}

	public function redirectURL() {
		return 'resource-ungated';
	}

	public function getForm() {
		$formID = $this->formID();

		return do_shortcode('[marketo_form id="' . $formID . '" redirect-url="' . $this->redirectURL() . '" /]');
	}

	public function getNextPost() {
		return get_next_post(true, "", "cv-resource-type");
	}

	public function getPreviousPost() {
		return get_previous_post(true, "", "cv-resource-type");
	}

	public function getTags() {
		$terms = wp_get_post_terms( $this->id(), "general-tags");

		return $terms;
	}

	public function getMainResource() {
		$terms = wp_get_post_terms( $this->id(), "cv-resource-type");
		$primaryTerm = $terms[0];

		return $primaryTerm;
	}

	public function getMainResourceTypeName() {
		$terms = wp_get_post_terms( $this->id(), "cv-resource-type");
		$primaryTerm = $terms[0];
		$primaryTermName = $primaryTerm->name;

		return $primaryTermName;
	}



}
