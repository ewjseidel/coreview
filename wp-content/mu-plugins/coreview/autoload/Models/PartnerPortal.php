<?php

namespace Coreview\Models;

use Narwhal\WordPress\Traits\PostAuthor;
use Narwhal\WordPress\Traits\PostDate;
use Coreview\Traits\FeaturedImage;
use wpscholar\WordPress\PostModelBase;
use wpscholar\WordPress\Traits\PostContent;
use wpscholar\WordPress\Traits\PostExcerpt;
use wpscholar\WordPress\Traits\PostId;
use wpscholar\WordPress\Traits\PostPermalink;
use wpscholar\WordPress\Traits\PostThumbnail;
use wpscholar\WordPress\Traits\PostTitle;
use Coreview\Traits\PostCategory;
use Coreview\PartnerPortal;

/**
 * Class Post
 * @package Coreview\Models
 */
class PartnerPortalModel extends PostModelBase {

	use PostAuthor,
		PostContent,
		PostDate,
		PostExcerpt,
		PostId,
		PostTitle,
		PostPermalink,
		PostCategory,
		PostThumbnail,
		FeaturedImage {
		PostPermalink::postPermalink as url;
		PostContent::postContent as content;
		PostExcerpt::postExcerpt as excerpt;
		PostId::postId as id;
		PostTitle::postTitle as title;
		PostAuthor::postAuthorName as authorName;
	}


	/**
	 * Check if post has content.
	 *
	 * @return bool
	 */
	public function hasContent() {
		return ! empty( $this->content() );
	}

	public function trimmedExcerpt( $length = 55, $more = '&hellip;' ) {
		return wp_trim_words( $this->excerpt(), $length, $more );
	}

	public function getAuthorFullName() {
		$firstName = get_the_author_meta( 'first_name' );
		$lastName = get_the_author_meta( 'last_name' );
		$fullName = $firstName." ".$lastName;

		if ( ! empty( $firstName )) {
			return $fullName;
		} else {
			return $this->authorName();
		}
	}

	public function getAuthorLinkedIn() {
		return carbon_get_post_meta( $this->id(), 'li_url');
	}

	public function getNextPost() {
		return get_next_post();
	}

	public function getPreviousPost() {
		return get_previous_post();
	}

	public function getTags() {
		$terms = wp_get_post_terms( $this->id(), "general-tags");

		return $terms;
	}

	public function getIpsum($var) {

		return $var;
	}


}
