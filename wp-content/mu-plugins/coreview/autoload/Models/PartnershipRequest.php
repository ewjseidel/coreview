<?php

namespace Coreview\Models;

use Coreview\PostTypes\PartnershipRequests;
use Coreview\Taxonomies\PartnerRegions;
use wpscholar\WordPress\PostModelBase;
use wpscholar\WordPress\Traits\PostId;
use wpscholar\WordPress\Traits\PostTitle;

/**
 * Class PartnershipRequest
 * @package Coreview\Models
 */
class PartnershipRequest extends PostModelBase {

	const POST_TYPE = PartnershipRequests::POST_TYPE;

	use PostId,
		PostTitle;

	public function dateRequested() {
		return get_the_date( 'd M Y', $this->post );
	}

	public function company() {
		return $this->post->company_name;
	}

	public function firstName() {
		return $this->post->first_name;
	}

	public function lastName() {
		return $this->post->last_name;
	}

	public function name() {
		return trim( "{$this->firstName()} {$this->lastName()}" );
	}

	public function phoneNumber() {
		return $this->post->phone_number;
	}

	public function emailAddress() {
		return $this->post->email_address;
	}

	public function location() {
		$terms = wp_get_object_terms( $this->postId(), PartnerRegions::TAXONOMY );
		$location = '';
		if ( $terms && ! is_wp_error( $terms ) ) {
			$location = reset( $terms )->name;

			/*if ( $location === 'United States' ) {
				$location .= ', ' . $this->post->us_state;
			}*/
		}


		return $location;
	}

	public function regionModel() {
		$model = false;
		$terms = wp_get_object_terms( $this->postId(), PartnerRegions::TAXONOMY );
		if ( $terms && ! is_wp_error( $terms ) ) {
			$model = new PartnerRegion( reset( $terms ) );
		}

		return $model;
	}

	public function partnerType() {
		return $this->post->partner_type;
	}

	public function marketFocus() {
		return $this->post->market_focus;
	}

	public function msRelatedPercentage() {
		return $this->post->percent_ms_related;
	}

	public function partnershipInterest() {
		return $this->post->partnership_interest;
	}

	/**
	 * Returns the contents for the initial email sent to the approver(s).
	 *
	 * @return string
	 */
	public function emailContents(): string {
		$content = '';
		$fields = [
			'Name'                                     => $this->name(),
			'Organization'                             => $this->company(),
			'Phone Number'                             => $this->phoneNumber(),
			'Email Address'                            => $this->emailAddress(),
			'Partner Type'                             => $this->partnerType(),
			'Market Focus'                             => $this->marketFocus(),
			'Country'                                  => $this->location(),
			'Percentage of Microsoft related business' => $this->msRelatedPercentage(),
		];

		foreach ( $fields as $label => $contents ) {
			$content .= "\n{$label}: {$contents}";
		}

		$content .= "\n\nWhy are you interested in partnering with CoreView?\n";
		$content .= $this->partnershipInterest();

		return $content . "\n";
	}

}