<?php

namespace Coreview\Models;

use Coreview\PostTypes\Events;
use Coreview\Taxonomies\EventTypes;
use Coreview\Traits\FeaturedImage;
use Narwhal\WordPress\Traits\PostAuthor;
use Narwhal\WordPress\Traits\PostDate;
use Coreview\Traits\GeneralType;
use wpscholar\WordPress\PostModelBase;
use wpscholar\WordPress\Traits\PostContent;
use wpscholar\WordPress\Traits\PostExcerpt;
use wpscholar\WordPress\Traits\PostId;
use wpscholar\WordPress\Traits\PostPermalink;
use wpscholar\WordPress\Traits\PostThumbnail;
use wpscholar\WordPress\Traits\PostTitle;

/**
 * Class Event
 * @package Coreview\Models
 */
class Event extends PostModelBase {

	use PostAuthor,
		PostContent,
		PostDate,
		PostExcerpt,
		PostId,
		PostTitle,
		PostPermalink,
		PostThumbnail,
		GeneralType,
		FeaturedImage {
		PostContent::postContent as content;
		PostExcerpt::postExcerpt as excerpt;
		PostId::postId as id;
		PostTitle::postTitle as title;
	}

	const POST_TYPE = Events::POST_TYPE;

	private $typeTaxonomy = EventTypes::TAXONOMY;


	/**
	 * Check if post has content.
	 *
	 * @return bool
	 */
	public function hasContent() {
		return ! empty( $this->content() );
	}

	public function trimmedExcerpt( $length = 55, $more = '&hellip;' ) {
		return wp_trim_words( $this->excerpt(), $length, $more );
	}

	public function url() {
		return $this->postPermalink();
	}

	public function eventStartDate() {
		$start_date = carbon_get_post_meta( $this->id(), 'start_date' );

		if ( empty( $start_date ) ) {
			return null;
		}

		return date( 'j M Y', $start_date );
	}

	public function eventEndDate() {
		$end_date = carbon_get_post_meta( $this->id(), 'end_date' );

		if ( empty( $end_date ) ) {
			return null;
		}

		return date( 'j M Y', $end_date );
	}

	public function getVenueData() {
		return carbon_get_post_meta( $this->id(), 'venue');
	}

	public function getNextPost() {
		return get_next_post();
	}

	public function getPreviousPost() {
		return get_previous_post();
	}

	public function getTags() {
		$terms = wp_get_post_terms( $this->id(), "general-tags");

		return $terms;
	}

}
