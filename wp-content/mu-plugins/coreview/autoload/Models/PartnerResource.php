<?php

namespace Coreview\Models;

use Coreview\PostTypes\PartnerResources;
use Coreview\Taxonomies\PartnerResourceTypes;
use Coreview\Traits\GeneralType;

/**
 * Class PartnerResource
 * @package Coreview\Models
 */
class PartnerResource extends Resource {

	const POST_TYPE = PartnerResources::POST_TYPE;

	use GeneralType;

	private $typeTaxonomy = PartnerResourceTypes::TAXONOMY;

	/**
	 * Returns true if the current user has permission to view this post.
	 *
	 * @return bool
	 */
	public function canUserView(): bool {
		$canView = false;
		if ( current_user_can( 'edit_posts' ) ) {
			$canView = true;
		} else {
			$permissions = get_post_meta( get_the_ID(), 'cv_roles_with_access' );
			if ( is_array( $permissions ) ) {
				foreach ( $permissions as $permission ) {
					if ( current_user_can( $permission ) ) {
						$canView = true;
						break;
					}
				}
			}
		}

		return $canView;
	}

	public function getMainResource() {
		$primaryTerm = '';
		$terms = wp_get_post_terms( $this->id(), $this->typeTaxonomy );
		if ( $terms ) {
			$primaryTerm = $terms[0];
		}

		return $primaryTerm;
	}

	public function getMainResourceTypeName() {
		$primaryTermName = '';
		$primaryTerm = $this->getMainResource();
		if ( $primaryTerm ) {
			$primaryTermName = $primaryTerm->name;
		}

		return $primaryTermName;
	}

}
