<?php

namespace Coreview\Models;

use Coreview\PostTypes\Deals;
use Coreview\Taxonomies\PartnerRegions;
use Narwhal\WordPress\Traits\PostAuthor;
use wpscholar\WordPress\PostModelBase;
use wpscholar\WordPress\Traits\PostId;
use wpscholar\WordPress\Traits\PostTitle;

/**
 * Class Deal
 * @package Coreview\Models
 */
class Deal extends PostModelBase {

	const POST_TYPE = Deals::POST_TYPE;

	use PostId,
		PostTitle,
		PostAuthor;

	public function managerEmailAddresses() {
		$emails = [];
		$dealRegion = $this->regionModel();
		$partnerRegion = $this->regionModel();
		if ( $dealRegion && $partnerRegion ) {
			// We have both regions, as we should.
			$dealRegionId = $dealRegion->parent() ?? $dealRegion->termId();
			$partnerRegionId = $partnerRegion->parent() ?? $partnerRegion->termId();
			// Always load up the email addresses of the partners region.
			$emails = $partnerRegion->regionEmailAddresses();
			// If the deal region is not the partners region, then add the deal region email addresses.
			if ( $dealRegionId !== $partnerRegionId ) {
				$emails = array_unique(array_merge( $emails, $dealRegion->regionEmailAddresses() ));
			}
		} else {
			// TODO: Set a fallback email address!
		}

		return $emails;
	}

	public function partnerRegionModel() {
		$partnerRegion = (int) get_user_meta( $this->postAuthorId(), '_cv_partner_region', true );
		$model = false;
		if ( $partnerRegion ) {
			$regionTerm = get_term( $partnerRegion, PartnerRegions::TAXONOMY );
			if ( $regionTerm ) {
				$model = new PartnerRegion( $regionTerm );
			}
		}

		return $model;
	}

	public function regionModel() {
		$model = false;
		$terms = wp_get_object_terms( $this->postId(), PartnerRegions::TAXONOMY );
		if ( $terms && ! is_wp_error( $terms ) ) {
			$model = new PartnerRegion( reset( $terms ) );
		}

		return $model;
	}

	public function dateRequested() {
		return get_the_date( 'd M Y', $this->post );
	}

	public function company() {
		return $this->post->company_name;
	}

	public function companyWebsite() {
		return $this->post->company_website;
	}

	public function numberOf365Licenses() {
		return $this->post->number_365_licenses;
	}

	public function location() {
		$terms = wp_get_object_terms( $this->postId(), PartnerRegions::TAXONOMY );
		$location = '';
		if ( $terms && ! is_wp_error( $terms ) ) {
			$location = reset( $terms )->name;

			if ( $location === 'United States' ) {
				$location .= ', ' . $this->post->us_state;
			}
		}


		return $location;
	}

	public function budget() {
		return $this->post->budget;
	}

	public function decisionMakers() {
		// TODO: Loop through array and put in better format?
		return $this->post->decision_makers;
	}

	public function msLicensing() {
		return $this->post->ms_licensing;
	}

	public function currentLicensing() {
		return $this->post->current_licensing;
	}

	public function challenges() {
		return $this->post->challenges;
	}

	public function solutions() {
		return $this->post->solutions;
	}

	public function nextSteps() {
		return $this->post->next_steps;
	}

	public function emailContents(): string {
		$content = '';

		$customerDetails = [
			'Company Name'                  => $this->company(),
			'Company Website'               => $this->companyWebsite(),
			'Number of Office 365 Licenses' => $this->numberOf365Licenses(),
			'Country'                       => $this->location(),
			'Budget Availability'           => $this->budget(),
		];
		foreach ( $customerDetails as $label => $value ) {
			$content .= "\n{$label}: {$value}";
		}

		$content .= "\n\nDecision Makers:\n";

		foreach ( $this->decisionMakers() as $decisionMaker ) {
			$content .= "\nName: {$decisionMaker['first_name']} {$decisionMaker['last_name']}";
			$content .= "\nJob Title: {$decisionMaker['job_title']}";
			$content .= "\nPhone Number: {$decisionMaker['phone_number']}";
			$content .= "\nEmail Address: {$decisionMaker['email_address']}";
			$content .= "\n";
		}

		$content .= "\n";

		$needsChallenges = [
			'Microsoft Environment/Licensing'             => $this->msLicensing(),
			'Current Licensing or Security Solutions'     => $this->currentLicensing(),
			'Challenges CoreView can help address'        => $this->challenges(),
			'Solutions required to meet their challenges' => $this->solutions(),
			'Next steps & decision timeframe'             => $this->nextSteps(),
		];
		foreach ( $needsChallenges as $label => $value ) {
			$content .= "\n{$label}:\n{$value}\n";
		}

		return $content;
	}

}