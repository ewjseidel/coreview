<?php

namespace Coreview\Models;

use Coreview\Traits\FeaturedImage;
use wpscholar\WordPress\PostModelBase;
use wpscholar\WordPress\Traits\PostContent;
use wpscholar\WordPress\Traits\PostExcerpt;
use wpscholar\WordPress\Traits\PostId;
use wpscholar\WordPress\Traits\PostPermalink;
use wpscholar\WordPress\Traits\PostThumbnail;
use wpscholar\WordPress\Traits\PostTitle;

/**
 * Class Page
 * @package Coreview\Models
 */
class Page extends PostModelBase {

	use PostContent,
		PostExcerpt,
		PostId,
		PostTitle,
		PostPermalink,
		PostThumbnail,
		FeaturedImage {
		PostPermalink::postPermalink as url;
		PostContent::postContent as content;
		PostExcerpt::postExcerpt as excerpt;
		PostId::postId as id;
		PostTitle::postTitle as title;
	}

	const POST_TYPE = 'page';

	/**
	 * Check if post has content.
	 *
	 * @return bool
	 */
	public function hasContent() {
		return ! empty( $this->content() );
	}

	public function trimmedExcerpt( $length = 55, $more = '&hellip;' ) {
		return wp_trim_words( $this->excerpt(), $length, $more );
	}

}
