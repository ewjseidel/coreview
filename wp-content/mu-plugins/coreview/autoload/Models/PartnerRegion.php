<?php

namespace Coreview\Models;

use Coreview\Taxonomies\PartnerRegions;
use wpscholar\WordPress\TermModelBase;
use wpscholar\WordPress\Traits\TermId;
use wpscholar\WordPress\Traits\TermName;
use wpscholar\WordPress\Traits\TermSlug;

class PartnerRegion extends TermModelBase {

	const TAXONOMY = PartnerRegions::TAXONOMY;

	use TermId,
		TermName,
		TermSlug;

	public function parent(): int {
		return $this->term->parent;
	}

	public function regionEmailAddresses() {
		if ( 0 !== $this->parent() ) {
			$emails = get_term_meta( $this->parent(), 'manager_emails', true );
		} else {
			$emails = get_term_meta( $this->termId(), 'manager_emails', true );
		}

		return $emails;
	}

}