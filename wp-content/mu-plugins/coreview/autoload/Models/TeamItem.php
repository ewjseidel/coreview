<?php

namespace Coreview\Models;

use Coreview\PostTypes\Resources;
use Coreview\PostTypes\TeamItems;
use Coreview\Taxonomies\ResourceTypes;
use Coreview\Traits\FeaturedImage;
use Narwhal\WordPress\Traits\PostAuthor;
use Narwhal\WordPress\Traits\PostDate;
use wpscholar\WordPress\PostModelBase;
use wpscholar\WordPress\Traits\PostContent;
use wpscholar\WordPress\Traits\PostExcerpt;
use wpscholar\WordPress\Traits\PostId;
use wpscholar\WordPress\Traits\PostPermalink;
use wpscholar\WordPress\Traits\PostThumbnail;
use wpscholar\WordPress\Traits\PostTitle;

/**
 * Class Resource
 * @package Coreview\Models
 */
class TeamItem extends PostModelBase {

	use PostAuthor,
		PostContent,
		PostDate,
		PostExcerpt,
		PostId,
		PostTitle,
		PostPermalink,
		PostThumbnail,
		FeaturedImage {
		PostContent::postContent as content;
		PostExcerpt::postExcerpt as excerpt;
		PostId::postId as id;
		PostTitle::postTitle as title;
	}

	const POST_TYPE = TeamItems::POST_TYPE;

	/**
	 * Check if post has content.
	 *
	 * @return bool
	 */
	public function hasContent() {
		return ! empty( $this->content() );
	}

	public function trimmedExcerpt( $length = 55, $more = '&hellip;' ) {
		return wp_trim_words( $this->excerpt(), $length, $more );
	}

	public function url() {
		return $this->postPermalink();
	}

	public function personName() {
		return carbon_get_post_meta( $this->id(), 'team_name');
	}

	public function personPhoto() {
		return carbon_get_post_meta( $this->id(), 'team_photo');
	}

	public function personPosition() {
		return carbon_get_post_meta( $this->id(), 'team_position');
	}

	public function personSocial() {
		return carbon_get_post_meta( $this->id(), 'team_linkedin');
	}




}
