<?php

namespace Coreview;

class BlockMarkup {

	public static function buttonGroup( $buttons ) {
		$buttonStyleClasses = [
			'blue'         => 'has-white-color has-border has-background has-blue-background-color',
			'white-border' => 'has-blue-color has-border has-background has-white-background-color',
			'blue-border'  => 'has-blue-color has-border has-blue-border',
			'blue-arrow-link' => 'has-blue-color has-arrow',
			'black-arrow-link' => 'has-black-color has-arrow',
			'white-arrow-link' => 'has-white-color has-arrow',

		];

		?>
		<div class="wp-block-narwhal-button-collection">
			<?php
			foreach ( $buttons as $button ) {
				if ( ! isset( $buttonStyleClasses[ $button['button_style'] ] ) ) {
					// Force a default.
					$button['button_style'] = 'blue';
				};
				?>
				<div class="wp-block-button cv-button">
					<a class="wp-block-button__link <?php echo esc_attr( $buttonStyleClasses[ $button['button_style'] ] ); ?>"
					   href="<?php echo esc_url( $button['button_url'] ); ?>">
						<?php echo esc_html( $button['button_text'] ); ?></a>
				</div>
				<?php
			}
			?>
		</div>
		<?php
	}

	public static function grid( $data, $callback ) {
		if ( ! is_callable( $callback ) ) {
			return;
		}

		?>
		<div class="wp-block-narwhal-grid-wrapper">
			<div class="wp-block-narwhal-grid has-<?php echo esc_attr( $data['grid_columns'] ? : 2 ); ?>-columns">
				<?php
				foreach ( $data['grid_items'] as $gridItem ) {
					// Call the callback to render the grid items.
					$callback( $gridItem );
				}
				?>
			</div>
		</div>
		<?php
	}

	public static function sectionHeading( $data ) {
	    ?>
        <div class="section-heading">
            <div class="section-heading_headline">
                <h2><?php echo esc_html( $data['section_headline'] );?></h2>
            </div>
            <?php if( !empty( $data['section_subtext'])):?>
                <div class="section-heading_subtext">
		            <?php echo $data['section_subtext'];?>
                </div>
            <?php endif;?>

        </div>
    <?php
    }

	public static function form( $data ) {


		?>
        <div class="marketo-form">
            <div class="marketo-form__inner form-id--<?php echo $data['form_id']?>">
	            <?php
	            global $wp_query;
	            if (isset($wp_query->query_vars['form-submit']) ) : ?>
                    <?php echo $data['thank_you_text']; ?>
                <?php else: ?>
		            <?php echo do_shortcode('[marketo_form id="' . $data['form_id'] . '" redirect-url="'. $data['redirect'].'" ]');?>
	            <?php endif; ?>
            </div>

        </div>
		<?php
	}

}
