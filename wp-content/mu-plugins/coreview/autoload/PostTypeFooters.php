<?php

namespace Coreview;

use Carbon_Fields\Container;
use Carbon_Fields\Field\Field;
use Coreview\PostTypes\MarketplaceItems;
use Coreview\PostTypes\OnDemandTrainingItems;

class PostTypeFooters {

	const MAX_FOOTER_BLOCKS = 1;

	protected static $postTypes = [
		'post'                           => 'Blog Post',
		
	];

	public static function initialize() {
		add_action( 'carbon_fields_register_fields', [ __CLASS__, 'registerPostFooterFields' ] );
	}

	public static function registerPostFooterFields() {
		$types = [
			[
				'type'      => 'post',
				'post_type' => 'wp_block',
			]
		];

		// General post meta container for supported post types.
		Container::make( 'post_meta', 'Footer Sections' )
		         ->where( 'post_type', 'IN', array_keys( self::$postTypes ) )
		         ->set_context( 'advanced' )
		         ->set_priority( 'default' )
		         ->add_fields( [
			         Field::make( 'association', 'coreview_post_footer_sections' )
			              ->set_max( self::MAX_FOOTER_BLOCKS )
			              ->set_types( $types ),
		         ] );

		// Fields for global defaults.
		$globalFields = [];
		foreach ( self::$postTypes as $postType => $label ) {
			$globalFields[] = Field::make( 'association', 'tri_post_footer_' . $postType, $label . ' Default Footer Content' )
			                       ->set_max( self::MAX_FOOTER_BLOCKS )
			                       ->set_types( $types );
		}
		Container::make( 'theme_options', esc_html__( 'Post Type Options', 'coreview' ) )
		         ->set_page_parent( 'options-general.php' )
		         ->add_fields( $globalFields );
	}

	/**
	 * @param array $footerSections
	 *
	 * @return array
	 */
	protected static function processMetaToPosts( $footerSections ) {
		$footers = [];
		if ( is_array( $footerSections ) ) {
			foreach ( $footerSections as $footerSection ) {
				$block = get_post( $footerSection['id'] );
				if ( $block ) {
					$footers[] = $block;
				}
			}
		}

		return $footers;
	}

	/**
	 * Returns an array of WP_Post objects to be displayed in the footer.
	 *
	 * @param int $postId
	 *
	 * @return \WP_Post[]
	 */
	public static function getPostFooters( $postId = null ) {
		if ( null === $postId ) {
			$postId = get_the_ID();
		}

		$footers = [];
		$postType = get_post_type( $postId );

		// If post type isn't supported, bail early.
		if ( ! $postType || ! isset( self::$postTypes[ $postType ] ) ) {
			return $footers;
		}

		// Check post meta for valid data.
		$footerSections = carbon_get_post_meta( $postId, 'coreview_post_footer_sections' );
		$footers = self::processMetaToPosts( $footerSections );

		// If no data yet, check for global settings.
		if ( empty( $footers ) ) {
			$footers = self::processMetaToPosts( getCFi18nThemeOption( 'tri_post_footer_' . $postType ) );
		}

		return $footers;
	}

}