<?php

namespace Coreview;

use Coreview\PostTypes\CaseStudies;
use Coreview\PostTypes\Events;
use Coreview\PostTypes\Integrations;
use Coreview\PostTypes\MarketplaceItems;
use Coreview\PostTypes\NewsItems;
use Coreview\PostTypes\OnDemandTrainingItems;
use Coreview\PostTypes\Partners;
use Coreview\PostTypes\Posts;
use Coreview\PostTypes\Resources;
use Coreview\PostTypes\Solutions;
use Coreview\PostTypes\TeamItems;
use Coreview\PostTypes\UpcomingTrainingItems;

class AllowedBlocks {

	const ALL_BLOCKS = [
		'core/block',
		'core/paragraph',
		'core/image',
		'core/heading',
		'core/gallery',
		'core/list',
		'core/quote',
		'core/audio',
		'core/cover-image',
		'core/file',
		'core/video',
		'core/table',
//		'core/verse',
//		'core/code',
		'core/freeform',
		'core/html',
		'core/preformatted',
		'core/pullquote',
		'core/button',
		'core/text-columns',
//		'core/media-text',
//		'core/more',
//		'core/nextpage',
//		'core/separator',
//		'core/spacer',
		'core/shortcode',
//		'core/archives',
//		'core/categories',
//		'core/latest-comments',
//		'core/latest-posts',
//		'core/embed',
//		'core-embed/twitter',
//		'core-embed/youtube',
//		'core-embed/facebook',
//		'core-embed/instagram',
//		'core-embed/wordpress',
//		'core-embed/soundcloud',
//		'core-embed/spotify',
//		'core-embed/flickr',
//		'core-embed/vimeo',
//		'core-embed/animoto',
//		'core-embed/cloudup',
//		'core-embed/collegehumor',
//		'core-embed/dailymotion',
//		'core-embed/funnyordie',
//		'core-embed/hulu',
//		'core-embed/imgur',
//		'core-embed/issuu',
//		'core-embed/kickstarter',
//		'core-embed/meetup-com',
//		'core-embed/mixcloud',
//		'core-embed/photobucket',
//		'core-embed/polldaddy',
//		'core-embed/reddit',
//		'core-embed/reverbnation',
//		'core-embed/screencast',
//		'core-embed/scribd',
//		'core-embed/slideshare',
//		'core-embed/smugmug',
//		'core-embed/speaker',
//		'core-embed/ted',
//		'core-embed/tumblr',
//		'core-embed/videopress',
//		'core-embed/wordpress-tv',
		'narwhal/background',
		'narwhal/post-type-archive',
		'narwhal/taxonomy-based-archive',
		'carbon-fields/bullet-grid',
		'carbon-fields/contact-elements',
		'carbon-fields/contact-elements-map',
		'carbon-fields/cta-block',
		'carbon-fields/disruptor',
		'carbon-fields/featured-resource',
		'carbon-fields/form-block',
		'carbon-fields/icon-grid',
		'carbon-fields/left-right',
		'carbon-fields/left-right-icon-grid',
		'carbon-fields/logo-list',
		'carbon-fields/pricing-tabber',
        'carbon-fields/regular-header',
		'carbon-fields/resource-grid',
		'carbon-fields/simple-header',
		'carbon-fields/single-stat',
		'carbon-fields/single-testimonial',
		'carbon-fields/small-header',
		'carbon-fields/testimonial-slider',
		'carbon-fields/top-level-header',
		'carbon-fields/vertical-tabber',
	];

	const DISABLED_BLOCKS = [
		'page'                           => [],
		Events::POST_TYPE                => [
			'narwhal/template-content-placeholder',
			'narwhal/left-right-sidebar',
			'narwhal/page-header',
			'narwhal/post-type-archive',
			'narwhal/taxonomy-based-archive',
		],
		NewsItems::POST_TYPE             => [
			'narwhal/template-content-placeholder',
			'narwhal/left-right-sidebar',
			'narwhal/page-header',
			'narwhal/post-type-archive',
			'narwhal/taxonomy-based-archive',
		],
		Posts::POST_TYPE                 => [
			'narwhal/template-content-placeholder',
			'narwhal/left-right-sidebar',
			'narwhal/page-header',
			'narwhal/post-type-archive',
			'narwhal/taxonomy-based-archive',
		],
		Resources::POST_TYPE             => [
			'narwhal/template-content-placeholder',
			'narwhal/left-right-sidebar',
			'narwhal/page-header',
			'narwhal/post-type-archive',
			'narwhal/taxonomy-based-archive',
		],
		TeamItems::POST_TYPE             => [
			'narwhal/template-content-placeholder',
			'narwhal/left-right-sidebar',
			'narwhal/page-header',
			'narwhal/post-type-archive',
			'narwhal/taxonomy-based-archive',
		],
	];

	public static function initialize() {
		add_filter( 'allowed_block_types', [ __CLASS__, '_allowedBlocks' ], 10, 2 );
	}

	public static function _allowedBlocks( $allowedBlocks, \WP_Post $post ) {
		if ( is_array( $allowedBlocks ) || true === $allowedBlocks ) {
			if ( isset( self::DISABLED_BLOCKS[ $post->post_type ] ) ) {
				$allowedBlocks = array_values( array_diff( self::ALL_BLOCKS, self::DISABLED_BLOCKS[ $post->post_type ] ) );
			}
		}

		return $allowedBlocks;
	}

}