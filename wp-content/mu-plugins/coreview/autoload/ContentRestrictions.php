<?php

namespace Coreview;

use Carbon_Fields\Container;
use Carbon_Fields\Field\Field;
use Coreview\PostTypes\PartnerResources;

/**
 * Allows restricting content based off of user permissions.
 *
 * Class ContentRestrictions
 * @package Coreview
 */
class ContentRestrictions {

	const POST_TYPES = [
		PartnerResources::POST_TYPE,
	];

	public static function initialize(): void {
		add_action( 'carbon_fields_register_fields', [ __CLASS__, '_registerFields' ] );
		add_action( 'template_redirect', [ __CLASS__, '_templateRedirect' ] );
		add_action( 'carbon_fields_post_meta_container_saved', [ __CLASS__, '_carbonSavePost' ] );
	}

	public static function _registerFields(): void {
		$roles = [];
		foreach ( PartnerRoles::ROLES as $role => $args ) {
			$roles[ $role ] = $args['label'];
		}

		Container::make( 'post_meta', 'Content Restrictions' )
		         ->where( 'post_type', 'IN', self::POST_TYPES )
		         ->set_context( 'side' )
		         ->add_fields( [
			         Field::make( 'set', 'cv_access_roles', __( 'Roles with Access', 'coreview' ) )
			              ->set_required()
			              ->set_options( $roles ),
		         ] );
	}

	public static function _carbonSavePost( $postId ): void {
		if ( in_array( get_post_type( $postId ), self::POST_TYPES, true ) ) {
			$roles = carbon_get_post_meta( $postId, 'cv_access_roles' );
			if ( ! is_array( $roles ) ) {
				$roles = [];
			}

			$currentRoles = get_post_meta( $postId, 'cv_roles_with_access' );
			$add = array_diff( $roles, $currentRoles );
			$remove = array_diff( $currentRoles, $roles );

			foreach ( $add as $addRole ) {
				add_post_meta( $postId, 'cv_roles_with_access', $addRole );
			}

			foreach ( $remove as $removeRole ) {
				delete_post_meta( $postId, 'cv_roles_with_access', $removeRole );
			}
		}
	}

	public static function _templateRedirect(): void {
		if ( is_singular( self::POST_TYPES ) ) {
			$canView = false;
			if ( current_user_can( 'edit_posts' ) ) {
				// User is considered an admin. Let them in.
				return;
			}

			$permissions = get_post_meta( get_the_ID(), 'cv_roles_with_access' );
			if ( is_array( $permissions ) ) {
				foreach ( $permissions as $permission ) {
					if ( current_user_can( $permission ) ) {
						$canView = true;
						break;
					}
				}
			}

			if ( ! $canView ) {
				// Redirect user away...
				// TODO: Do we want different functionality here?
				wp_safe_redirect( home_url( '/' ) );
				exit();
			}
		}
	}

}