<?php

namespace Coreview\Taxonomies;

use Coreview\PostTypes\CaseStudies;
use Coreview\PostTypes\Events;
use Coreview\PostTypes\Posts;
use Coreview\PostTypes\Resources;

/**
 * Class GeneralTopic
 * @package Coreview\Taxonomies
 */
class GeneralTopic {

	const TAXONOMY = 'cv-general-topic';

	public static function initialize() {
		add_action( 'init', [ __CLASS__, 'registerTaxonomy' ] );
	}

	public static function registerTaxonomy() {

		// Add new taxonomy, make it hierarchical (like categories)
		$labels = array(
			'name'              => esc_html_x( 'Topics', 'taxonomy general name', 'coreview' ),
			'singular_name'     => esc_html_x( 'Topic', 'taxonomy singular name', 'coreview' ),
			'search_items'      => esc_html__( 'Search Topic', 'coreview' ),
			'all_items'         => esc_html__( 'All Topics', 'coreview' ),
			'parent_item'       => esc_html__( 'Parent Topic', 'coreview' ),
			'parent_item_colon' => esc_html__( 'Parent Topic:', 'coreview' ),
			'edit_item'         => esc_html__( 'Edit Topic', 'coreview' ),
			'update_item'       => esc_html__( 'Update Topic', 'coreview' ),
			'add_new_item'      => esc_html__( 'Add New Topic', 'coreview' ),
			'new_item_name'     => esc_html__( 'New Topic', 'coreview' ),
			'menu_name'         => esc_html__( 'Topics', 'coreview' ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'show_in_rest'      => true,
			'rewrite'           => [
				'slug' => 'general-topic',
			],
		);

		$args = apply_filters( 'taxonomy_args-' . self::TAXONOMY, $args );

		$postTypes = [

		];

		register_taxonomy( self::TAXONOMY, $postTypes, $args );

		foreach ( $postTypes as $postType ) {
			register_taxonomy_for_object_type( self::TAXONOMY, $postType );
		}
	}

}
