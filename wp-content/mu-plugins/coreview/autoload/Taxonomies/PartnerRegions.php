<?php

namespace Coreview\Taxonomies;

use Coreview\PostTypes\Deals;
use Coreview\PostTypes\PartnershipRequests;
use FlexFields\Make;

class PartnerRegions {

	const TAXONOMY = 'cv-partner-region';

	public static function initialize(): void {
		add_action( 'init', [ __CLASS__, 'registerTaxonomy' ], 0 );
		add_action( 'init', [ __CLASS__, 'registerFields' ] );
	}

	public static function registerTaxonomy(): void {

		// Add new taxonomy, make it hierarchical (like categories)
		$labels = array(
			'name'              => esc_html_x( 'Region', 'taxonomy general name', 'coreview' ),
			'singular_name'     => esc_html_x( 'Region', 'taxonomy singular name', 'coreview' ),
			'search_items'      => esc_html__( 'Search Region', 'coreview' ),
			'all_items'         => esc_html__( 'All Regions', 'coreview' ),
			'parent_item'       => esc_html__( 'Parent Region', 'coreview' ),
			'parent_item_colon' => esc_html__( 'Parent Region:', 'coreview' ),
			'edit_item'         => esc_html__( 'Edit Region', 'coreview' ),
			'update_item'       => esc_html__( 'Update Region', 'coreview' ),
			'add_new_item'      => esc_html__( 'Add New Region', 'coreview' ),
			'new_item_name'     => esc_html__( 'New Region', 'coreview' ),
			'menu_name'         => esc_html__( 'Regions', 'coreview' ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'show_in_rest'      => true,
			'rewrite'           => [
				'slug' => 'partner-resource-type',
			],
			'capabilities' => [
				'assign_terms' => 'read',
			],
		);

		$args = apply_filters( 'taxonomy_args-' . self::TAXONOMY, $args );

		$postTypes = [
			Deals::POST_TYPE,
			PartnershipRequests::POST_TYPE,
		];

		register_taxonomy( self::TAXONOMY, $postTypes, $args );

		foreach ( $postTypes as $postType ) {
			register_taxonomy_for_object_type( self::TAXONOMY, $postType );
		}
	}

	public static function registerFields() {
		$termMetaBox = Make::TermMetaBox( self::TAXONOMY );

		$termMetaBox->fields->registerField( 'manager_emails', [
			'label'       => esc_html__( 'Manager Emails', 'ko-exception-requests' ),
			'description' => esc_html__( 'Note: emails should only be entered for the regional level. The country level is not used at this time.', 'coreview' ),
			'field'       => 'repeating-text',
			'type'        => 'email',
		] );
	}

	public static function getUserRegionOptions() {
		$options = [
			0 => '-- Select Region --',
		];
		$terms = get_terms( [
			'taxonomy'   => self::TAXONOMY,
			'hide_empty' => false,
			'parent'     => 0,
		] );

		if ( $terms && ! is_wp_error( $terms ) ) {
			foreach ( $terms as $term ) {
				/* @var \WP_Term $term */
				$options[ $term->term_id ] = $term->name;
			}
		}

		return $options;
	}

	/**
	 * Returns all country terms (i.e. not top level terms).
	 *
	 * @return array
	 */
	public static function getAllCountryTerms(): array {
		$terms = get_terms( [
			'taxonomy'   => self::TAXONOMY,
			'hide_empty' => false,
		] );
		$countries = [];

		if ( $terms && ! is_wp_error( $terms ) ) {
			foreach ( $terms as $term ) {
				/* @var \WP_Term $term */
				if ( 0 !== $term->parent ) {
					$countries[] = $term;
				}
			}
		}

		return $countries;
	}

	public static function usaId() {
		$id = - 1;
		$term = get_term_by( 'slug', 'united-states', self::TAXONOMY );
		if ( $term ) {
			$id = $term->term_id;
		}

		return $id;
	}

}