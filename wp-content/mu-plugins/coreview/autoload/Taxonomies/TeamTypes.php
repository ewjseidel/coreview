<?php

namespace Coreview\Taxonomies;

use Coreview\PostTypes\TeamItems;

/**
 * Class TeamTypes
 * @package Coreview\Taxonomies
 */
class TeamTypes {

	const TAXONOMY = 'cv-team-type';

	public static function initialize() {
		add_action( 'init', [ __CLASS__, 'registerTaxonomy' ] );
	}

	public static function registerTaxonomy() {

		// Add new taxonomy, make it hierarchical (like categories)
		$labels = array(
			'name'              => esc_html_x( 'Team Types', 'taxonomy general name', 'coreview' ),
			'singular_name'     => esc_html_x( 'Team Type', 'taxonomy singular name', 'coreview' ),
			'search_items'      => esc_html__( 'Search Team Types', 'coreview' ),
			'all_items'         => esc_html__( 'All Team Types', 'coreview' ),
			'parent_item'       => esc_html__( 'Parent Team Type', 'coreview' ),
			'parent_item_colon' => esc_html__( 'Parent Team Type:', 'coreview' ),
			'edit_item'         => esc_html__( 'Edit Team Type', 'coreview' ),
			'update_item'       => esc_html__( 'Update Team Type', 'coreview' ),
			'add_new_item'      => esc_html__( 'Add New Team Type', 'coreview' ),
			'new_item_name'     => esc_html__( 'New Team Type', 'coreview' ),
			'menu_name'         => esc_html__( 'Team Types', 'coreview' ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'show_in_rest'      => true,
			'rewrite'           => [
				'slug' => 'Team-type',
			],
		);

		$args = apply_filters( 'taxonomy_args-' . self::TAXONOMY, $args );

		$postTypes = [
			TeamItems::POST_TYPE,
		];

		register_taxonomy( self::TAXONOMY, $postTypes, $args );

		foreach ( $postTypes as $postType ) {
			register_taxonomy_for_object_type( self::TAXONOMY, $postType );
		}
	}

}
