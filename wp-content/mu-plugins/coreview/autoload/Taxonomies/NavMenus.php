<?php

namespace Coreview\Taxonomies;

class NavMenus {

	public static function initialize() {
		add_action( 'init', [ __CLASS__, 'registerTaxonomy' ] );
	}

	public static function registerTaxonomy() {
		$tax = get_taxonomy( 'nav_menu' );
		if ( $tax ) {
			$args = (array) $tax;
			$args['show_in_rest'] = true;
			$args['rest_base'] = 'nav-menus';
			register_taxonomy( 'nav_menu', 'nav_menu_item', $args );
		}
	}

}