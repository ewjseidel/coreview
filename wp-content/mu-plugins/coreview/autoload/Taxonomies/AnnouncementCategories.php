<?php

namespace Coreview\Taxonomies;

use Coreview\Collections\EventCollection;
use Coreview\PostTypes\CaseStudies;
use Coreview\PostTypes\Events;
use Coreview\PostTypes\NewsItems;
use Coreview\PostTypes\Posts;
use Coreview\PostTypes\Resources;

/**
 * Class AnnouncementCategories
 * @package Coreview\Taxonomies
 */
class AnnouncementCategories {

	const TAXONOMY = 'cv-announcement-category';

	public static function initialize() {
		add_action( 'init', [ __CLASS__, 'registerTaxonomy' ] );
	}

	public static function registerTaxonomy() {

		// Add new taxonomy, make it hierarchical (like categories)
		$labels = array(
			'name'              => esc_html_x( 'Announcement Category', 'taxonomy general name', 'coreview' ),
			'singular_name'     => esc_html_x( 'Announcement Category', 'taxonomy singular name', 'coreview' ),
			'search_items'      => esc_html__( 'Search Announcement Categories', 'coreview' ),
			'all_items'         => esc_html__( 'All Categories', 'coreview' ),
			'parent_item'       => esc_html__( 'Parent Announcement Category', 'coreview' ),
			'parent_item_colon' => esc_html__( 'Parent Announcement Category:', 'coreview' ),
			'edit_item'         => esc_html__( 'Edit Announcement Category', 'coreview' ),
			'update_item'       => esc_html__( 'Update Announcement Category', 'coreview' ),
			'add_new_item'      => esc_html__( 'Add New Announcement Category', 'coreview' ),
			'new_item_name'     => esc_html__( 'New Announcement Category', 'coreview' ),
			'menu_name'         => esc_html__( 'Announcement Category', 'coreview' ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'show_in_rest'      => true,
			'rewrite'           => [
				'slug' => 'announcement-category',
			],
		);

		$args = apply_filters( 'taxonomy_args-' . self::TAXONOMY, $args );

		$postTypes = [
			NewsItems::POST_TYPE,
			Events::POST_TYPE,
		];

		register_taxonomy( self::TAXONOMY, $postTypes, $args );

		foreach ( $postTypes as $postType ) {
			register_taxonomy_for_object_type( self::TAXONOMY, $postType );
		}
	}

}
