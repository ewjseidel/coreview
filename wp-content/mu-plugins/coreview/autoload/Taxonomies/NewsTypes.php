<?php

namespace Coreview\Taxonomies;

use Coreview\PostTypes\NewsItems;

/**
 * Class NewsTypes
 * @package Coreview\Taxonomies
 */
class NewsTypes {

	const TAXONOMY = 'cv-news-type';

	public static function initialize() {
		add_action( 'init', [ __CLASS__, 'registerTaxonomy' ] );
	}

	public static function registerTaxonomy() {

		// Add new taxonomy, make it hierarchical (like categories)
		$labels = array(
			'name'              => esc_html_x( 'News Types', 'taxonomy general name', 'coreview' ),
			'singular_name'     => esc_html_x( 'News Type', 'taxonomy singular name', 'coreview' ),
			'search_items'      => esc_html__( 'Search News Types', 'coreview' ),
			'all_items'         => esc_html__( 'All News Types', 'coreview' ),
			'parent_item'       => esc_html__( 'Parent News Type', 'coreview' ),
			'parent_item_colon' => esc_html__( 'Parent News Type:', 'coreview' ),
			'edit_item'         => esc_html__( 'Edit News Type', 'coreview' ),
			'update_item'       => esc_html__( 'Update News Type', 'coreview' ),
			'add_new_item'      => esc_html__( 'Add New News Type', 'coreview' ),
			'new_item_name'     => esc_html__( 'New News Type', 'coreview' ),
			'menu_name'         => esc_html__( 'News Types', 'coreview' ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'show_in_rest'      => true,
			'rewrite'           => [
				'slug' => 'news-type',
			],
		);

		$args = apply_filters( 'taxonomy_args-' . self::TAXONOMY, $args );

		$postTypes = [
			NewsItems::POST_TYPE,
		];

		register_taxonomy( self::TAXONOMY, $postTypes, $args );

		foreach ( $postTypes as $postType ) {
			register_taxonomy_for_object_type( self::TAXONOMY, $postType );
		}
	}

}
