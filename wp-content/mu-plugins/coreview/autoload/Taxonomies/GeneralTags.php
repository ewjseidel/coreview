<?php

namespace Coreview\Taxonomies;

use Coreview\PostTypes\Events;
use Coreview\PostTypes\NewsItems;
use Coreview\PostTypes\PartnerResources;
use Coreview\PostTypes\Posts;
use Coreview\PostTypes\Resources;

/**
 * Class GeneralTags
 * @package Coreview\Taxonomies
 */
class GeneralTags {

	const TAXONOMY = 'general-tags';

	public static function initialize() {
		add_action( 'init', [ __CLASS__, 'registerTaxonomy' ] );
	}

	public static function registerTaxonomy() {

		// Add new taxonomy, make it hierarchical (like categories)
		$labels = array(
			'name'              => esc_html_x( 'Tags', 'taxonomy general name', 'coreview' ),
			'singular_name'     => esc_html_x( 'Tag', 'taxonomy singular name', 'coreview' ),
			'search_items'      => esc_html__( 'Search Tags', 'coreview' ),
			'all_items'         => esc_html__( 'All Tags', 'coreview' ),
			'parent_item'       => esc_html__( 'Parent Tag', 'coreview' ),
			'parent_item_colon' => esc_html__( 'Parent Tag:', 'coreview' ),
			'edit_item'         => esc_html__( 'Edit Tag', 'coreview' ),
			'update_item'       => esc_html__( 'Update Tag', 'coreview' ),
			'add_new_item'      => esc_html__( 'Add New Tag', 'coreview' ),
			'new_item_name'     => esc_html__( 'New Tag', 'coreview' ),
			'menu_name'         => esc_html__( 'Tags', 'coreview' ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'show_in_rest'      => true,
			'rewrite'           => [
				'slug' => 'tags',
			],
		);

		$args = apply_filters( 'taxonomy_args-' . self::TAXONOMY, $args );

		$postTypes = [
			Events::POST_TYPE,
			PartnerResources::POST_TYPE,
			Resources::POST_TYPE,
			NewsItems::POST_TYPE,
			Posts::POST_TYPE
		];

		register_taxonomy( self::TAXONOMY, $postTypes, $args );

		foreach ( $postTypes as $postType ) {
			register_taxonomy_for_object_type( self::TAXONOMY, $postType );
		}
	}

}
