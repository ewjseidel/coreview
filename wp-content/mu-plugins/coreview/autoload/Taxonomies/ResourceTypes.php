<?php

namespace Coreview\Taxonomies;

use Coreview\PostTypes\Resources;

/**
 * Class ResourceTypes
 * @package Coreview\Taxonomies
 */
class ResourceTypes {

	const TAXONOMY = 'cv-resource-type';

	public static function initialize() {
		add_action( 'init', [ __CLASS__, 'registerTaxonomy' ] );
	}

	public static function registerTaxonomy() {

		// Add new taxonomy, make it hierarchical (like categories)
		$labels = array(
			'name'              => esc_html_x( 'Type', 'taxonomy general name', 'coreview' ),
			'singular_name'     => esc_html_x( 'Type', 'taxonomy singular name', 'coreview' ),
			'search_items'      => esc_html__( 'Search Type', 'coreview' ),
			'all_items'         => esc_html__( 'All Types', 'coreview' ),
			'parent_item'       => esc_html__( 'Parent Type', 'coreview' ),
			'parent_item_colon' => esc_html__( 'Parent Type:', 'coreview' ),
			'edit_item'         => esc_html__( 'Edit Type', 'coreview' ),
			'update_item'       => esc_html__( 'Update Type', 'coreview' ),
			'add_new_item'      => esc_html__( 'Add New Type', 'coreview' ),
			'new_item_name'     => esc_html__( 'New Type', 'coreview' ),
			'menu_name'         => esc_html__( 'Types', 'coreview' ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'show_in_rest'      => true,
			'rewrite'           => [
				'slug' => 'resource-type',
			],
		);

		$args = apply_filters( 'taxonomy_args-' . self::TAXONOMY, $args );

		$postTypes = [
			Resources::POST_TYPE,
			'post'
		];

		register_taxonomy( self::TAXONOMY, $postTypes, $args );

		foreach ( $postTypes as $postType ) {
			register_taxonomy_for_object_type( self::TAXONOMY, $postType );
		}
	}

}
