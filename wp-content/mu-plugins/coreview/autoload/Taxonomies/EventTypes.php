<?php

namespace Coreview\Taxonomies;

use Coreview\PostTypes\Events;

/**
 * Class EventTypes
 * @package Coreview\Taxonomies
 */
class EventTypes {

	const TAXONOMY = 'cv-events-type';

	public static function initialize() {
		add_action( 'init', [ __CLASS__, 'registerTaxonomy' ] );
	}

	public static function registerTaxonomy() {

		// Add new taxonomy, make it hierarchical (like categories)
		$labels = array(
			'name'              => esc_html_x( 'Event Types', 'taxonomy general name', 'coreview' ),
			'singular_name'     => esc_html_x( 'Event Type', 'taxonomy singular name', 'coreview' ),
			'search_items'      => esc_html__( 'Search Events Types', 'coreview' ),
			'all_items'         => esc_html__( 'All Events Types', 'coreview' ),
			'parent_item'       => esc_html__( 'Parent Events Type', 'coreview' ),
			'parent_item_colon' => esc_html__( 'Parent Events Type:', 'coreview' ),
			'edit_item'         => esc_html__( 'Edit Events Type', 'coreview' ),
			'update_item'       => esc_html__( 'Update Events Type', 'coreview' ),
			'add_new_item'      => esc_html__( 'Add New Event Type', 'coreview' ),
			'new_item_name'     => esc_html__( 'New Event Type', 'coreview' ),
			'menu_name'         => esc_html__( 'Events Types', 'coreview' ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'show_in_rest'      => true,
			'rewrite'           => [
				'slug' => 'event-type',
			],
		);

		$args = apply_filters( 'taxonomy_args-' . self::TAXONOMY, $args );

		$postTypes = [
			Events::POST_TYPE,
		];

		register_taxonomy( self::TAXONOMY, $postTypes, $args );

		foreach ( $postTypes as $postType ) {
			register_taxonomy_for_object_type( self::TAXONOMY, $postType );
		}
	}

}
