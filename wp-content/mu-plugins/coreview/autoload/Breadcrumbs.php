<?php

namespace Coreview;

class Breadcrumbs {

	/**
	 * @var array Stores breadcrumbs
	 */
	public $crumbs = [];

	public $before = '';

	public $after = '';

	/**
	 * Breadcrumbs constructor.
	 *
	 * @param string $before
	 * @param string $after
	 */
	public function __construct( $before = '', $after = '' ) {
		$this->before = $before;
		$this->after = $after;
		$this->makeCrumbs();
	}

	/**
	 * @param int $pageId
	 */
	public function makeCrumbs( $pageId = null ) {
		if ( null === $pageId ) {
			$pageId = get_the_ID();
			$post = get_post( $pageId );

			if ( is_front_page() ) {
				$this->homeCrumb();
			} elseif ( is_singular() ) {
				if ( $post ) {
					$this->otherCrumb( $post );
				}
			}
		} else {
			$post = get_post( $pageId );

			if ( $post ) {
				$this->otherCrumb( $post );
			}
		}
	}

	protected function homeCrumb() {
		$this->crumbs[] = $this->singleCrumb( home_url( '/' ), esc_html__( 'Home', 'Coreview' ) );
	}

	protected function otherCrumb( \WP_Post $post ) {
		$this->crumbs[] = $this->singleCrumb( get_the_permalink( $post ), get_the_title( $post ) );

		// Check if we have parent items.
		if ( 0 !== $post->post_parent ) {
			$this->makeCrumbs( $post->post_parent );
		} else {
			$this->homeCrumb();
		}
	}

	protected function singleCrumb( $url, $title ) {
		return $this->before . '<a href="' . esc_url( $url ) . '">' . esc_html( $title ) . '</a>' . $this->after;
	}

	public function theCrumbs() {
		$crumbs = array_reverse( $this->crumbs );
		echo wp_kses( implode( ' ', $crumbs ), [
			'a'   => [
				'class' => true,
				'href'  => true,
			],
			'div' => [
				'class' => true,
			],
            'li' => [
                'class' => true,
            ],
            'span' => [
                'class' => true,
            ],
        ] );
	}

}