<?php

namespace Coreview;

use Carbon_Fields\Block;
use Carbon_Fields\Container;
use Carbon_Fields\Field;


/**
 * Custom functionality for the partner portal.
 *
 * Class PartnerPortal
 * @package Coreview
 */
class PartnerPortal {

	const PAGE_TEMPLATE = 'template-partner-portal-dashboard.php';

	public static function initialize(): void {
		add_action( 'template_redirect', [ __CLASS__, '_templateRedirect' ] );
		add_action( 'carbon_fields_register_fields', [ __CLASS__, 'registerFields' ] );

	}

	/**
	 * Redirect users away from partner portal pages if they don't have permission to be there.
	 */
	public static function _templateRedirect(): void {
		if ( is_singular( 'page' ) ) {
			$template = basename( get_page_template() );
			if ( strpos( $template, 'template-partner-portal-' ) === 0 ) {
				if ( ! current_user_can( 'edit_posts' ) ) {
					$canView = false;
					foreach ( array_keys( PartnerRoles::ROLES ) as $permission ) {
						if ( current_user_can( $permission ) ) {
							$canView = true;
							break;
						}
					}

					if ( ! $canView ) {
						wp_safe_redirect( home_url( '/' ) );
						exit();
					}
				}
			}
		}
	}

	/**
	 * Returns the URL to the partner dashboard page.
	 *
	 * @return string
	 */
	public static function dashboardUrl(): string {
		return self::getPageUrlByTemplate( 'template-partner-portal-dashboard.php' );
	}

	/**
	 * Returns the URL to the partner resources archive page.
	 *
	 * @return string
	 */
	public static function resourcesUrl(): string {
		return self::getPageUrlByTemplate( 'template-partner-portal-resources.php' );
	}

	/**
	 * Helper function for finding the first page utilizing the specified page template.
	 *
	 * @param $template
	 *
	 * @return string
	 */
	protected static function getPageUrlByTemplate( $template ): string {
		// Use a cache to keep from hitting the database for this info for 1 minute.
		$cacheKey = md5( __METHOD__ . $template );
		$url = wp_cache_get( $cacheKey );
		if ( ! $url ) {
			$url = home_url( '/' );
			$query = new \WP_Query( [
				'post_type'      => 'page',
				'posts_per_page' => 1,
				'no_found_rows'  => true,
				'meta_query'     => [
					[
						'key'     => '_wp_page_template',
						'compare' => '=',
						'value'   => $template,
					],
				],
			] );

			if ( $query->have_posts() ) {
				$url = get_the_permalink( $query->post );
			}

			wp_cache_set( $cacheKey, $url, '', MINUTE_IN_SECONDS );
		}

		return $url;
	}


	public static function registerFields() {
		Container::make('post_meta','News')
		         ->where( 'post_type', '=', 'page' )
		         ->where( 'post_template', '=', self::PAGE_TEMPLATE )
		         ->add_fields([
			         Field::make('text','news_heading', __( 'News Section Heading', 'coreview')),
			         Field::make('complex', 'news_item', __( 'News Item', 'coreveiw'))
			              ->set_layout( 'tabbed-horizontal' )
			              ->add_fields( array(
				              Field::make( 'text', 'news_item_heading', __( 'News Item Heading', 'coreview' ) ),
				              Field::make( 'rich_text', 'news_item_body', __( 'News Body Copy', 'coreview' ) )

			              ) )

		         ]);


		Container::make('post_meta', 'Program Overview')
		         ->where( 'post_type', '=', 'page' )
		         ->where( 'post_template', '=', self::PAGE_TEMPLATE )
		         ->add_fields([
			         Field::make('text','program_heading', __( 'Program Section Heading', 'coreview'))
			              ->set_help_text( 'Add text to append to the user type on the dashboard' ),
			         Field::make( 'rich_text', 'prgram_section_copy', __( 'Body Copy for program overview.', 'coreview')),
			         Field::make('complex', 'benefit_item', __( 'Benefit Item', 'coreveiw'))
			              ->set_layout( 'tabbed-horizontal' )
			              ->add_fields( array(
				              Field::make( 'image', 'benefit_icon', __( 'Benefit Item Image', 'coreview' ) ),
				              Field::make( 'text', 'benefit_heading', __( 'Benefit Heading', 'coreview' ) ),
				              Field::make( 'rich_text', 'benefit_body', __( 'Benefit Body Copy', 'coreview' ) )

			              ) ),
			         Field::make( 'text', 'button_text', __( 'CTA Text', 'coreview' ) ),
			         Field::make( 'text', 'button_url', __( 'CTA Link (URL)', 'coreview' ) )
			              ->set_attribute( 'type', 'url' ),

		         ]);


//		Container::make( 'post_meta', 'CoreView Contacts')
//				->where( 'post_type', '=', 'page' )
//				->where( 'post_template', '=', self::PAGE_TEMPLATE )
//				->add_fields([
//					Field::make('text','contact_heading', __( 'Contacts Heading', 'coreview')),
//					Field::make('complex', 'contact_info', __( 'Contact Item', 'coreveiw'))
//					     ->set_layout( 'tabbed-horizontal' )
//					     ->add_fields( array(
//						     Field::make( 'text', 'contact_item_heading', __( 'Contact Heading', 'coreview' ) ),
//						     Field::make( 'rich_text', 'contact_item_info', __( 'Contact Item Info', 'coreview' ) ),
//
//					     ) )
//
//				]);
	}

}