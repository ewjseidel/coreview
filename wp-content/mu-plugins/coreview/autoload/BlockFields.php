<?php

namespace Coreview;

use Carbon_Fields\Field;

class BlockFields {

	public static function buttonGroup( $max, $field_label ) {
		return Field::make( 'complex', 'buttons', __( $field_label , 'coreview' ) )
		            ->set_layout( 'tabbed-horizontal' )
					->set_max($max)
		            ->add_fields( [
			            Field::make( 'text', 'button_text', __( 'Button Text', 'coreview' ) ),
			            Field::make( 'text', 'button_url', __( 'Link (URL)', 'coreview' ) )
			                 ->set_attribute( 'type', 'url' ),
			            Field::make( 'select', 'button_style', __( 'Button Style', 'coreview' ) )
			                 ->set_options( [
				                 'blue'         => __( 'Blue', 'coreview' ),
				                 'white-border' => __( 'White Border', 'coreview' ),
				                 'blue-border'  => __( 'Blue Border', 'coreview' ),
				                 'blue-arrow-link' => __( 'Blue Arrow Link', 'coreview'),
				                 'black-arrow-link' => __( 'Black Arrow Link', 'coreview'),
				                 'white-arrow-link' => __( 'White Arrow Link', 'coreview')
			                 ] ),
		            ] );
	}


	public static function headerGroup() {
	    return [
                Field::make( 'text', 'title', __( 'Title', 'coreview' ) ),
                Field::make( 'radio', 'align', __( 'Alignment', 'coreview' ) )
                    ->set_options([
                            'text-left'  => __('Text Left', 'coreview'),
                            'text-right' => __('Text Right', 'coreview'),
                        ]
                    ),
                Field::make( 'rich_text', 'content', __( 'Content', 'coreview' ) ),
                Field::make( 'image', 'image', __( 'Image', 'coreview' ) ),
        ];
    }

	public static function formGroup() {
		return [
			Field::make( 'select', 'form_id', __( 'Form Type' ) )
			     ->add_options( [
				     'none'                => __( '--' ),
				     '1018'                => __( 'Newsletter Signup' ),
				     '1019'                => __( 'Contact Us' ),
				     '1017'                => __( 'Request Pricing' ),
				     '1015'                => __( 'Request a Trial' ),
				     '1016'                => __( 'Request a Demo' ),
				     'partnership_request' => __( 'Partnership Request' ),
			     ] ),
			Field::make( 'select', 'redirect', __( 'Redirect Behavior' ) )
				->set_conditional_logic( [
					[
						'field'   => 'form_id',
						'value'   => 'partnership_request',
						'compare' => '!='
					],
				] )
				->add_options( [
					'none' =>        __( '--' ),
					get_site_url(null, '/thank-you') =>   __( 'Thank You Page' ),
					'form-submit' => __( 'Stay on Page' )
				]),
			Field::make( 'rich_text', 'thank_you_text', __( 'Thank You Copy' ) )
				->set_conditional_logic( [
					[
						'field'   => 'form_id',
						'value'   => 'partnership_request',
						'compare' => '!='
					],
				] ),
		];
	}

	public static function grid( array $fields, array $args = [] ) {
		$args = wp_parse_args( $args, [
			'columns' => 2,
			'label'   => __( 'Grid Items', 'coreview' ),
		] );

		return [
			Field::make( 'select', 'grid_columns', __( 'Grid Columns', 'coreview' ) )
			     ->set_default_value( $args['columns'] )
			     ->set_options( [
				     2 => 2,
				     3 => 3,
				     4 => 4,
			     ] ),
			Field::make( 'complex', 'grid_items', $args['label'] )
			     ->set_layout( 'tabbed-horizontal' )
			     ->add_fields( $fields ),
		];
	}

	public static function sectionHeading() {
		return [
			Field::make( 'text', 'section_headline', __('Section Headline', 'coreview') ),
			Field::make( 'rich_text', 'section_subtext', __( 'Section Subtext', 'coreview' ) )
		];
	}


}
