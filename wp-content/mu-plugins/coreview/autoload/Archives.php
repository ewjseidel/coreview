<?php

namespace Coreview;

use Coreview\Collections\MixedPostCollection;
use Coreview\Collections\PartnerResourceCollection;
use Coreview\Collections\PostCollection;
use Coreview\Collections\ResourceCollection;
use Coreview\Collections\TeamCollection;
use Coreview\Models\PartnerResource;
use Coreview\PostTypes\TeamItems;
use Coreview\PostTypes\PartnerResources;
use wpscholar\WordPress\PostCollectionBase;

class Archives {

	private static $defaultQueryArgs = [
		'page'           => 1,
		'posts_per_page' => 12,
	];

	private static $queryArgs = [
//		'post'                           => [],
		TeamItems::POST_TYPE             => [],
		'resource-collection' => [
			'post_type' => [
				PostTypes\Posts::POST_TYPE,
				PostTypes\Resources::POST_TYPE,
			],
		],
		'news-events' => [
			'post_type' => [
				PostTypes\NewsItems::POST_TYPE,
				PostTypes\Events::POST_TYPE,
			],
		],
	];

	private static $queryVarMap = [
		'category' => 'category',
	];

	private static $taxonomies = [
		'post'                => [
			'category',
		],
		'resource-collection' => [
			Taxonomies\GeneralTags::TAXONOMY,
			Taxonomies\ResourceTypes::TAXONOMY,
		],
		'news-events' => [
			Taxonomies\AnnouncementCategories::TAXONOMY,
			Taxonomies\GeneralTags::TAXONOMY,
		],
		PartnerResource::POST_TYPE => [
			Taxonomies\GeneralTags::TAXONOMY,
			Taxonomies\PartnerResourceTypes::TAXONOMY,
		],
	];

	private static $collections = [
		'post'                => PostCollection::class,
		TeamItems::POST_TYPE  => TeamCollection::class,
		'resource-collection' => MixedPostCollection::class,
		'news-events'         => MixedPostCollection::class,
		PartnerResource::POST_TYPE => PartnerResourceCollection::class,
	];

	/**
	 * @param $taxonomy
	 *
	 * @return string
	 */
	public static function getQueryVar( $taxonomy ) {
		$var = $taxonomy;
		if ( ! empty( self::$queryVarMap[ $taxonomy ] ) ) {
			$var = self::$queryVarMap[ $taxonomy ];
		}

		return $var;
	}

	/**
	 * Returns an array of post type archive query arguments.
	 *
	 * @param string $postType
	 *
	 * @return array
	 */
	public static function getQueryArgs( $postType ) {
		if ( isset( self::$queryArgs[ $postType ] ) ) {
			return array_merge( self::$defaultQueryArgs, self::$queryArgs[ $postType ] );
		}

		return self::$defaultQueryArgs;
	}

	/**
	 * Returns an array of valid taxonomies for the provided post type.
	 *
	 * @param string $postType
	 *
	 * @return array
	 */
	public static function getTaxonomies( $postType ) {
		if ( isset( self::$taxonomies[ $postType ] ) ) {
			return self::$taxonomies[ $postType ];
		}

		return [];
	}

	/**
	 * Returns an array of contextual args, including ones from the current URL query string.
	 *
	 * @param string $postType
	 * @param array $args
	 *
	 * @return array
	 */
	public static function getContextualArgs( $postType, array $args = [] ) {
		$taxes      = self::getTaxonomies( $postType );
		$taxQueries = [];
		foreach ( $taxes as $taxonomy ) {
			if ( isset( self::$queryVarMap[ $taxonomy ] ) ) {
				$value = filter_input( INPUT_GET, self::$queryVarMap[ $taxonomy ] );
			} else {
				$value = filter_input( INPUT_GET, $taxonomy );
			}
			if ( ! empty( $value ) ) {
				if ( is_numeric( $value ) ) {
					$value = absint( $value );
				}
				$taxQueries[] = [
					'taxonomy' => $taxonomy,
					'field'    => is_numeric( $value ) ? 'term_id' : 'slug',
					'terms'    => $value,
				];
			}
		}
		if ( ! empty( $taxQueries ) ) {
			$args['tax_query'] = $taxQueries;
		}

		$pageNum = absint( filter_input( INPUT_GET, 'pg', FILTER_SANITIZE_NUMBER_INT ) );
		if ( $pageNum ) {
			$args['paged'] = $pageNum;
		}

		$search = filter_input( INPUT_GET, 'search', FILTER_SANITIZE_STRING );
		if ( ! empty( $search ) ) {
			$args['s'] = $search;
		}

		return $args;
	}

	/**
	 * @param string $postType
	 * @param array $args
	 * @param bool $getGlobalContext
	 *
	 * @return bool|PostCollectionBase
	 */
	public static function getCollection( $postType, array $args = [], $getGlobalContext = true ) {
		if ( isset( self::$collections[ $postType ] ) ) {
			if ( $getGlobalContext ) {
				$args = self::getContextualArgs( $postType, $args );
			}
			$args = wp_parse_args( $args, self::getQueryArgs( $postType ) );

			return new self::$collections[ $postType ]( $args );
		}

		return false;
	}

	/**
	 * @param string $taxonomy
	 *
	 * @return string|mixed
	 */
	public static function getCurrentFilterValue( $taxonomy ) {
		if ( isset( self::$queryVarMap[ $taxonomy ] ) ) {
			$value = filter_input( INPUT_GET, self::$queryVarMap[ $taxonomy ] );
		} else {
			$value = filter_input( INPUT_GET, $taxonomy );
		}

		return $value;
	}

}