<?php

namespace Coreview\Traits;

trait PostCategory {

    /**
     * @var \WP_Term
     * NOTE: `$cat` MUST be unique across all traits used on a class. Use something more specific.
     */
    private $cat;

    /**
     * @return \WP_Term
     */
    public function category() {
        if ( ! $this->cat ) {
            $cats = wp_get_object_terms( $this->id(), 'category' );
            if ( ! is_wp_error( $cats ) && count( $cats ) ) {
                $this->cat = reset( $cats );
            }
        }

        return $this->cat;
    }

    /**
     * @return string
     */
    public function categoryName() {
        return $this->category()->name;
    }

}