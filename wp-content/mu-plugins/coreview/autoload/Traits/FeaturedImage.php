<?php

namespace Coreview\Traits;

use function Coreview\getFallbackImageId;

trait FeaturedImage {

	public function featuredImage( $size = 'large' ) {
		if ( $this->hasPostThumbnail() ) {
			return $this->postThumbnail( $size );
		}

//		$imageId = getFallbackImageId( $this->post->ID );
//
//		if ( $imageId ) {
//			return wp_get_attachment_image( $imageId, $size );
//		}

		// No image found!
		return '';
	}

}