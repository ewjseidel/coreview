<?php

namespace Coreview\Traits;

trait GeneralType {

	/**
	 * @var \WP_Term
	 * NOTE: `$cat` MUST be unique across all traits used on a class. Use something more specific.
	 */
	private $triType;

	/**
	 * @return \WP_Term
	 */
	public function typeTerm() {
		if ( ! $this->triType ) {
			$types = wp_get_object_terms( $this->id(), $this->typeTaxonomy );
			if ( ! is_wp_error( $types ) && count( $types ) ) {
				$this->triType = reset( $types );
			}
		}

		return $this->triType;
	}

	/**
	 * @return string
	 */
	public function type() {
		if ( $type = $this->typeTerm() ) {
			return $this->typeTerm()->name;
		}

		return '';
	}

	public function typeSlug() {
		if ( $type = $this->typeTerm() ) {
			return $this->typeTerm()->slug;
		}

		return '';
	}

}
