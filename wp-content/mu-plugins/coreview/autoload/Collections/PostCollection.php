<?php
namespace Coreview\Collections;

use Coreview\Models\Post;
use wpscholar\WordPress\PostCollectionBase;

/**
 * Class PostCollection
 * @package Coreview\Collections
 */
class PostCollection extends PostCollectionBase {

    /**
     * Events type name
     *
     * @var string
     */
    const POST_TYPE = Post::POST_TYPE;

    /**
     * Convert a post ID into a Post model.
     *
     * @param int $id
     *
     * @return Post
     */
    protected function transform( $id ) {
        return new Post( get_post( $id ) );
    }

}
