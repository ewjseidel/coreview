<?php

namespace Coreview\Collections;

use Coreview\Models\NewsItem;
use wpscholar\WordPress\PostCollectionBase;

/**
 * Class NewsCollection
 * @package Tricentis\Collections
 */
class NewsCollection extends PostCollectionBase {

	/**
	 * News type name
	 *
	 * @var string
	 */
	const POST_TYPE = NewsItem::POST_TYPE;

	/**
	 * Convert a post ID into a NewsItem model.
	 *
	 * @param int $id
	 *
	 * @return NewsItem
	 */
	protected function transform( $id ) {
		return new NewsItem( get_post( $id ) );
	}

}
