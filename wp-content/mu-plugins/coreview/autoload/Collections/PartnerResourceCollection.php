<?php

namespace Coreview\Collections;

use Coreview\Models\PartnerResource;
use Coreview\PartnerRoles;
use wpscholar\WordPress\PostCollectionBase;

/**
 * Class PartnerResourceCollection
 * @package Coreview\Collections
 */
class PartnerResourceCollection extends PostCollectionBase {

	/**
	 * Resources type name
	 *
	 * @var string
	 */
	const POST_TYPE = PartnerResource::POST_TYPE;

	public function currentUserPermissions() {
		if ( current_user_can( 'edit_posts' ) ) {
			return array_keys( PartnerRoles::ROLES );
		}

		$permissions = [];
		foreach ( array_keys( PartnerRoles::ROLES ) as $permission ) {
			if ( current_user_can( $permission ) ) {
				$permissions[] = $permission;
			}
		}

		return $permissions;
	}

	public function fetch( $args = [] ) {
		$args = wp_parse_args( $args );

		$permissions = $this->currentUserPermissions();
		if ( empty( $permissions ) ) {
			$permissions = [ 'no-access' ];
		}

		$args['meta_query'] = [
			[
				'key'     => 'cv_roles_with_access',
				'value'   => $permissions,
				'compare' => 'IN',
			],
		];

		parent::fetch( $args );
	}

	/**
	 * Convert a post ID into a Resource model.
	 *
	 * @param int $id
	 *
	 * @return PartnerResource
	 */
	protected function transform( $id ) {
		return new PartnerResource( get_post( $id ) );
	}

}
