<?php

namespace Coreview\Collections;

use Coreview\Models\Resource;
use Coreview\Models\TeamItem;
use Coreview\PostTypes\TeamItems;
use wpscholar\WordPress\PostCollectionBase;

/**
 * Class ResourceCollection
 * @package Coreview\Collections
 */
class TeamCollection extends PostCollectionBase {

	/**
	 * Resources type name
	 *
	 * @var string
	 */
	const POST_TYPE = TeamItems::POST_TYPE;

	/**
	 * Convert a post ID into a Resource model.
	 *
	 * @param int $id
	 *
	 * @return Resource
	 */
	protected function transform( $id ) {
		return new TeamItem( get_post( $id ) );

	}

}
