<?php

namespace Coreview\Collections;

use Coreview\Models\Event;
use wpscholar\WordPress\PostCollectionBase;

/**
 * Class EventCollection
 * @package Coreview\Collections
 */
class EventCollection extends PostCollectionBase {

	/**
	 * Event type name
	 *
	 * @var string
	 */
	const POST_TYPE = Event::POST_TYPE;

	/**
	 * Convert a post ID into a Resource model.
	 *
	 * @param int $id
	 *
	 * @return Event
	 */
	protected function transform( $id ) {
		return new Event( get_post( $id ) );

	}

}
