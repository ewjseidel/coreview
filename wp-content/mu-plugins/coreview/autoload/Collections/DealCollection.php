<?php

namespace Coreview\Collections;

use Coreview\Models\Deal;
use wpscholar\WordPress\PostCollectionBase;

/**
 * Class DealCollection
 * @package Tricentis\Collections
 */
class DealCollection extends PostCollectionBase {

	/**
	 * News type name
	 *
	 * @var string
	 */
	const POST_TYPE = Deal::POST_TYPE;

	public function fetch( $args = [] ) {
		$args = wp_parse_args( $args );

		$args['author'] = get_current_user_id();

		parent::fetch( $args );
	}

	/**
	 * Convert a post ID into a NewsItem model.
	 *
	 * @param int $id
	 *
	 * @return Deal()
	 */
	protected function transform( $id ): Deal {
		return new Deal( get_post( $id ) );
	}

}
