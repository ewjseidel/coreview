<?php

namespace Coreview\Collections;

use Coreview\Models\Resource;
use wpscholar\WordPress\PostCollectionBase;

/**
 * Class ResourceCollection
 * @package Coreview\Collections
 */
class ResourceCollection extends PostCollectionBase {

	/**
	 * Resources type name
	 *
	 * @var string
	 */
	const POST_TYPE = Resource::POST_TYPE;

	/**
	 * Convert a post ID into a Resource model.
	 *
	 * @param int $id
	 *
	 * @return Resource
	 */
	protected function transform( $id ) {
		return new Resource( get_post( $id ) );
	}

}
