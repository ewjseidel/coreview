<?php

namespace Coreview\Collections;

use Coreview\PostTypeModelRegistry;
use wpscholar\WordPress\PostCollectionBase;
use wpscholar\WordPress\PostModelBase;

/**
 * Class MixedPostCollection
 *
 * @package FDOC\Collections
 */
class MixedPostCollection extends PostCollectionBase {

	/**
	 * Fetch items
	 *
	 * @param array|string $args
	 */
	public function fetch( $args = [] ) {

		$args = wp_parse_args( $args );

		$items = [];

		$query_args = array_merge(
			$this->default_args,
			$args,
			array_merge(
				[
					'fields'    => 'ids',
				],
				$this->required_args
			)
		);

		$query = new \WP_Query( $query_args );

		$this->found_posts = (int) $query->found_posts;
		$this->max_num_pages = (int) $query->max_num_pages;
		$this->page = (int) $query->get( 'paged' );

		if ( $query->have_posts() ) {
			$items = $query->posts;
		}

		$this->populate( $items );
	}

	/**
	 * Convert a post ID into a model.
	 *
	 * @param int $id
	 *
	 * @return PostModelBase
	 */
	protected function transform( $id ) {
		return PostTypeModelRegistry::getInstance( get_post( $id ) );
	}

}
