<?php

namespace Coreview;

class MobileNavWalker extends \Walker_Nav_Menu {

	/**
	 * Starts the element output.
	 *
	 * @since 3.0.0
	 * @since 4.4.0 The {@see 'nav_menu_item_args'} filter was added.
	 *
	 * @see Walker::start_el()
	 *
	 * @param string $output Used to append additional content (passed by reference).
	 * @param \WP_Post $item Menu item data object.
	 * @param int $depth Depth of menu item. Used for padding.
	 * @param \stdClass $args An object of wp_nav_menu() arguments.
	 * @param int $id Current item ID.
	 */

	private $show_parent_title = false;

	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

		if( $this->show_parent_title ) {

            $object_id  = get_post_meta( $item->menu_item_parent, '_menu_item_object_id', true );
            $object     = get_post_meta( $item->menu_item_parent, '_menu_item_object',    true );
            $type       = get_post_meta( $item->menu_item_parent, '_menu_item_type',      true );
            $title      = get_the_title( $item->menu_item_parent );

            if( empty( $title ) ) {

                if ( 'post_type' == $type ) {
                    $title = get_post( $object_id )->post_title;
                } elseif ( 'taxonomy' == $type) {
                    $title = get_term( $object_id, $object )->name;
                }

            }

            $output .= sprintf( '<li class="cv-mobile-nav__sub-menu-title"><h3>%1$s</h3></li>', $title ); // OR post_parent if dynamically generated
            $this->show_parent_title = false;

        }

		if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
			$t = '';
			$n = '';
		} else {
			$t = "\t";
			$n = "\n";
		}
		$indent = ( $depth ) ? str_repeat( $t, $depth ) : '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;

		/**
		 * Filters the arguments for a single nav menu item.
		 *
		 * @since 4.4.0
		 *
		 * @param \stdClass $args An object of wp_nav_menu() arguments.
		 * @param \WP_Post $item Menu item data object.
		 * @param int $depth Depth of menu item. Used for padding.
		 */
		$args = apply_filters( 'nav_menu_item_args', $args, $item, $depth );

		/**
		 * Filters the CSS class(es) applied to a menu item's list item element.
		 *
		 * @since 3.0.0
		 * @since 4.1.0 The `$depth` parameter was added.
		 *
		 * @param array $classes The CSS classes that are applied to the menu item's `<li>` element.
		 * @param \WP_Post $item The current menu item.
		 * @param \stdClass $args An object of wp_nav_menu() arguments.
		 * @param int $depth Depth of menu item. Used for padding.
		 */
		$class_names = implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
		$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

		/**
		 * Filters the ID applied to a menu item's list item element.
		 *
		 * @since 3.0.1
		 * @since 4.1.0 The `$depth` parameter was added.
		 *
		 * @param string $menu_id The ID that is applied to the menu item's `<li>` element.
		 * @param \WP_Post $item The current menu item.
		 * @param \stdClass $args An object of wp_nav_menu() arguments.
		 * @param int $depth Depth of menu item. Used for padding.
		 */
		$id = apply_filters( 'nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args, $depth );
		$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

		$output .= $indent . '<li' . $id . $class_names . '>';

		$atts = array();
		$atts['title'] = ! empty( $item->attr_title ) ? $item->attr_title : '';
		$atts['target'] = ! empty( $item->target ) ? $item->target : '';
		$atts['rel'] = ! empty( $item->xfn ) ? $item->xfn : '';
		$atts['href'] = ! empty( $item->url ) ? $item->url : '';

		/**
		 * Filters the HTML attributes applied to a menu item's anchor element.
		 *
		 * @since 3.6.0
		 * @since 4.1.0 The `$depth` parameter was added.
		 *
		 * @param array $atts {
		 *     The HTML attributes applied to the menu item's `<a>` element, empty strings are ignored.
		 *
		 * @type string $title Title attribute.
		 * @type string $target Target attribute.
		 * @type string $rel The rel attribute.
		 * @type string $href The href attribute.
		 * }
		 *
		 * @param \WP_Post $item The current menu item.
		 * @param \stdClass $args An object of wp_nav_menu() arguments.
		 * @param int $depth Depth of menu item. Used for padding.
		 */
		$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

		$attributes = '';
		foreach ( $atts as $attr => $value ) {
			if ( ! empty( $value ) ) {
				$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
				$attributes .= ' ' . $attr . '="' . $value . '"';
			}
		}

		/** This filter is documented in wp-includes/post-template.php */
		$title = apply_filters( 'the_title', $item->title, $item->ID );

		/**
		 * Filters a menu item's title.
		 *
		 * @since 4.4.0
		 *
		 * @param string $title The menu item's title.
		 * @param \WP_Post $item The current menu item.
		 * @param \stdClass $args An object of wp_nav_menu() arguments.
		 * @param int $depth Depth of menu item. Used for padding.
		 */
		$title = apply_filters( 'nav_menu_item_title', $title, $item, $args, $depth );
		$arrow_svg = file_get_contents( get_template_directory() . "/assets/img/background-icons/link-arrow.svg");

		$item_output = $args->before;
		$item_output .= '<a' . $attributes . ' class="cv-mobile-nav__link">';
		if ( $this->has_children ) {
			$item_output .= '<span class="cv-mobile-nav__item-label">' . $title . '</span>';
			$item_output .= '<span class="cv-mobile-nav__sub-nav-toggle">' . $arrow_svg . '</span>';
		} else {
			$item_output .= '<span class="cv-mobile-nav__item-label">' . $title . '</span>';
		}
		$item_output .= '</a>';
		$item_output .= $args->after;

		/**
		 * Filters a menu item's starting output.
		 *
		 * The menu item's starting output only includes `$args->before`, the opening `<a>`,
		 * the menu item's title, the closing `</a>`, and `$args->after`. Currently, there is
		 * no filter for modifying the opening and closing `<li>` for a menu item.
		 *
		 * @since 3.0.0
		 *
		 * @param string $item_output The menu item's starting HTML output.
		 * @param \WP_Post $item Menu item data object.
		 * @param int $depth Depth of menu item. Used for padding.
		 * @param \stdClass $args An object of wp_nav_menu() arguments.
		 */
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

	function start_lvl( &$output, $depth = 0, $args = array() ) {
		// Depth-dependent classes.
		$indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
		$display_depth = ( $depth + 1);// because it counts the first submenu as 0
		$classes = array(
			'sub-menu',
			'cv-mobile-nav__sub-menu',
			( $display_depth % 2  ? 'cv-mobile-nav__sub-menu-odd' : 'cv-mobile-nav__sub-menu-even' ),
			( $display_depth >=2 ? 'cv-mobile-nav__sub-sub-menu' : '' ),
			'cv-mobile-nav__sub-menu-depth-' . $display_depth,
		);
		$this->show_parent_title = true;
		$class_names = implode( ' ', $classes );
		// Build HTML for output.
		$output .= "\n" . $indent . '<ul class="' . $class_names . '"><li></li>' . "\n";
	}

}