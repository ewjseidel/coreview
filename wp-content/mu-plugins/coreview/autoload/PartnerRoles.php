<?php

namespace Coreview;

/**
 * Handles registering partner roles.
 *
 * Class PartnerRoles
 * @package Coreview
 */
class PartnerRoles {

	const ROLES = [
		'coreview_pp_reseller' => [
			'label'        => 'Partner: Reseller',
			'capabilities' => [
				'read',
				'coreview_pp_reseller',
			],
		],
		'coreview_pp_msp'      => [
			'label'        => 'Partner: MSP',
			'capabilities' => [
				'read',
				'coreview_pp_msp',
			],
		],
	];

	const PRIVATE_ROLES = [
		'coreview_pp_blended' => [
			'label'        => 'Partner: Blended',
			'capabilities' => [
				'read',
				'coreview_pp_msp',
				'coreview_pp_reseller',
			],
		],
	];

	public static function initialize(): void {
		// Setup roles on visit to users page if they don't exist.
		add_action( 'load-users.php', [ __CLASS__, 'setupRoles' ] );
		// Remove and re-setup roles on visit to tools page.
		add_action( 'load-tools.php', [ __CLASS__, 'setupRoles' ], 10, 0 );
	}

	/**
	 * Removes all custom roles.
	 */
	public static function _cleanupAllRoles(): void {
		$roles = wp_roles()->roles;
		foreach ( $roles as $role_name => $role_info ) {
			// Remove roles that we dynamically created...
			if ( 0 === strpos( $role_name, 'coreview_' ) ) {
				wp_roles()->remove_role( $role_name );
			}
		}
	}

	/**
	 * Returns an array of all custom CoreView roles, empty array if no CoreView roles found.
	 *
	 * @return array
	 */
	public static function roleList(): array {
		$allRoles = wp_roles()->roles;
		$roles = [];
		foreach ( $allRoles as $roleName => $roleInfo ) {
			// Only allow returning of roles we created
			if ( 0 === strpos( $roleName, 'coreview_' ) ) {
				$roles[ $roleName ] = $roleInfo['name'];
			}
		}

		ksort( $roles );

		return $roles;
	}

	/**
	 * Sets up our custom roles
	 *
	 * @param bool $cleanup If true, will try to delete any existing roles (default), false to disable this behavior
	 *
	 * @return array Returns an array of generated role names on success, empty array otherwise
	 */
	public static function setupRoles( $cleanup = true ): array {
		$siteRoles = [];
		if ( $cleanup ) {
			// Make sure any existing roles are removed
			self::_cleanupAllRoles();
		}
		// Add the roles for this site
		$roles = array_merge( self::ROLES, self::PRIVATE_ROLES );
		foreach ( $roles as $name => $args ) {
			$roleObj = wp_roles()->add_role( $name, $args['label'], self::_prepareBulkCaps( $args['capabilities'] ) );
			if ( $roleObj ) {
				$siteRoles[] = $roleObj->name;
			}
		}

		return $siteRoles;
	}

	/**
	 * Sets up an array of capabilities
	 *
	 * @param array $capabilities
	 *
	 * @return array
	 */
	protected static function _prepareBulkCaps( $capabilities ): array {
		$caps = [];
		foreach ( $capabilities as $cap ) {
			$caps[ $cap ] = true;
		}

		return $caps;
	}

	/**
	 * Returns true if the user is a blended user.
	 *
	 * @return bool
	 */
	public static function isUserBlended(): bool {
		return self::isUserReseller() && self::isUserMSP();
	}

	/**
	 * Returns true if the current user is a reseller.
	 *
	 * @return bool
	 */
	public static function isUserReseller(): bool {
		return current_user_can( 'coreview_pp_reseller' );
	}

	/**
	 * Returns true if the current user is an MSP.
	 *
	 * @return bool
	 */
	public static function isUserMSP(): bool {
		return current_user_can( 'coreview_pp_msp' );
	}

}
