<?php

namespace Coreview;

use Coreview\Models\Post;
use wpscholar\Collection;
use wpscholar\WordPress\PostModelBase;

/**
 * Class PostTypeModelRegistry
 *
 * @package Coreview
 */
class PostTypeModelRegistry {

	protected static $collection;

	/**
	 * Fetch collection containing models
	 *
	 * @return Collection
	 */
	public static function collection() {
		if ( null === self::$collection ) {
			self::$collection = new Collection();
		}

		return self::$collection;
	}

	/**
	 * Register a new model.
	 *
	 * @param string $postType
	 * @param string $modelClassName
	 */
	public static function add( $postType, $modelClassName ) {
		self::collection()->put( $postType, $modelClassName );
	}

	/**
	 * Get the class name for the model associated with the specified post type.
	 *
	 * @param string $postType
	 *
	 * @param string|null $fallback
	 *
	 * @return string
	 */
	public static function get( $postType, $fallback = null ) {
		if ( ! $fallback ) {
			$fallback = Post::class;
		}

		return self::collection()->get( $postType, $fallback );
	}

	/**
	 * Delete a model from the registry.
	 *
	 * @param string $postType
	 */
	public static function delete( $postType ) {
		self::collection()->forget( $postType );
	}

	/**
	 * Get a model instance (if possible) given a post object.
	 *
	 * @param \WP_Post $post
	 *
	 * @return PostModelBase|null
	 */
	public static function getInstance( \WP_Post $post ) {
		$instance = null;
		$className = self::get( $post->post_type );
		if ( class_exists( $className ) ) {
			$instance = new $className( $post );
		}

		return $instance;
	}

}
