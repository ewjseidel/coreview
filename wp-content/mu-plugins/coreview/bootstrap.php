<?php

if ( ! defined( 'COREVIEW_FILE' ) ) {

	define( 'COREVIEW_FILE', __FILE__ );

	if ( file_exists( __DIR__ . '/vendor/autoload.php' ) ) {
		// Autoload dependencies
		require __DIR__ . '/vendor/autoload.php';
	}

	// Register our custom autoloader
	spl_autoload_register( function ( $class ) {
		$prefix = 'Coreview\\';
		$len    = strlen( $prefix );
		if ( strncmp( $prefix, $class, $len ) === 0 ) {
			$file = __DIR__ . '/autoload/' . str_replace( '\\', '/', substr( $class, $len ) ) . '.php';
			if ( file_exists( $file ) ) {
				require $file;
			}
		}

	} );

	// Register custom autoloader for future packages
	spl_autoload_register( function ( $class ) {
		$prefix = 'wpscholar\\';
		$len    = strlen( $prefix );
		if ( strncmp( $prefix, $class, $len ) === 0 ) {
			$file = __DIR__ . '/wpscholar/' . str_replace( '\\', '/', substr( $class, $len ) ) . '.php';
			if ( file_exists( $file ) ) {
				require $file;
			}
		}

	} );

	// Automatically load all PHP files in the '/includes' directory
	$iterator = new RecursiveDirectoryIterator( __DIR__ . '/includes' );
	foreach ( new RecursiveIteratorIterator( $iterator ) as $file ) {
		if ( $file->getExtension() === 'php' ) {
			require $file;
		}
	}

}
