<?php
/**
 * Plugin Name: CoreView Functionality
 * Plugin URI: https://www.coreview.com/
 * Description: Custom functionality for the CoreView website.
 * Author: Narwhal Digital
 * Author URI: https://www.narwhal.digital/
 * Version: 1.0.0
 * License: GPL3
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 * Text Domain: coreview
 */

/**
 * This file is just a bootstrap loader for the primary plugin files.
 */

require __DIR__ . '/coreview/bootstrap.php';
//require __DIR__ . '/coreview-blocks/bootstrap.php';