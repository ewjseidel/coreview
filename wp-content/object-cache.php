<?php

// Only load object cache on Pantheon for the time being.
if ( isset( $_ENV['PANTHEON_ENVIRONMENT'] ) ) {
	# This is a Windows-friendly symlink
	require_once WP_CONTENT_DIR . '/plugins/wp-redis/object-cache.php';
}
