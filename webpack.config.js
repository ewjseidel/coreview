'use strict';

const autoprefixer = require('autoprefixer');
const browsers = require('@wordpress/browserslist-config');
const globImporter = require('node-sass-glob-importer');
const path = require('path');

const mode = process.env.NODE_ENV || 'development';
const isProduction = mode === 'production';

//const Blocks = require('./webpack.blocks.config');
const Theme = require('./webpack.theme.config');

const externals = {
    '@wordpress/blocks': 'wp.blocks',
    '@wordpress/components': 'wp.components',
    '@wordpress/compose': 'wp.compose',
    '@wordpress/data': 'wp.data',
    '@wordpress/editor': 'wp.editor',
    '@wordpress/element': 'wp.element',
    '@wordpress/hooks': 'wp.hooks',
    '@wordpress/i18n': 'wp.i18n',
    '@narwhal/components': 'narwhalWp.components',
    jquery: 'jQuery',
    lodash: 'lodash',
};

const loaders = {
    css: {
        loader: 'css-loader',
        options: {
            sourceMap: true
        }
    },
    postCss: {
        loader: 'postcss-loader',
        options: {
            plugins: [
                autoprefixer({
                    browsers,
                    flexbox: 'no-2009'
                })
            ],
            sourceMap: true
        }
    },
    sass: {
        loader: 'sass-loader',
        options: {
            importer: globImporter(),
            sourceMap: true,
            includePaths: [path.resolve('./node_modules')],
        }
    }
};

module.exports = [
    //Blocks(mode, isProduction, externals, loaders),
    Theme(mode, isProduction, externals, loaders),
];
